using System;
using Android.Text;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Android.OS;
using STSR_Droid.Views;
using STSR_Droid.Models;
using STSR_Droid.ORM;
using Android.Content.PM;
using Android.Graphics;

namespace STSR_Droid
{
    [Activity(Label = "Default", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SR_DefaultActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            // Create your application here
            SetContentView(Resource.Layout.SR_Default);
            View DefaultView = FindViewById<View>(Resource.Id.DefaultView);
            Button btnRequest = FindViewById<Button>(Resource.Id.btnRequest);
            btnRequest.Click += btnRequest_Click;
            Button btnCustApproval = FindViewById<Button>(Resource.Id.btnCustApproval);
            btnCustApproval.Click += btnCustApproval_Click;
            Button btnG4SApproval = FindViewById<Button>(Resource.Id.btnG4SApproval);
            btnG4SApproval.Click += btnG4SApproval_Click;
            Button btnQuit = FindViewById<Button>(Resource.Id.btnQuit);
            btnQuit.Click += btnQuit_Click;

            DefaultView.SetBackgroundColor(Color.White);

        }

        void btnRequest_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(SR_RequestListActivity));
        }

        void btnCustApproval_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(SR_CustApprovalListActivity));
        }

        void btnG4SApproval_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(SR_G4SApprovalListActivity));
        }

        void btnQuit_Click(object sender, EventArgs e)
        {
            Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        }

    }
}
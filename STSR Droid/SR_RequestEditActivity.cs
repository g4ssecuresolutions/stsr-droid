using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.Widget;
//using Android.Support.V7.App;
using STSR_Droid.ORM;
using STSR_Droid.Class;
using Android.Graphics;
using Android.Content.PM;

namespace STSR_Droid
{
    [Activity(Label = "SR_RequestEditActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SR_RequestEditActivity : Activity
    {
        
        protected override void OnCreate(Bundle bundle)
        {

            // Create your application here

            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.SR_RequestEdit);

            TextView lblRequestID = FindViewById<TextView>(Resource.Id.lblRequestID);
            TextView txtRequestID = FindViewById<TextView>(Resource.Id.txtRequestID);
            TextView lblStatus = FindViewById<TextView>(Resource.Id.lblStatus);
            TextView txtStatus = FindViewById<TextView>(Resource.Id.txtStatus);
            TextView lblBranch = FindViewById<TextView>(Resource.Id.lblBranch);
            TextView txtBranch = FindViewById<TextView>(Resource.Id.txtBranch);
            ImageView imgPriority = FindViewById<ImageView>(Resource.Id.imgPriority);
            txtRequestID.SetTextColor(Color.Red);
            txtStatus.SetTextColor(Color.Red);
            txtBranch.SetTextColor(Color.Red);
            lblRequestID.SetTextColor(Color.Black);
            lblStatus.SetTextColor(Color.Black);
            lblBranch.SetTextColor(Color.Black);
            imgPriority.Visibility = ViewStates.Invisible;

            //int RequestID = CInt(txtRequestID.Tag);

            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            SlidingTabFragment fragment = new SlidingTabFragment();
            fragment.ModuleType = "RequestApproval";
            //DBRequestRepository orDb = new DBRequestRepository();
            //fragment.SR_Session = orDb.SetSessionValues(fragment.SR_Session);
            fragment.SR_Session.SR_Status = txtStatus.Text;
            transaction.Replace(Resource.Id.sample_content_fragment, fragment);
            transaction.Commit();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.RequestMenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }
    }
}
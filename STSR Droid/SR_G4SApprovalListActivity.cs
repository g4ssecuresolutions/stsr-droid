using System;
using System.Collections;
using Android.Text;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Android.OS;
using STSR_Droid.Views;
using STSR_Droid.Models;
using STSR_Droid.ORM;
using STSR_Droid.Class;
using Android.Graphics;
using Android.Content.PM;
using System.Collections.Generic;

namespace STSR_Droid
{
    [Activity(Label = "SR_G4SApprovalListActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SR_G4SApprovalListActivity : Activity
    {

        protected override void OnCreate(Bundle bundle)
        {
            try
            {
                base.OnCreate(bundle);
                SetContentView(Resource.Layout.SR_G4SApprovalList);
                View DefaultView = FindViewById<View>(Resource.Id.RequestView);
                //Button btnRequest = FindViewById<Button>(Resource.Id.btnList);
                Button btnSearch = FindViewById<Button>(Resource.Id.btnSearch);

                //btnRequest.Click += btnRequest_Click;
                btnSearch.Click += btnSearch_Click;

                DefaultView.SetBackgroundColor(Color.White);
                var G4SApprovalAdapter = new Adapter.SR_G4SApprovalAdapter(this);
                var G4SApprovalListView = FindViewById<ListView>(Resource.Id.lstRequest);
                var tvheader = FindViewById<TextView>(Resource.Id.tv_header);

                tvheader.SetTextColor(Color.Black);
                G4SApprovalListView.Adapter = G4SApprovalAdapter;
                G4SApprovalAdapter.OnClickItem += G4SApprovalAdapter_OnClickItem;

            } catch (Exception e)
            {
                string t = e.Message;
            }

        }
        //void btnRequest_Click(object sender, EventArgs e)
        //{
        //    var G4SApprovalAdapter = new Adapter.SR_G4SApprovalAdapter(this);
        //    var G4SApprovalListView = FindViewById<ListView>(Resource.Id.lstRequest);
        //    var tvheader = FindViewById<TextView>(Resource.Id.tv_header);

        //    tvheader.SetTextColor(Color.Black);
        //    G4SApprovalListView.Adapter = G4SApprovalAdapter;
        //    G4SApprovalAdapter.OnClickItem += G4SApprovalAdapter_OnClickItem;
        //}
        void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ListView G4SApprovalListView = FindViewById<ListView>(Resource.Id.lstRequest);
                EditText RequestID = FindViewById<EditText>(Resource.Id.txtSearchID);
                TextView tvheader = FindViewById<TextView>(Resource.Id.tv_header);
                int intRequestID;
                int.TryParse(RequestID.Text.ToString(), out intRequestID);
                var G4SApprovalAdapter = new Adapter.SR_G4SApprovalAdapter(this, intRequestID);

                tvheader.SetTextColor(Color.Black);
                G4SApprovalListView.Adapter = G4SApprovalAdapter;
                G4SApprovalAdapter.OnClickItem += G4SApprovalAdapter_OnClickItem;
            }
            catch (Exception re)
            {
                string t = re.Message;
            }
        }
        void G4SApprovalAdapter_OnClickItem(int oIndex)
        {
            DBG4SRepository orDb2 = new DBG4SRepository();
            SR_Session oSession = new SR_Session();
            oSession= orDb2.SetSessionValues(oSession);
            string oUserID = oSession.SR_UserID;
            STSR_Droid.Models.SR_G4SApprove oRequest = new STSR_Droid.Models.SR_G4SApprove();
            SetContentView(Resource.Layout.SR_G4SApprovalEdit);
            DBG4SRepository dbr = new DBG4SRepository();

            List<SR_G4SApprove> lstRequest = new List<SR_G4SApprove>();

            List<SRWS_ASMX.SR_G4SApprove> vData = dbr.SR_SPWorkflow_SubmitbyRequestID(oUserID,oIndex.ToString());
            List<SRWS_ASMX.SR_G4SApprove> itemData = new List<SRWS_ASMX.SR_G4SApprove>();

            foreach (var item in vData)
            {
                List<SRWS_ASMX.SR_G4SApprove> oProItem = new List<SRWS_ASMX.SR_G4SApprove>();
                oProItem.Add(item);
                oRequest.LoadModel(oProItem);
                //oRequest.RequestID = item.RequestID;
                //oRequest.RequestStatusID = item.RequestStatusID;
                //oRequest.Status = item.Status;
                itemData.Add(item);
            }

            TextView lblRequestID = FindViewById<TextView>(Resource.Id.lblRequestID);
            TextView txtRequestID = FindViewById<TextView>(Resource.Id.txtRequestID);
            TextView lblStatus = FindViewById<TextView>(Resource.Id.lblStatus);
            TextView txtStatus = FindViewById<TextView>(Resource.Id.txtStatus);
            TextView lblBranch = FindViewById<TextView>(Resource.Id.lblBranch);
            TextView txtBranch = FindViewById<TextView>(Resource.Id.txtBranch);
            TextView txtMessage = FindViewById<TextView>(Resource.Id.txtMessage);


            txtRequestID.SetTextColor(Color.Red);
            txtStatus.SetTextColor(Color.Red);
            txtBranch.SetTextColor(Color.Red);
            txtMessage.SetTextColor(Color.Red);
            lblRequestID.SetTextColor(Color.Black);
            lblStatus.SetTextColor(Color.Black);
            lblBranch.SetTextColor(Color.Black);

            //int RequestID = CInt(txtRequestID.Tag);

            txtRequestID.Text = "Request ID: " + oIndex.ToString();
            txtRequestID.Tag = oIndex.ToString();
            txtStatus.Text = oRequest.Status;
            txtBranch.Text = oRequest.DeptID;
            if (oRequest.AppdCharges) { txtMessage.Text = "Customer approved Additional charges."; }
            if (oRequest.DeniedCharges) { txtMessage.Text = "Customer denied Additional charges."; }
            //StartActivity(typeof(SR_RequestEditActivity));

            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            SlidingTabFragment fragment = new SlidingTabFragment();
            fragment.RequestID = oIndex.ToString();
            fragment.ModuleType = "G4SApproval";
            fragment.mod_PortalG4SApprove = itemData;
            transaction.Replace(Resource.Id.sample_content_fragment, fragment);
            transaction.Commit();



        }

    }
}
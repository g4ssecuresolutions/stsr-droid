using System;
using System.Collections.Generic;
using System.Linq;
////using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace SR_Class
{
    public class SR_Core
    {
        public Byte[] xxGetBytes(Array strArray)
        {
            //Byte[] ArrBytes;
            List<Byte> Bytes = new List<Byte>();
            //byte[] Transfer = new byte[strArray.Length * sizeof(int)];
            byte[] Transfer = ConvertToBytes(strArray);




            for (int i = 0; i < Transfer.Length; ++i)
            {
                Bytes.Add((byte)Transfer[i]);
            }
            return Bytes.ToArray();
        }
        static public byte[] ConvertToBytes(Array myArray)
        {
            List<byte> allBytes = new List<byte>();
            
            foreach (object obj in myArray)
            {
                byte[] bytes = null;
                if (obj is string)
                {
                    //allBytes.AddRange(Convert.ToByte((obj.ToString())));
                    //bytes = new byte[i];
                    //bytes[i] = Convert.ToByte(obj);
                    //i = ++i;
                    allBytes.Add(Convert.ToByte(obj));
                    //char[] chrString = ((string)obj).ToCharArray();
                    //bytes = new byte[chrString.Length];
                    //int loop = 0;
                    //for (loop = 0; loop < chrString.Length; loop++)
                    //{
                    //    bytes[loop] = Convert.ToByte(chrString[loop]);
                    //}
                }
                else if (obj is int)
                {
                    bytes = BitConverter.GetBytes((int)obj);
                }
                else if (obj is double)
                {
                    bytes = BitConverter.GetBytes((double)obj);
                }
                if (bytes != null)
                {
                    allBytes.AddRange(bytes);
                }
                bytes = null;
            }
            return allBytes.ToArray();
        }
        public Boolean ExistField(DataRow oRow, String oName) 
        {
            Boolean Rtn = false;

            try
            {
                if (oRow.Table.Columns.Contains(oName))
                {
                    Rtn = true;
                }
                else
                {
                    Rtn = false;
                }
            }
            catch { Rtn = false; }

            return Rtn;
        }

    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using Java.Lang;
using STSR_Droid.ORM;
using STSR_Droid.Models;


namespace STSR_Droid.Class
{
    public partial class SlidingTabFragment : Fragment, DatePickerDialog.IOnDateSetListener, TimePickerDialog.IOnTimeSetListener
    {
        public static SR_CboAdapter oLckCustomer = new SR_CboAdapter();
        public static SR_CboAdapter oLckSite = new SR_CboAdapter();
        public static SR_CboAdapter oLckState = new SR_CboAdapter();
        public static SR_CboAdapter oLckOfficerType = new SR_CboAdapter();
        public static SR_CboAdapter oLckReasonServices = new SR_CboAdapter();
        public static SR_CboAdapter oLckReasonDeny = new SR_CboAdapter();
        public static SR_CboAdapter oLckDepartment = new SR_CboAdapter();
        public static SR_CboAdapter oLckLocationType = new SR_CboAdapter();
        

        public static SR_Request mod_Request = new SR_Request();
        public static SR_Request mod_SaveRequest = new SR_Request();

        public static SR_G4SApprove mod_G4SApprove = new SR_G4SApprove();
        public static SR_G4SApprove mod_SaveG4SApprove = new SR_G4SApprove();

        public static SR_CustomerApprove mod_CustomerApprove = new SR_CustomerApprove();
        public static SR_CustomerApprove mod_SaveCustomerApprove = new SR_CustomerApprove();

        public static Activity xActivity = new Activity();
        public static SlidingTabFragment xSliding = new SlidingTabFragment();
        //public static Dialog xDialog = new Dialog(xActivity);
        public static FragmentManager xFragment { get; set; }
        //public static SR_DatePickerFragment oLckTerminateDate = new SR_DatePickerFragment();

        public static bool Loaded_Page1 = false;
        public static bool Loaded_Page2 = false;
        public static bool Loaded_Page3 = false;
        public static bool Loaded_Page4 = false;
        public static bool Loaded_Page5 = false;

        private SlidingTabScrollView mSlidingTabScrollView;
        private ViewPager mViewPager;
        private int xpos;
        //private static ViewPager mRefViewPager;
        //public DatePickerDialog Pk_Date;

        public string ModuleType { get; set; }
        public string RequestID { get; set; }
        public static string Refclassitem { get; set; }
        // View Repositories
        public static View view_00 = null;
        public static View view_01 = null;
        public static View view_02 = null;
        public static View view_03 = null;
        public static View view_04 = null;
        public SR_Session SR_Session = new SR_Session();
        public SRWS_ASMX.SR_RequestItem mod_PortalRequest = new SRWS_ASMX.SR_RequestItem();
        public List<SRWS_ASMX.SR_G4SApprove> mod_PortalG4SApprove = new List<SRWS_ASMX.SR_G4SApprove>();
        public List<SRWS_ASMX.SR_CustomerApprove> mod_PortalCustomerApprove = new List<SRWS_ASMX.SR_CustomerApprove>();

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            switch (ModuleType)
            {
                case "RequestApproval":
                    // Loading fundamental data **********************
                    DBRequestRepository orDb1 = new DBRequestRepository();
                    SR_Session = orDb1.SetSessionValues(SR_Session);
                    mod_SaveRequest.LoadModel(mod_PortalRequest);
                    mod_SaveRequest.UpdatedBy = Convert.ToInt32(SR_Session.SR_UserID);
                    mod_SaveRequest.RequestedBy = Convert.ToInt32(SR_Session.SR_UserID);
                    mod_Request.LoadModel(mod_PortalRequest);
                    break;
                case "G4SApproval":
                    // Loading fundamental data **********************
                    DBG4SRepository orDb2 = new DBG4SRepository();
                    SR_Session = orDb2.SetSessionValues(SR_Session);
                    mod_SaveG4SApprove.LoadModel(mod_PortalG4SApprove);
                    //mod_SaveG4SApprove.UpdatedBy = Convert.ToInt32(SR_Session.SR_UserID);
                    //mod_SaveG4SApprove.RequestedBy = Convert.ToInt32(SR_Session.SR_UserID);
                    mod_G4SApprove.LoadModel(mod_PortalG4SApprove);
                    break;
                case "CustomerApproval":
                    // Loading fundamental data **********************
                    DBCustRepository orDb3 = new DBCustRepository();
                    SR_Session = orDb3.SetSessionValues(SR_Session);
                    mod_SaveCustomerApprove.LoadModel(mod_PortalCustomerApprove);
                    //mod_SaveCustomerApprove.UpdatedBy = Convert.ToInt32(SR_Session.SR_UserID);
                    //mod_SaveCustomerApprove.RequestedBy = Convert.ToInt32(SR_Session.SR_UserID);
                    mod_CustomerApprove.LoadModel(mod_PortalCustomerApprove);
                    break;
            }

            view_00 = null;
            view_01 = null;
            view_02 = null;
            view_03 = null;
            view_04 = null;
            var view = inflater.Inflate(Resource.Layout.fragment_Sample, container, false);
            return view;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            
            mSlidingTabScrollView = view.FindViewById<SlidingTabScrollView>(Resource.Id.sliding_tabs);
            mViewPager = view.FindViewById<ViewPager>(Resource.Id.viewpager);
            mViewPager.Adapter = new SamplePagerAdapter(ModuleType,RequestID,Activity,FragmentManager, this);

            Loaded_Page1 = false;
            Loaded_Page2 = false;
            Loaded_Page3 = false;
            Loaded_Page4 = false;
            Loaded_Page5 = false;

            mViewPager.PageScrolled += MViewPager_PageScrolled;
            mSlidingTabScrollView.ViewPager = mViewPager;
            

        }

        private void MViewPager_PageScrolled(object sender, ViewPager.PageScrolledEventArgs e)
        {
            if (e.Position != xpos)
            {
                switch (ModuleType)
                {
                    case "RequestApproval":
                        LoadSaveRequest(mod_SaveRequest, view_00, view_01, view_02, view_03);
                        mod_Request = mod_SaveRequest;
                        break;
                    case "G4SApproval":
                        LoadSaveG4SApproval(mod_SaveG4SApprove, view_00, view_01, view_02);
                        break;
                    case "CustomerApproval":
                        LoadSaveCustomerApproval(mod_SaveCustomerApprove, view_00, view_01);
                        break;
                }
                      
                xpos = e.Position;
            }
        }

        public void OnDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
        {
            var date = new DateTime(year, monthOfYear + 1, dayOfMonth);
            
            if (Refclassitem == "btnTerminateDate") { View.FindViewById<TextView>(Resource.Id.dpTerminateDate).Text = date.ToString("yyyy-MMM-dd"); }
            if (Refclassitem == "btnRequestDate") { View.FindViewById<TextView>(Resource.Id.txtRequestDate).Text = date.ToString("yyyy-MMM-dd"); }
            if (Refclassitem == "btnStartDate") { View.FindViewById<TextView>(Resource.Id.txtStartDate).Text = date.ToString("yyyy-MMM-dd"); }
            if (Refclassitem == "btnEndDate") { View.FindViewById<TextView>(Resource.Id.txtEndDate).Text = date.ToString("yyyy-MMM-dd"); }
        }
        public void OnTimeSet(TimePicker view, int hourOfDay, int minute)
        {
            var time = hourOfDay.ToString("D2") + ":" + minute.ToString("D2");

            if (Refclassitem == "btnTerminateTime") { View.FindViewById<TextView>(Resource.Id.txtTerminateTime).Text = time; }
            if (Refclassitem == "btnStartTime") { View.FindViewById<TextView>(Resource.Id.txtStartTime).Text = time; }
            if (Refclassitem == "btnEndTime") { View.FindViewById<TextView>(Resource.Id.txtEndTime).Text = time; }
        }

        private static string GetTime(DateTime oValue)
        {
            string Rtn = string.Empty;
            int hour = oValue.Hour;
            int minute = oValue.Minute;
            return Rtn = hour.ToString("D2") + ":" + minute.ToString("D2");
        }
        public class SamplePagerAdapter : PagerAdapter
        {
            List<string> items = new List<string>();
            //// View Repositories
            //View view_00 = null;
            //View view_01 = null;
            //View view_02 = null;
            //View view_03 = null;
            //View view_04 = null;
            // Models into the class

            STSR_Droid.Models.SR_Request oRequest = new Models.SR_Request();
            List<STSR_Droid.Models.SR_G4SApprove> olstG4SApprove = new List<Models.SR_G4SApprove>();
            
            STSR_Droid.Models.SR_G4SCharges oG4SCharges = new Models.SR_G4SCharges();
            List<STSR_Droid.Models.SR_CustomerApprove> olstCustomerApprove = new List<Models.SR_CustomerApprove>();

            public string ModuleType { get; set; }
            public string RequestID { get; set; }
            public Activity Activity { get; set; }
            public FragmentManager Fragment { get; set; }
            public SlidingTabFragment Sliding { get; set; }

            public SamplePagerAdapter(string oModuleType,string oRequestID, Activity oActivity, FragmentManager oFragment, SlidingTabFragment oSliding) : base()
            {
                
                ModuleType = oModuleType;
                RequestID = oRequestID;
                Activity = oActivity;
                Fragment = oFragment;
                Sliding = oSliding;
                string oUserID = Sliding.SR_Session.SR_UserID.ToString();

                switch (oModuleType)
                {
                    case "RequestApproval":
                        items.Add("Customer");
                        items.Add("Request Info");
                        items.Add("Contact");
                        items.Add("Order");
                        items.Add("Action");
                        LoadRequest(RequestID);
                        break;
                    case "G4SApproval":
                        items.Add("Request Info");
                        items.Add("Additional Charges");
                        items.Add("Choice");
                        items.Add("Action");
                        LoadG4S(RequestID, oUserID);
                        break;
                    case "CustomerApproval":
                        items.Add("Request Info");
                        items.Add("Additional Charges");
                        items.Add("Action");
                        LoadCustomer(RequestID, oUserID);
                        break;
                    default:
                        break;
                }

            }

            public override int Count
            {
                get { return items.Count; }
            }

            public override bool IsViewFromObject(View view, Java.Lang.Object obj)
            {
                return view == obj;
            }

            public static string SR_Action { get; set; }

            public override Java.Lang.Object InstantiateItem(ViewGroup container, int position)
            {
                //string tr = Mod
                View view = null;
                if (ModuleType == "RequestApproval" && (view_00 == null || view_01 == null || view_02 == null || view_03 == null || view_04 == null))
                {
                    SR_Action = Sliding.SR_Session.SR_Action;
                    xSliding = Sliding;
                    xActivity = Activity;
                    xFragment = Fragment;

                    view_00 = LayoutInflater.From(container.Context).Inflate(Resource.Layout.SR_RequestPagerItem_01, container, false);
                    view_01 = LayoutInflater.From(container.Context).Inflate(Resource.Layout.SR_RequestPagerItem_02, container, false);
                    view_02 = LayoutInflater.From(container.Context).Inflate(Resource.Layout.SR_RequestPagerItem_03, container, false);
                    view_03 = LayoutInflater.From(container.Context).Inflate(Resource.Layout.SR_RequestPagerItem_04, container, false);
                    view_04 = LayoutInflater.From(container.Context).Inflate(Resource.Layout.SR_RequestPagerItem_05, container, false);
                    
                    InitRequestControl_01(view_00);
                    LoadRequestControl_01(view_00);
                    InitRequestControl_02(view_01);
                    LoadRequestControl_02(view_01);
                    InitRequestControl_03(view_02);
                    LoadRequestControl_03(view_02);
                    InitRequestControl_04(view_03);
                    LoadRequestControl_04(view_03);
                    InitRequestControl_05(view_04);
                    LoadRequestControl_05(view_04);
                }
                if (ModuleType == "G4SApproval" && (view_00 == null || view_01 == null || view_02 == null || view_03 == null))
                {
                    SR_Action = Sliding.SR_Session.SR_Action;
                    xSliding = Sliding;
                    xActivity = Activity;
                    xFragment = Fragment;

                    //string oUserID = xSliding.SR_Session.SR_UserID.ToString();

                    view_00 = LayoutInflater.From(container.Context).Inflate(Resource.Layout.SR_G4SApprovalPagerItem_01, container, false);
                    view_01 = LayoutInflater.From(container.Context).Inflate(Resource.Layout.SR_G4SApprovalPagerItem_02, container, false);
                    view_02 = LayoutInflater.From(container.Context).Inflate(Resource.Layout.SR_G4SApprovalPagerItem_03, container, false);
                    view_03 = LayoutInflater.From(container.Context).Inflate(Resource.Layout.SR_G4SApprovalPagerItem_04, container, false);

                    //LoadG4S(RequestID, oUserID);
                    InitG4SControl_01(view_00);
                    LoadG4SControl_01(view_00);
                    InitG4SControl_02(view_01);
                    LoadG4SControl_02(view_01);
                    InitG4SControl_03(view_02);
                    LoadG4SControl_03(view_02);
                    InitG4SControl_04(view_03);
                    LoadG4SControl_04(view_03);
                    
                }
                if (ModuleType == "CustomerApproval" && (view_00 == null || view_01 == null || view_02 == null ))
                {
                    SR_Action = Sliding.SR_Session.SR_Action;
                    xSliding = Sliding;
                    xActivity = Activity;
                    xFragment = Fragment;

                    //string oUserID = xSliding.SR_Session.SR_UserID.ToString();

                    view_00 = LayoutInflater.From(container.Context).Inflate(Resource.Layout.SR_CustomerApprovalPagerItem_01, container, false);
                    view_01 = LayoutInflater.From(container.Context).Inflate(Resource.Layout.SR_CustomerApprovalPagerItem_02, container, false);
                    view_02 = LayoutInflater.From(container.Context).Inflate(Resource.Layout.SR_CustomerApprovalPagerItem_03, container, false);

                    //LoadCustomer(RequestID, oUserID);
                    InitCustomerControl_01(view_00);
                    LoadCustomerControl_01(view_00);
                    InitCustomerControl_02(view_01);
                    LoadCustomerControl_02(view_01);
                    InitCustomerControl_03(view_02);
                    LoadCustomerControl_03(view_02);
                    
                }
                
                //Addition to Class. 
                switch (ModuleType)
                {
                    case "RequestApproval":
                        switch (position)
                        {
                            case 0:
                                view = view_00;
                                container.AddView(view);
                                break;
                            case 1:
                                view = view_01;
                                container.AddView(view);
                                break;
                            case 2:
                                view = view_02;
                                container.AddView(view);
                                break;
                            case 3:
                                view = view_03;
                                container.AddView(view);
                                break;
                            case 4:
                                view = view_04;
                                container.AddView(view);
                                break;
                            default:
                                view = view_00;
                                container.AddView(view);
                                break;
                        }
                        break;
                    case "G4SApproval":
                        switch (position)
                        {
                            case 0:
                                view = view_00;
                                container.AddView(view);
                                break;
                            case 1:
                                view = view_01;
                                container.AddView(view);
                                break;
                            case 2:
                                view = view_02;
                                container.AddView(view);
                                break;
                            case 3:
                                view = view_03;
                                container.AddView(view);
                                break;
                            default:
                                view = view_00;
                                container.AddView(view);
                                break;
                        }
                        break;
                    case "CustomerApproval":
                        switch (position)
                        {
                            case 0:
                                view = view_00;
                                container.AddView(view);
                                break;
                            case 1:
                                view = view_01;
                                container.AddView(view);
                                break;
                            case 2:
                                view = view_02;
                                container.AddView(view);
                                break;
                            default:
                                view = view_00;
                                container.AddView(view);
                                break;
                        }
                        break;
                    default:
                        break;
                }
                int pos = position + 1;
                return view;
            }


            public string GetHeaderTitle(int position)
            {
                return items[position];
            }

            public override void DestroyItem(ViewGroup container, int position, Java.Lang.Object obj)
            {
                container.RemoveView((View)obj);
            }

            //public void cboCustomer_ItemClickListener(View oView, EventArgs e)

            private void LoadRequest(string oRequestID)
            {
                DBRequestRepository dbr = new DBRequestRepository();
                var oRepRequest = dbr.SR_GetRequestbyID(oRequestID);
                if (oRepRequest != null)
                {
                    oRequest.LoadModel(oRepRequest.FirstOrDefault<SRWS_ASMX.SR_RequestItem>());
                }
            }

            private void LoadG4S(string oRequestID, string oUserID)
            {
                
                DBG4SRepository dbr = new DBG4SRepository();
                List<SRWS_ASMX.SR_G4SApprove> oRepG4S = dbr.SR_SPWorkflow_SubmitbyRequestID(oUserID,oRequestID);
                if (oRepG4S != null)
                {
                    //oG4SApprove.LoadModel(oRepG4S.FirstOrDefault<SRWS_ASMX.SR_G4SApprove>());
                    //foreach (var item in oRepG4S)
                    //{
                    STSR_Droid.Models.SR_G4SApprove oG4SApprove = new Models.SR_G4SApprove();
                    oG4SApprove.LoadModel(oRepG4S);
                    olstG4SApprove.Add(oG4SApprove);
                    //}
                }

            }

            private void LoadCustomer(string oRequestID, string oUserID)
            {
                
                DBCustRepository dbr = new DBCustRepository();
                List<SRWS_ASMX.SR_CustomerApprove> oRepCust = dbr.SR_SPWorkflow_SubmitbyCustRequestID(oUserID, oRequestID);
                if (oRepCust != null)
                {
                    //oRequest.LoadModel(oRepRequest.FirstOrDefault<SRWS_ASMX.SR_RequestItem>());
                    //foreach (var item in oRepCust)
                    //{
                    STSR_Droid.Models.SR_CustomerApprove oCustomerApprove = new Models.SR_CustomerApprove();
                    oCustomerApprove.LoadModel(oRepCust);
                    olstCustomerApprove.Add(oCustomerApprove);
                    //}
                }
            }

        }
    }
}
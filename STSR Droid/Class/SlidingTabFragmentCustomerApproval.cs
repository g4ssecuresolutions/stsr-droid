using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using Java.Lang;
using STSR_Droid.Models;
using STSR_Droid.ORM;

using SR_Class;
using SQLite;
using STSR_Droid.SQLiteTables;


namespace STSR_Droid.Class
{
    public partial class SlidingTabFragment : Fragment, DatePickerDialog.IOnDateSetListener, TimePickerDialog.IOnTimeSetListener
    {
        #region "Init Request Control"
        //*************************************************************************************************************
        //  Init Customer Control
        //*************************************************************************************************************

        private static void InitCustomerControl_01(View oview)
        {
        }
        private static void InitCustomerControl_02(View oview)
        {
            TextView txtNotes = oview.FindViewById<TextView>(Resource.Id.txtNotes);
            txtNotes.Click += TxtNotes_Click;

            CheckBox chkQuestion = oview.FindViewById<CheckBox>(Resource.Id.chkAdditionalChargesQuestion);
            chkQuestion.CheckedChange += ChkQuestion_CheckedChange;
        }

        private static void ChkQuestion_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            string oMessage = "";
            if (e.IsChecked)
            {
                Dialog dialog = new Dialog(xActivity);
                dialog.SetTitle("Send a question:");
                dialog.SetContentView(Resource.Layout.SR_PopEditBox);
                dialog.Show();

                EditText txtSource = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                TextView txtTarget = (TextView)xActivity.FindViewById(Resource.Id.txtNotes);
                Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
                Button btnclear = (Button)dialog.FindViewById(Resource.Id.btnClear);
                Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);

                txtSource.Text = txtTarget.Text;

                if (btncancel != null)
                {
                    btncancel.Click += (osender, Args) =>
                    {
                        oMessage = "Cancel Editor";
                        Toast.MakeText(xActivity, oMessage, ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }
                if (btnclear != null)
                {
                    btnclear.Click += (osender, Args) =>
                    {
                        EditText txtSourceNote = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                        txtSourceNote.Text = "";
                        oMessage = "Clear Editor";
                        Toast.MakeText(xActivity, oMessage, ToastLength.Short).Show();
                    };
                }
                if (btnset != null)
                {
                    btnset.Click += (osender, Args) =>
                    {
                        EditText txtSourceNote = (EditText)dialog.FindViewById(Resource.Id.txtNote);

                        //Send question by email (txtSourceNote.Text)
                        ORM.DBCustRepository orb = new DBCustRepository();
                        bool oResponse = orb.SendQuestionRequest(mod_SaveCustomerApprove,txtSourceNote.Text);
                        //oDatabox.Query = oQuery.SaveNewHoldRequest(oRequest)
                        if (oResponse)
                        {
                            oMessage = "Your question had been sent by email.";
                        }
                        else {
                            oMessage = "Your question couldn't be sent by email.";
                        }

                        //String NoteText = txtNote.Text;
                        Toast.MakeText(xActivity, oMessage, ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }
            }

        }

        private static void TxtNotes_Click(object sender, EventArgs e)
        {
            Dialog dialog = new Dialog(xActivity);
            dialog.SetTitle("Notes:");
            dialog.SetContentView(Resource.Layout.SR_PopEditBox);
            dialog.Show();
            
            EditText txtSource = (EditText)dialog.FindViewById(Resource.Id.txtNote);
            TextView txtTarget = (TextView)xActivity.FindViewById(Resource.Id.txtNotes);
            Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
            Button btnclear = (Button)dialog.FindViewById(Resource.Id.btnClear);
            Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);

            txtSource.Text = txtTarget.Text ;

            if (btncancel != null)
            {
                btncancel.Click += (osender, Args) =>
                {
                    Toast.MakeText(xActivity, "Cancel Editor", ToastLength.Short).Show();
                    dialog.Cancel();
                };
            }
            if (btnclear != null)
            {
                btnclear.Click += (osender, Args) =>
                {
                    EditText txtSourceNote = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                    txtSourceNote.Text = "";
                    Toast.MakeText(xActivity, "Clear Editor", ToastLength.Short).Show();
                };
            }
            if (btnset != null)
            {
                btnset.Click += (osender, Args) =>
                {
                    EditText txtSourceNote = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                    TextView txtTargetNote = (TextView)xActivity.FindViewById(Resource.Id.txtNotes);
                    txtTargetNote.Text = txtSourceNote.Text;
                    //String NoteText = txtNote.Text;
                    Toast.MakeText(xActivity, "Set Editor", ToastLength.Short).Show();
                    dialog.Cancel();
                };
            }
        }

        private static void InitCustomerControl_03(View oview)
        {
            if (!Loaded_Page3)
            {
                Button btnCustomer_Save = oview.FindViewById<Button>(Resource.Id.btnUpdate);
                Button btnCustomer_Cancel = oview.FindViewById<Button>(Resource.Id.btnCancel);

                btnCustomer_Save.Click += BtnCustomer_Save_Click;
                btnCustomer_Cancel.Click += BtnCustomer_Cancel_Click;

                Loaded_Page3 = true;
            };
        }
        #region "User Interface Renders"
        private static void BtnCustomer_Cancel_Click(object sender, EventArgs e)
        {
            Toast.MakeText(xActivity, "Request Cancelled", ToastLength.Short).Show();
            xActivity.Finish();
        }

        private static void BtnCustomer_Save_Click(object sender, EventArgs e)
        {
            string oMessage = "Cannot reach database. System detected a problem.";

            CheckBox chkAdditionalChargesApproved = xActivity.FindViewById<CheckBox>(Resource.Id.chkAdditionalChargesApproved);
            CheckBox chkAdditionalChargesDenied = xActivity.FindViewById<CheckBox>(Resource.Id.chkAdditionalChargesDenied);

            if (chkAdditionalChargesApproved.Checked || chkAdditionalChargesDenied.Checked)
            {
                mod_SaveRequest.RequestStatusID = (int)StatusID.Submitted;
            }
            

            mod_SaveRequest.AppdCharges = chkAdditionalChargesApproved.Checked;
            mod_SaveRequest.AddlChargesDenied = chkAdditionalChargesDenied.Checked;


            ORM.DBCustRepository orb = new DBCustRepository();
            oMessage = orb.SaveCustApproveRequest(mod_SaveCustomerApprove,mod_CustomerApprove);

            Toast.MakeText(xActivity, oMessage, ToastLength.Short).Show();
            xActivity.Finish();
        }
        #endregion
        #endregion
        #region "Load Request Control"
        //*************************************************************************************************************
        //  Load Customer Control
        //*************************************************************************************************************
        private static void LoadCustomerControl_01(View oview)
        {
            TextView txtRequestID = oview.FindViewById<TextView>(Resource.Id.txtRequestID);
            TextView txtSiteName = oview.FindViewById<TextView>(Resource.Id.txtSiteName);
            TextView txtAddress1 = oview.FindViewById<TextView>(Resource.Id.txtAddress1);
            TextView txtCity = oview.FindViewById<TextView>(Resource.Id.txtCity);
            TextView txtState = oview.FindViewById<TextView>(Resource.Id.txtState);
            TextView txtContact = oview.FindViewById<TextView>(Resource.Id.txtContact);
            TextView txtOfficerType = oview.FindViewById<TextView>(Resource.Id.txtOfficerType);
            TextView txtOfficerRequired = oview.FindViewById<TextView>(Resource.Id.txtOfficerRequired);
            TextView txtStartDate = oview.FindViewById<TextView>(Resource.Id.txtStartDate);
            TextView txtEndDate = oview.FindViewById<TextView>(Resource.Id.txtEndDate);
            TextView txtDepartment = oview.FindViewById<TextView>(Resource.Id.txtDepartment);
            TextView txtWageRate = oview.FindViewById<TextView>(Resource.Id.txtWageRate);
            TextView txtBillRate = oview.FindViewById<TextView>(Resource.Id.txtBillRate);

            txtRequestID.Text = mod_SaveCustomerApprove.SR_RequestID.ToString();
            txtSiteName.Text = mod_SaveCustomerApprove.CustName.ToString();
            txtAddress1.Text = mod_SaveCustomerApprove.Address1.ToString();
            txtCity.Text = mod_SaveCustomerApprove.City.ToString();
            txtState.Text = mod_SaveCustomerApprove.State.ToString();
            txtContact.Text = mod_SaveCustomerApprove.Contact.ToString();
            txtOfficerType.Text = mod_SaveCustomerApprove.OfficerType.ToString();
            txtOfficerRequired.Text = mod_SaveCustomerApprove.OfficerNum.ToString();
            txtStartDate.Text = mod_SaveCustomerApprove.StartDate.ToString();
            txtEndDate.Text = mod_SaveCustomerApprove.EndDate.ToString();
            txtDepartment.Text = mod_SaveCustomerApprove.DeptID.ToString();
            txtWageRate.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveCustomerApprove.RequestRate.ToString()));
            txtBillRate.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveCustomerApprove.RequestBill.ToString()));


        }
        private static void LoadCustomerControl_02(View oview)
        {
            TextView txtAdditionalCharges = oview.FindViewById<TextView>(Resource.Id.txtAdditionalCharges);
            TextView txtHotelAmount = oview.FindViewById<TextView>(Resource.Id.txtHotelAmount);
            TextView txtPerDiemAmount = oview.FindViewById<TextView>(Resource.Id.txtPerDiemAmount);
            TextView txtMileageAmount = oview.FindViewById<TextView>(Resource.Id.txtMileageAmount);
            TextView txtCarRentalAmount = oview.FindViewById<TextView>(Resource.Id.txtCarRentalAmount);
            TextView txtOtherAmount = oview.FindViewById<TextView>(Resource.Id.txtOtherAmount);

            CheckBox chkAdditionalChargesApproved = oview.FindViewById<CheckBox>(Resource.Id.chkAdditionalChargesApproved);
            CheckBox chkAdditionalChargesDenied = oview.FindViewById<CheckBox>(Resource.Id.chkAdditionalChargesDenied);
            CheckBox chkAdditionalChargesQuestion = oview.FindViewById<CheckBox>(Resource.Id.chkAdditionalChargesQuestion);

            TextView txtNotes = oview.FindViewById<TextView>(Resource.Id.txtNotes);


            //txtAdditionalCharges.Text = mod_SaveCustomerApprove.AddlCharges.ToString();
            txtHotelAmount.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveCustomerApprove.HotelAmount.ToString()));
            txtPerDiemAmount.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveCustomerApprove.PerDiemAmount.ToString()));
            txtMileageAmount.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveCustomerApprove.MileageAmount.ToString()));
            txtCarRentalAmount.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveCustomerApprove.CarRentalAmount.ToString()));
            txtOtherAmount.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveCustomerApprove.OtherAmount.ToString()));


            chkAdditionalChargesApproved.Checked = mod_SaveRequest.AppdCharges;
            chkAdditionalChargesDenied.Checked = mod_SaveRequest.AddlChargesDenied;
            //chkAdditionalChargesQuestion.Checked = mod_SaveRequest.;


            txtNotes.Text = mod_SaveCustomerApprove.Notes.ToString();


        }
        private static void LoadCustomerControl_03(View oview)
        {

        }
        #endregion
        #region "Load Save Module"

        
        static void LoadSaveCustomerApproval(SR_CustomerApprove oRequest, View oView_00, View oView_01)
        {
            // *** Page 01 ***************************************************************************
            TextView txtRequestID = oView_01.FindViewById<TextView>(Resource.Id.txtRequestID);
            TextView txtSiteName = oView_01.FindViewById<TextView>(Resource.Id.txtSiteName);
            TextView txtAddress1 = oView_01.FindViewById<TextView>(Resource.Id.txtAddress1);
            TextView txtCity = oView_01.FindViewById<TextView>(Resource.Id.txtCity);
            TextView txtState = oView_01.FindViewById<TextView>(Resource.Id.txtState);
            TextView txtContact = oView_01.FindViewById<TextView>(Resource.Id.txtContact);
            TextView txtOfficerType = oView_01.FindViewById<TextView>(Resource.Id.txtOfficerType); 
            TextView txtOfficerRequired = oView_01.FindViewById<TextView>(Resource.Id.txtOfficerRequired);
            TextView txtStartDate = oView_01.FindViewById<TextView>(Resource.Id.txtStartDate);
            TextView txtEndDate = oView_01.FindViewById<TextView>(Resource.Id.txtEndDate);
            TextView txtDepartment = oView_01.FindViewById<TextView>(Resource.Id.txtDepartment);
            TextView txtWageRate = oView_01.FindViewById<TextView>(Resource.Id.txtWageRate);
            TextView txtBillRate = oView_01.FindViewById<TextView>(Resource.Id.txtBillRate);


            // *** Page 02 ***************************************************************************

            TextView txtAdditionalCharges = oView_01.FindViewById<TextView>(Resource.Id.txtAdditionalCharges);
            TextView txtHotelAmount = oView_01.FindViewById<TextView>(Resource.Id.txtHotelAmount);
            TextView txtPerDiemAmount = oView_01.FindViewById<TextView>(Resource.Id.txtPerDiemAmount);
            TextView txtMileageAmount = oView_01.FindViewById<TextView>(Resource.Id.txtMileageAmount);
            TextView txtCarRentalAmount = oView_01.FindViewById<TextView>(Resource.Id.txtCarRentalAmount);
            TextView txtOtherAmount = oView_01.FindViewById<TextView>(Resource.Id.txtOtherAmount);

            CheckBox chkAdditionalChargesApproved = oView_01.FindViewById<CheckBox>(Resource.Id.chkAdditionalChargesApproved);
            CheckBox chkAdditionalChargesDenied = oView_01.FindViewById<CheckBox>(Resource.Id.chkAdditionalChargesDenied);
            CheckBox chkAdditionalChargesQuestion = oView_01.FindViewById<CheckBox>(Resource.Id.chkAdditionalChargesQuestion);

            TextView txtNotes = oView_01.FindViewById<TextView>(Resource.Id.txtNotes);


            // *** Loading Module ********************************************************************
            // *** Page 01 ***************************************************************************
            //if (cboCustomer != null) { mod_SaveRequest.CorpCustID = cboCustomer.ItemSelected(); };
            //if (cboSite != null) { };

            if (txtRequestID != null) { mod_SaveCustomerApprove.SR_RequestID = Convert.ToInt32(txtRequestID.Text); };
            if (txtSiteName != null) { mod_SaveCustomerApprove.CustName = txtSiteName.Text; };
            if (txtAddress1 != null) { mod_SaveCustomerApprove.Address1 = txtAddress1.Text; };
            if (txtCity != null) { mod_SaveCustomerApprove.City = txtCity.Text; };
            if (txtState != null) { mod_SaveCustomerApprove.State = txtState.Text; };
            if (txtContact != null) { mod_SaveCustomerApprove.Contact = txtContact.Text; };
            if (txtOfficerType != null) { mod_SaveCustomerApprove.OfficerType = txtOfficerType.Text; };
            if (txtOfficerRequired != null) { mod_SaveCustomerApprove.OfficerNum = Convert.ToInt32(txtOfficerRequired.Text); };
            if (txtStartDate != null) { mod_SaveCustomerApprove.StartDate = Convert.ToDateTime(txtStartDate.Text); };
            if (txtEndDate != null) { mod_SaveCustomerApprove.EndDate = Convert.ToDateTime(txtEndDate.Text); };
            if (txtDepartment != null) { mod_SaveCustomerApprove.DeptID = txtDepartment.Text; };
            //if (txtWageRate != null) { mod_SaveCustomerApprove.RequestRate = double.Parse(txtWageRate.Text, System.Globalization.NumberStyles.Currency); };
            //if (txtBillRate != null) { mod_SaveCustomerApprove.RequestBill = double.Parse(txtBillRate.Text, System.Globalization.NumberStyles.Currency); };

            if (txtWageRate != null) { if (txtWageRate.Text == "") { mod_SaveCustomerApprove.RequestRate = 0; } else { mod_SaveCustomerApprove.RequestRate = double.Parse(txtWageRate.Text, System.Globalization.NumberStyles.Currency); } };
            if (txtBillRate != null) { if (txtBillRate.Text == "") { mod_SaveCustomerApprove.RequestBill = 0; } else { mod_SaveCustomerApprove.RequestBill = double.Parse(txtBillRate.Text, System.Globalization.NumberStyles.Currency); } };


            // *** Page 02 ***************************************************************************

            //if (txtAdditionalCharges != null) { mod_SaveCustomerApprove.AddlCharges = Convert.ToBoolean(txtAdditionalCharges.Text); };
            //if (txtHotelAmount != null) { mod_SaveCustomerApprove.HotelAmount = double.Parse(txtHotelAmount.Text, System.Globalization.NumberStyles.Currency); };
            //if (txtPerDiemAmount != null) { mod_SaveCustomerApprove.PerDiemAmount = double.Parse(txtPerDiemAmount.Text, System.Globalization.NumberStyles.Currency); };
            //if (txtMileageAmount != null) { mod_SaveCustomerApprove.MileageAmount = double.Parse(txtMileageAmount.Text, System.Globalization.NumberStyles.Currency); };
            //if (txtCarRentalAmount != null) { mod_SaveCustomerApprove.CarRentalAmount = double.Parse(txtCarRentalAmount.Text, System.Globalization.NumberStyles.Currency); };
            //if (txtOtherAmount != null) { mod_SaveCustomerApprove.OtherAmount = double.Parse(txtOtherAmount.Text, System.Globalization.NumberStyles.Currency); };

            if (txtHotelAmount != null) { if (txtHotelAmount.Text == "") { mod_SaveCustomerApprove.HotelAmount = 0; } else { mod_SaveCustomerApprove.HotelAmount = double.Parse(txtHotelAmount.Text, System.Globalization.NumberStyles.Currency); } };
            if (txtPerDiemAmount != null) { if (txtPerDiemAmount.Text == "") { mod_SaveCustomerApprove.PerDiemAmount = 0; } else { mod_SaveCustomerApprove.PerDiemAmount = double.Parse(txtPerDiemAmount.Text, System.Globalization.NumberStyles.Currency); } };
            if (txtMileageAmount != null) { if (txtMileageAmount.Text == "") { mod_SaveCustomerApprove.MileageAmount = 0; } else { mod_SaveCustomerApprove.MileageAmount = double.Parse(txtMileageAmount.Text, System.Globalization.NumberStyles.Currency); } };
            if (txtCarRentalAmount != null) { if (txtCarRentalAmount.Text == "") { mod_SaveCustomerApprove.CarRentalAmount = 0; } else { mod_SaveCustomerApprove.CarRentalAmount = double.Parse(txtCarRentalAmount.Text, System.Globalization.NumberStyles.Currency); } };
            if (txtOtherAmount != null) { if (txtOtherAmount.Text == "") { mod_SaveCustomerApprove.OtherAmount = 0; } else { mod_SaveCustomerApprove.OtherAmount = double.Parse(txtOtherAmount.Text, System.Globalization.NumberStyles.Currency); } };


            if (chkAdditionalChargesApproved != null) { mod_SaveCustomerApprove.AppdCharges = chkAdditionalChargesApproved.Checked; };
            if (chkAdditionalChargesDenied != null) { mod_SaveCustomerApprove.DeniedCharges = chkAdditionalChargesDenied.Checked; };
            if (chkAdditionalChargesQuestion != null) { mod_SaveCustomerApprove.QuestCharges = chkAdditionalChargesQuestion.Checked; };

            if (txtNotes != null) { mod_SaveCustomerApprove.Notes = txtNotes.Text; };

        }
        #endregion
    }
}
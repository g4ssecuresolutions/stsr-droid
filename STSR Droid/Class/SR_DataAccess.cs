using System;
using System.Data;
using System.IO;
using SQLite;
//using System.Web.Services;
using SR_Class;
using STSR_Droid;
using STSR_Droid.SQLiteTables;
//using STSR_Droid.SRWSServices;



namespace SR_Class
{
    
    public class SR_DataAccess: SR_ServiceManage
    {
        public bool IsEULA { get; set; }

        public SQLiteConnection CreateSystemDB()
        {
            string dbPath = Path.Combine(Environment.GetFolderPath
                (Environment.SpecialFolder.Personal), "sqliteSystem.db3");
            var db = new SQLiteConnection(dbPath);
            return db;
        }
        public SQLiteConnection CreateBackupDB()
        {
            string dbPath = Path.Combine(Environment.GetFolderPath
                (Environment.SpecialFolder.Personal), "sqliteBackup.db3");
            var db = new SQLiteConnection(dbPath);
            return db;
        }


        public bool FirstTime(string oUserID)
        {
            bool Rtn = false;
            try
            {
                string oCnt = string.Empty;
                //var vData = ServiceConnection().SR_LoginFirstTime(oUserID);

                //foreach (var item in vData)
                //{
                //    oCnt = item.ToString();
                //}
                if (oCnt == "1") { Rtn = true; } else { Rtn = false; }
            }
            catch { Rtn = false; }

            return Rtn;
        }

        public string CreateSessionTable()
        {
            try
            {
                SQLiteConnection db = CreateSystemDB();
                db.CreateTable<TblSession>();
                return "SUCCESS";
            }
            catch (Exception e)
            {
                return "Error: " + e.Message;
            }
        }
        public string CleanSessionTable()
        {
            try
            {
                SQLiteConnection DBSystemConnection = CreateSystemDB();
                DBSystemConnection.CreateTable<TblSession>();
                var tbl = DBSystemConnection.Table<TblSession>();

                if (tbl.Count() != 0)
                {
                    IsEULA = tbl.FirstOrDefault().SR_EULA;
                }

                //SQLiteConnection db = CreateSystemDB();
                DBSystemConnection.Execute("DELETE FROM TblSession");

                return "SUCCESS";
            }
            catch (Exception e)
            {
                return "Error: " + e.Message;
            }
        }

        public static string CreateTable<T>() where T : new()
        {
            try { 
            SR_DataAccess da = new SR_DataAccess();
            SQLiteConnection db = da.CreateBackupDB();
            db.CreateTable<T> ();
            return "SUCCESS";
        }
            catch (Exception e)
            {
                return "Error: " + e.Message;
            }
}

        public static string CleanTable<T>(string strTable) where T : new()
        {
            try { 
            SR_DataAccess da = new SR_DataAccess();
            SQLiteConnection db = da.CreateBackupDB();
            db.Execute("DELETE FROM " + strTable);

            return "SUCCESS";
        }
            catch (Exception e)
            {
                return "Error: " + e.Message;
            }
        }
        //private bool checkSessionTable()
        //{

        //    SQLiteConnection checkDB = null;
        //    try
        //    {
        //        string dbPath = Path.Combine(Environment.GetFolderPath
        //        (Environment.SpecialFolder.Personal), "sqliteSystem.db3");

        //        var db = new SQLiteConnection(dbPath);
                

        //        String myPath = DB_PATH + DB_NAME;
        //        checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        //    }
        //    catch (SQLiteException e)
        //    {

        //        //database does't exist yet.
        //    }

        //    if (checkDB != null)
        //    {
        //        checkDB.close();
        //    }

        //    return checkDB != null ? true : false;

        //}
    }
    
}
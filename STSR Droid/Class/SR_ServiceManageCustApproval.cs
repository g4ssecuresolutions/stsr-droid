using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Services.Protocols;
using System.Security.Cryptography.X509Certificates;
using SR_Class;
using SRWS_ASMX;
using STSR_Droid.SRWSServices;
using System.Data;



namespace SR_Class
{
    public partial class SR_ServiceManage : ISoapServices
    {
        public List<SRWS_ASMX.SR_RequestItem> SR_Workflow_ClientSelect(string oUserID, string oUserCustAccess)
        {
            SR_DataConversion dc = new SR_DataConversion();
            RequestItems = new List<SRWS_ASMX.SR_RequestItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getrequestitem = oService.SR_Workflow_ClientSelect(oUserID, oUserCustAccess);
                foreach (var item in getrequestitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    RequestItems.Add(dc.ASMX_RequestItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return RequestItems;
        }

        public string SaveCustApproveRequest(STSR_Droid.Models.SR_Session oSession, STSR_Droid.Models.SR_CustomerApprove oRequest, STSR_Droid.Models.SR_CustomerApprove oUnchangedRequest, int oUserID)
        {
            string Rtn = "";
            SR_DataConversion dc = new SR_DataConversion();

            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                Rtn = oService.SR_btnSaveCustomerApprove(oSession.ToSRWSServices(), dc.SR_CustAppToSrvItem(oRequest, oUserID), dc.SR_CustAppToSrvItem(oUnchangedRequest, oUserID));

            }
            catch (Exception ex)
            {
                Rtn = "";
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }

        public bool SendQuestionRequest(STSR_Droid.Models.SR_CustomerApprove oRequest,string oMessage, int oUserID)
        {
            bool Rtn = false;
            SR_DataConversion dc = new SR_DataConversion();

            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                Rtn = oService.sendEmail_Charge(oRequest.SR_RequestID, oUserID, oMessage);

            }
            catch (Exception ex)
            {
                Rtn = false;
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }
    }
}
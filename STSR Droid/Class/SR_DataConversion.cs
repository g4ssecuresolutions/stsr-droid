using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SRWS_ASMX;
using STSR_Droid.Models;


namespace SR_Class
{
    public class SR_DataConversion
    {
        public SR_GetOfficeItem ASMX_OfficeItem_to_item(STSR_Droid.SRWSServices.SR_Office item)
        {
            SR_GetOfficeItem Rtn = new SR_GetOfficeItem();
            Rtn.DeptID = item.DeptID;
            Rtn.Descr = item.Descr;
            Rtn.Address1 = item.Address1;
            Rtn.Address2 = item.Address2;
            Rtn.City = item.City;
            Rtn.State = item.State;
            Rtn.Latitude = item.Latitude;
            Rtn.Longitude = item.Longitude;
            Rtn.TimeZone = item.TimeZone;
            Rtn.UserID_01 = item.UserID_01;
            Rtn.UserID_02 = item.UserID_02;
            Rtn.UserID_03 = item.UserID_03;
            Rtn.UserID_04 = item.UserID_04;
            return Rtn;
        }
        public STSR_Droid.SRWSServices.SR_Office OfficeItem_to_ASMX_item(SR_Office item)
        {
            STSR_Droid.SRWSServices.SR_Office Rtn = new STSR_Droid.SRWSServices.SR_Office();
            Rtn.DeptID = item.DeptID;
            Rtn.Descr = item.Descr;
            Rtn.Address1 = item.Address1;
            Rtn.Address2 = item.Address2;
            Rtn.City = item.City;
            Rtn.State = item.State;
            Rtn.Latitude = item.Latitude;
            Rtn.Longitude = item.Longitude;
            Rtn.TimeZone = item.TimeZone;
            Rtn.UserID_01 = item.UserID_01;
            Rtn.UserID_02 = item.UserID_02;
            Rtn.UserID_03 = item.UserID_03;
            Rtn.UserID_04 = item.UserID_04;
            return Rtn;
        }
        public SRWS_ASMX.SR_UserItem ASMX_UserItem_to_item(STSR_Droid.SRWSServices.SR_User item)
        {
            SRWS_ASMX.SR_UserItem Rtn = new SRWS_ASMX.SR_UserItem();
            Rtn.SR_UserID = item.SR_UserID;
            Rtn.SR_RoleID = item.SR_RoleID;
            Rtn.FullName = item.FullName;
            Rtn.WFApprover = item.WFApprover ? "True" : "False";
            Rtn.WFExtender = item.WFExtender ? "True" : "False";
            Rtn.ClientApprover = item.ClientApprover ? "True" : "False";
            Rtn.ClientExtender = item.ClientExtender ? "True" : "False";
            Rtn.Active = item.Active ? "True" : "False";
            Rtn.CorpCustID = item.CorpCustID;
            Rtn.ResetPassword = item.ResetPassword ? "True" : "False";
            Rtn.NUPassword = item.NUPassword ? "True" : "False";

            return Rtn;
        }
        public STSR_Droid.SRWSServices.SR_User UserItem_to_ASMX_item(SR_UserItem item)
        {
            STSR_Droid.SRWSServices.SR_User Rtn = new STSR_Droid.SRWSServices.SR_User();

            Rtn.SR_UserID = item.SR_UserID;
            Rtn.SR_RoleID = item.SR_RoleID;
            //Rtn.userName = item.UserName;
            //Rtn.empName = item.EmpName;
            //Rtn.password = item.Password;
            Rtn.FullName = item.FullName;
            //Rtn.firstName = item.FirstName;
            //Rtn.lastName = item.LastName;
            //Rtn.emailAddress = item.EmailAddress;
            //Rtn.sendEmail = item.SendEmail;
            Rtn.WFApprover = item.WFApprover == "True" ? true : false;
            Rtn.WFExtender = item.WFExtender == "True" ? true : false;
            Rtn.ClientApprover = item.ClientApprover == "True" ? true : false;
            Rtn.ClientExtender = item.ClientExtender == "True" ? true : false;
            Rtn.Active = item.Active == "True" ? true : false;
            Rtn.CorpCustID = item.CorpCustID;
            //Rtn.deptID = item.DeptID;
            Rtn.ResetPassword = item.ResetPassword == "True" ? true : false;
            Rtn.NUPassword = item.NUPassword == "True" ? true : false;
            //Rtn.note = item.Note;
            return Rtn;
        }
        public SR_RequestItem ASMX_RequestItem_to_item(STSR_Droid.SRWSServices.SR_Request item)
        {
            SR_RequestItem Rtn = new SR_RequestItem();

            Rtn.RequestID = item.RequestID;
            Rtn.SiteID = item.SiteID;
            Rtn.CorpCustID = item.CorpCustID;
            Rtn.ManualEntry = item.ManualEntry;
            Rtn.SiteName = item.SiteName;
            Rtn.Address1 = item.Address1;
            Rtn.Address2 = item.Address2;
            Rtn.City = item.City;
            Rtn.State = item.State;
            Rtn.ZipCode = item.ZipCode;
            Rtn.Latitude = Convert.ToDouble(item.Latitude);
            Rtn.Longitude = Convert.ToDouble(item.Longitude);
            Rtn.Contact = item.Contact;
            Rtn.ContactPhone = item.ContactPhone;

            Rtn.RequesterName = item.RequesterName;
            Rtn.RequesterPhone = item.RequesterPhone;
            Rtn.TechnicianName = item.TechnicianName;
            Rtn.TechnicianPhone = item.TechnicianPhone;
            Rtn.GuardName = item.GuardName;
            Rtn.GuardPhone = item.GuardPhone;

            Rtn.OfficerType = item.OfficerType;
            Rtn.OfficerNum = item.OfficerNum;
            Rtn.StartDate = item.StartDate;
            Rtn.EndDate = item.EndDate;
            Rtn.Notes = item.Notes;
            Rtn.RequestRate = Convert.ToDouble(item.RequestRate);
            Rtn.RequestCancel = item.RequestCancel;
            Rtn.RequestTerminate = item.RequestTerminate;
            Rtn.RequestTerminateDate = item.RequestTerminateDate;
            Rtn.RequestTerminateTime = item.RequestTerminateTime;
            Rtn.UpdatedBy = item.UpdatedBy;
            Rtn.UpdatedDTTM = item.UpdatedDTTM;
            Rtn.RequestedBy = item.RequestedBy;
            Rtn.RequestedDTTM = item.RequestedDTTM;
            Rtn.AddlCharges = item.AddlCharges;
            Rtn.AppdCharges = item.AppdCharges;
            Rtn.DeptID = item.DeptID;
            Rtn.AddlChargesDenied = item.AddlChargesDenied;
            Rtn.AddlChargesDeniedBy = item.AddlChargesDeniedBy;
            Rtn.RequestStatusID = item.RequestStatusID;
            Rtn.ArmGuard = item.ArmGuard;
            Rtn.ReasonService = item.ReasonService;
            Rtn.Uniform = item.Uniform;
            Rtn.Keys = item.Keys;
            Rtn.AuthorizedArea = item.AuthorizedArea;
            Rtn.EmergencyConstruction = item.EmergencyConstruction;
            Rtn.DMOPreApproved = item.DMOPreApproved;
            Rtn.DMOGranted = item.DMOGranted;
            Rtn.RequestDate = item.RequestDate;
            Rtn.ContactDepartment = item.ContactDepartment;
            Rtn.LocationType = item.LocationType;
            Rtn.ReasonServiceID = item.ReasonServiceID;
            Rtn.LocationTypeID = item.LocationTypeID;
            Rtn.Urgent = item.Urgent;
            Rtn.SaveNote = item.SaveNote == "True" ? true : false;
            Rtn.ExtensionDate = item.ExtensionDate;
            Rtn.ExtensionNote = item.ExtensionNote;
            Rtn.TotalAdditionalAmount = Convert.ToDouble(item.TotalAdditionalAmount);
            Rtn.CustomerSiteEntry = item.CustomerSiteEntry;
            //Rtn.CustName = item.custn
            Rtn.Status = item.Status;
            Rtn.CodeName = item.CodeName;

            Rtn.PONumber = item.PONumber;
            Rtn.CostCenterCode = item.CostCenterCode;
            Rtn.WorkOrderNumber = item.WorkOrderNumber;

            return Rtn;
        }
        public STSR_Droid.SRWSServices.SR_Request RequestItem_to_ASMX_item(SR_RequestItem item)
        {
            STSR_Droid.SRWSServices.SR_Request Rtn = new STSR_Droid.SRWSServices.SR_Request();
            Rtn.RequestID = item.RequestID;
            Rtn.SiteID = item.SiteID;
            Rtn.CorpCustID = item.CorpCustID;
            Rtn.ManualEntry = item.ManualEntry;
            Rtn.SiteName = item.SiteName;
            Rtn.Address1 = item.Address1;
            Rtn.Address2 = item.Address2;
            Rtn.City = item.City;
            Rtn.State = item.State;
            Rtn.ZipCode = item.ZipCode;
            Rtn.Latitude = Convert.ToDecimal(item.Latitude);
            Rtn.Longitude = Convert.ToDecimal(item.Longitude);
            Rtn.Contact = item.Contact;
            Rtn.ContactPhone = item.ContactPhone;

            Rtn.RequesterName = item.RequesterName;
            Rtn.RequesterPhone = item.RequesterPhone;
            Rtn.TechnicianName = item.TechnicianName;
            Rtn.TechnicianPhone = item.TechnicianPhone;
            Rtn.GuardName = item.GuardName;
            Rtn.GuardPhone = item.GuardPhone;

            Rtn.OfficerType = item.OfficerType;
            Rtn.OfficerNum = item.OfficerNum;
            Rtn.StartDate = item.StartDate;
            Rtn.EndDate = item.EndDate;
            Rtn.Notes = item.Notes;
            Rtn.RequestRate = Convert.ToDecimal(item.RequestRate);
            Rtn.RequestCancel = item.RequestCancel;
            Rtn.RequestTerminate = item.RequestTerminate;
            Rtn.RequestTerminateDate = item.RequestTerminateDate;
            Rtn.RequestTerminateTime = item.RequestTerminateTime;
            Rtn.UpdatedBy = item.UpdatedBy;
            Rtn.UpdatedDTTM = item.UpdatedDTTM;
            Rtn.RequestedBy = item.RequestedBy;
            Rtn.RequestedDTTM = item.RequestedDTTM;
            Rtn.AddlCharges = item.AddlCharges;
            Rtn.AppdCharges = item.AppdCharges;
            Rtn.DeptID = item.DeptID;
            Rtn.AddlChargesDenied = item.AddlChargesDenied;
            Rtn.AddlChargesDeniedBy = item.AddlChargesDeniedBy;
            Rtn.RequestStatusID = item.RequestStatusID;
            Rtn.ArmGuard = item.ArmGuard;
            Rtn.ReasonService = item.ReasonService;
            Rtn.Uniform = item.Uniform;
            Rtn.Keys = item.Keys;
            Rtn.AuthorizedArea = item.AuthorizedArea;
            Rtn.EmergencyConstruction = item.EmergencyConstruction;
            Rtn.DMOPreApproved = item.DMOPreApproved;
            Rtn.DMOGranted = item.DMOGranted;
            Rtn.RequestDate = item.RequestDate;
            Rtn.ContactDepartment = item.ContactDepartment;
            Rtn.LocationType = item.LocationType;
            Rtn.ReasonServiceID = item.ReasonServiceID;
            Rtn.LocationTypeID = item.LocationTypeID;
            Rtn.Urgent = item.Urgent;
            Rtn.SaveNote = item.SaveNote ? "True" : "False";
            Rtn.ExtensionDate = item.ExtensionDate;
            Rtn.ExtensionNote = item.ExtensionNote;
            Rtn.TotalAdditionalAmount = Convert.ToDecimal(item.TotalAdditionalAmount);
            Rtn.CustomerSiteEntry = item.CustomerSiteEntry;
            //Rtn.CustName = item.custn
            Rtn.Status = item.Status;
            Rtn.CodeName = item.CodeName;
            return Rtn;
        }

        public SR_CustomerItem ASMX_CustomerItem_to_item(STSR_Droid.SRWSServices.SR_Customer item)
        {
            SR_CustomerItem Rtn = new SR_CustomerItem();

            Rtn.CorpCustID = item.CorpCustID;
            Rtn.CustName = item.CustName;

            return Rtn;
        }
        
        public SRWS_ASMX.SR_G4SApprove ASMX_ApprovalItem_to_item(STSR_Droid.SRWSServices.SR_G4SApprove item)
        {
            SRWS_ASMX.SR_G4SApprove Rtn = new SRWS_ASMX.SR_G4SApprove();

            Rtn.AddlCharges = item.AddlCharges;
            Rtn.Address1 = item.Address1;
            Rtn.Amount = item.Amount;
            Rtn.AppdCharges = item.AppdCharges;
            Rtn.Approved = item.Approved;
            Rtn.Approvedby = item.Approvedby;
            Rtn.City = item.City;
            Rtn.Code = item.Code;
            Rtn.Contact = item.Contact;
            Rtn.CorpCustID = item.CorpCustID;
            Rtn.CtrlEnabled = item.CtrlEnabled;
            Rtn.CustName = item.CustName;
            Rtn.CustNotes = item.CustNotes;
            Rtn.Denied = item.Denied;
            Rtn.Deniedby = item.Deniedby;
            Rtn.DeniedCharges = item.DeniedCharges;
            Rtn.DeptID = item.DeptID;
            Rtn.Description = item.Description;
            Rtn.DisplayName = item.DisplayName;
            Rtn.EnableCtrlAddl = item.EnableCtrlAddl;
            Rtn.EnableCtrlAppd = item.EnableCtrlAppd;
            Rtn.EnableCtrlApprove = item.EnableCtrlApprove;
            Rtn.EnableCtrlDenied = item.EnableCtrlDenied;
            Rtn.EndDate = item.EndDate;
            Rtn.EndDate2 = item.EndDate2;
            //Rtn.ID = item.ID;
            Rtn.Notes = item.Notes;
            Rtn.OfficerNum = item.OfficerNum;
            Rtn.OfficerType = item.OfficerType;
            Rtn.Questioned = item.Questioned;
            Rtn.Questionedby = item.Questionedby;
            Rtn.RequestRate = item.RequestRate;
            Rtn.RequestStatusID = item.RequestStatusID;
            Rtn.ShowApprove = item.ShowApprove;
            Rtn.SR_RequestID = item.SR_RequestID;
            Rtn.StartDate = item.StartDate;
            Rtn.StartDate2 = item.StartDate2;
            Rtn.State = item.State;
            Rtn.Status = item.Status;
            Rtn.TotalAdditionalAmount = item.TotalAdditionalAmount;
            Rtn.IsGuard = item.IsGuard;

            Rtn.PONumber = item.PONumber;
            Rtn.WorkOrderNumber = item.WorkOrderNumber;
            Rtn.CostCenterCode = item.CostCenterCode;

            return Rtn;
        }
        public SR_SiteItem ASMX_SiteItem_to_item(STSR_Droid.SRWSServices.SR_Site item)
        {
            SR_SiteItem Rtn = new SR_SiteItem();

            Rtn.SiteID = item.SiteID;
            Rtn.DisplaySite = item.DisplaySite;

            return Rtn;
        }
        public SR_StateItem ASMX_StateItem_to_item(STSR_Droid.SRWSServices.SR_LckStates item)
        {
            SR_StateItem Rtn = new SR_StateItem();

            Rtn.Code = item.Code;
            Rtn.Description = item.Description;

            return Rtn;
        }
        public SR_RequestItem ASMX_SiteAddressItem_to_item(STSR_Droid.SRWSServices.SR_Request item)
        {
            SR_RequestItem Rtn = new SR_RequestItem();

            Rtn.DeptID = item.DeptID;
            Rtn.RequestID = item.RequestID;
            Rtn.SiteID = item.SiteID;
            Rtn.CorpCustID = item.CorpCustID;
            Rtn.ManualEntry = item.ManualEntry;
            Rtn.SiteName = item.SiteName;
            Rtn.Address1 = item.Address1;
            Rtn.Address2 = item.Address2;
            Rtn.City = item.City;
            Rtn.State = item.State;
            Rtn.ZipCode = item.ZipCode;
            Rtn.Latitude = Convert.ToDouble(item.Latitude);
            Rtn.Longitude = Convert.ToDouble(item.Longitude);
            Rtn.Contact = item.Contact;
            Rtn.ContactPhone = item.ContactPhone;

            return Rtn;
        }

        public  SRWS_ASMX.SR_LookUpItem ASMX_OfficerTypeItem_to_item(STSR_Droid.SRWSServices.SR_LckOfficerType item)
        {
            SRWS_ASMX.SR_LookUpItem Rtn = new SRWS_ASMX.SR_LookUpItem();

            Rtn.SR_RecordID = item.SR_RecordID;
            Rtn.Code = item.Code;
            Rtn.Description = item.Description;
            Rtn.DefaultValue = item.DefaultValue;
            Rtn.Enabled = item.Enabled;

            return Rtn;
        }
        public  SRWS_ASMX.SR_LookUpItem ASMX_LocationTypeItem_to_item(STSR_Droid.SRWSServices.SR_LckLocationType item)
        {
            SRWS_ASMX.SR_LookUpItem Rtn = new SRWS_ASMX.SR_LookUpItem();

            Rtn.SR_RecordID = item.SR_RecordID;
            Rtn.Code = item.Code;
            Rtn.Description = item.Description;
            Rtn.DefaultValue = item.DefaultValue;
            Rtn.Enabled = item.Enabled;

            return Rtn;
        }
        public SRWS_ASMX.SR_LookUpItem ASMX_DenyReasonItem_to_item(STSR_Droid.SRWSServices.SR_LckDenyReason item)
        {
            SRWS_ASMX.SR_LookUpItem Rtn = new SRWS_ASMX.SR_LookUpItem();

            Rtn.SR_RecordID = item.Sequence.ToString();
            Rtn.Code = item.Sequence.ToString();
            Rtn.Description = item.DisplayName;
            Rtn.DefaultValue = item.DisplayName;
            Rtn.Enabled = "true";

            return Rtn;
        }
        public SRWS_ASMX.SR_LookUpItem ASMX_ReasonServicesItem_to_item(STSR_Droid.SRWSServices.SR_LckReasonServices item)
        {
            SRWS_ASMX.SR_LookUpItem Rtn = new SRWS_ASMX.SR_LookUpItem();

            Rtn.SR_RecordID = item.SR_RecordID;
            Rtn.Code = item.Code;
            Rtn.Description = item.Description;
            Rtn.DefaultValue = item.DefaultValue;
            Rtn.Enabled = item.Enabled;

            return Rtn;
        }
        public SRWS_ASMX.SR_LookUpItem ASMX_DepartmentItem_to_item(STSR_Droid.SRWSServices.SR_LckOffice item)
        {
            SRWS_ASMX.SR_LookUpItem Rtn = new SRWS_ASMX.SR_LookUpItem();

            Rtn.SR_RecordID = item.DeptID;
            Rtn.Code = item.DeptID;
            Rtn.Description = item.Descr;
            Rtn.DefaultValue = item.Descr;
            Rtn.Enabled = "true";

            return Rtn;
        }

        public SRWS_ASMX.SR_CustomerApprove ASMX_CustApprovalItem_to_item(STSR_Droid.SRWSServices.SR_G4SApprove item)
        {
            SRWS_ASMX.SR_CustomerApprove Rtn = new SRWS_ASMX.SR_CustomerApprove();

            Rtn.AddlCharges = item.AddlCharges;
            Rtn.Address1 = item.Address1;
            Rtn.Amount = item.Amount;
            Rtn.AppdCharges = item.AppdCharges;
            Rtn.Approved = item.Approved;
            Rtn.Approvedby = item.Approvedby;
            Rtn.City = item.City;
            Rtn.Code = item.Code;
            Rtn.Contact = item.Contact;
            Rtn.CorpCustID = item.CorpCustID;
            Rtn.CtrlEnabled = item.CtrlEnabled;
            Rtn.CustName = item.CustName;
            Rtn.CustNotes = item.CustNotes;
            Rtn.Denied = item.Denied;
            Rtn.Deniedby = item.Deniedby;
            Rtn.DeniedCharges = item.DeniedCharges;
            Rtn.DeptID = item.DeptID;
            Rtn.Description = item.Description;
            Rtn.DisplayName = item.DisplayName;
            Rtn.EnableCtrlAddl = item.EnableCtrlAddl;
            Rtn.EnableCtrlAppd = item.EnableCtrlAppd;
            Rtn.EnableCtrlApprove = item.EnableCtrlApprove;
            Rtn.EnableCtrlDenied = item.EnableCtrlDenied;
            Rtn.EndDate = item.EndDate;
            Rtn.EndDate2 = item.EndDate2;
            //Rtn.ID = item.ID;
            Rtn.Notes = item.Notes;
            Rtn.OfficerNum = item.OfficerNum;
            Rtn.OfficerType = item.OfficerType;
            Rtn.Questioned = item.Questioned;
            Rtn.Questionedby = item.Questionedby;
            Rtn.RequestRate = item.RequestRate;
            Rtn.RequestStatusID = item.RequestStatusID;
            Rtn.ShowApprove = item.ShowApprove;
            Rtn.SR_RequestID = item.SR_RequestID;
            Rtn.StartDate = item.StartDate;
            Rtn.StartDate2 = item.StartDate2;
            Rtn.State = item.State;
            Rtn.Status = item.Status;
            Rtn.TotalAdditionalAmount = item.TotalAdditionalAmount;

            return Rtn;
        }
        public STSR_Droid.SRWSServices.SR_SaveWorkflowApprove SR_CustAppToSrvItem(STSR_Droid.Models.SR_CustomerApprove item, int oUserID)
        {
            STSR_Droid.SRWSServices.SR_SaveWorkflowApprove Rtn = new STSR_Droid.SRWSServices.SR_SaveWorkflowApprove();

            Rtn.SR_RequestID = item.SR_RequestID;
            Rtn.UserID = oUserID;
            Rtn.RequestStatusID = item.RequestStatusID;
            Rtn.RequestApproved = false;
            Rtn.RequestDeny = false;
            Rtn.Notes = item.Notes;
            Rtn.RequestRate = item.RequestRate;
            Rtn.AddlCharges = item.AddlCharges;
            Rtn.AppdCharges = item.AppdCharges;
            Rtn.DeniedCharges = item.DeniedCharges;
            Rtn.DeptID = item.DeptID;

            // Charges section
            Rtn.HotelAmount = item.HotelAmount;
            Rtn.PerDiemAmount = item.PerDiemAmount;
            Rtn.MileageAmount = item.MileageAmount;
            Rtn.CarRentalAmount = item.CarRentalAmount;
            Rtn.OtherAmount = item.OtherAmount;

            return Rtn; 
        }
        public STSR_Droid.SRWSServices.SR_SaveWorkflowApprove SR_G4SAppToSrvItem(STSR_Droid.Models.SR_G4SApprove item, int oUserID, bool IsApproved, bool isDenied)
        {
            STSR_Droid.SRWSServices.SR_SaveWorkflowApprove Rtn = new STSR_Droid.SRWSServices.SR_SaveWorkflowApprove();

            Rtn.SR_RequestID = item.SR_RequestID;
            Rtn.UserID = oUserID;
            Rtn.RequestStatusID = item.RequestStatusID;
            Rtn.RequestApproved = IsApproved;
            Rtn.RequestDeny = isDenied;
            Rtn.Notes = item.Notes;
            Rtn.RequestRate = item.RequestRate;
            Rtn.AddlCharges = item.AddlCharges;
            Rtn.AppdCharges = item.AppdCharges;
            Rtn.DeniedCharges = item.DeniedCharges;
            Rtn.DeptID = item.DeptID;

            // Charges section
            Rtn.HotelAmount = item.HotelAmount;
            Rtn.PerDiemAmount = item.PerDiemAmount;
            Rtn.MileageAmount = item.MileageAmount;
            Rtn.CarRentalAmount = item.CarRentalAmount;
            Rtn.OtherAmount = item.OtherAmount;

            return Rtn;
        }

    }
}
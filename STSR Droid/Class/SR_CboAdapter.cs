using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using STSR_Droid.Models;

namespace STSR_Droid.Class
{
    public class SR_CboAdapter
    {
        private List<string> items = new List<string>();

        public List<SR_LookUp> Lckitem = new List<SR_LookUp>();

        public void LoadAdapter(string oIndex, string oValue)
        {
            SR_LookUp item = new SR_LookUp();
            item.Index = oIndex;
            item.Value = oValue;
            Lckitem.Add(item);
        }

        public ArrayAdapter GetAdapter(View oview)
        {
            items.Clear();
            items = RenderList();
            //ArrayAdapter Rtn = new ArrayAdapter<string>(oview.Context, Android.Resource.Layout.SimpleSpinnerItem, items);
            ArrayAdapter Rtn = new ArrayAdapter<string>(oview.Context, Resource.Layout.Spinner_Item, items);
            Rtn.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            return Rtn;
        }

        private List<string> RenderList()
        {
            List<string> Rtn = new List<string>();
            foreach (SR_LookUp itm in Lckitem)
            {
                Rtn.Add(itm.Value);
            }
            return Rtn;
        }

        public int GetPosition(string oRef, SR_CboAdapter oCollection )
        {
            string oSiteID = oRef;
            int oCnt = 0;
            foreach (SR_LookUp itm in oCollection.Lckitem)
            {
                if (oSiteID == itm.Index)
                {

                    break;
                }
                oCnt = oCnt + 1;
            }
            if (oCnt >= oCollection.items.Count()) { oCnt = 0; };
            return oCnt;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Util;

namespace STSR_Droid.Class
{
    public class SR_SlidingTabStrip : LinearLayout
    {
        // ----------------------------------------------------------------
        private const int DEFAULT_BOTTOM_BORDER_THICKNESS_DIPS = 2;
        private const byte DEFAULT_BOTTOM_BORDER_COLOR_ALPHA = 0X26;
        private const int SELECTER_INDICATOR_THICKNESS_DIPS = 8;
        private int[] INDICATOR_COLORS = { 0X19A319, 0X0000FC };
        private int[] DIVIDER_COLORS = {0XC5C5C5 };

        private const int DEFAULT_DIVIDER_THICKNESS_DISPS = 1;
        private const float DEFAULT_DIVIDER_HEIGHT = 0.5f;

        private int mBottomBorderThickness;
        private Paint mBottomBorderPaint;
        private int mDefaultBottomBorderColor;

        private int mSelectedIndicatorThickness;
        private Paint mSelectedIndicatorPaint;

        private Paint mDividerPaint;
        private float mDividerHeight;

        private int mSelectedPosition;
        private float mSelectionOffSet;

        private SlidingTabScrollView.TabColorizer mCustomTabColorizer;
        private SimpleTabColorizer mDefaultTabColorizer;
        // ----------------------------------------------------------------

        public SR_SlidingTabStrip(Context context) : this (context,null)
        {

        }

        public SR_SlidingTabStrip(Context context,IAttributeSet attrs) : base(context,attrs)
        {
            SetWillNotDraw(false);

            float density = Resources.DisplayMetrics.Density;

            TypedValue outValue = new TypedValue();
            context.Theme.ResolveAttribute(Android.Resource.Attribute.ColorForeground, outValue, true);
            int themeForeGround = outValue.Data;
            mDefaultBottomBorderColor = SetColorAlpha(themeForeGround, DEFAULT_BOTTOM_BORDER_COLOR_ALPHA);

            mDefaultTabColorizer = new SimpleTabColorizer();
            mDefaultTabColorizer.IndicatorColors= INDICATOR_COLORS;
            mDefaultTabColorizer.DividerColors=DIVIDER_COLORS;

            mBottomBorderThickness = (int)(DEFAULT_BOTTOM_BORDER_THICKNESS_DIPS * density);
            mBottomBorderPaint = new Paint();
            mBottomBorderPaint.Color = GetColorFromInteger(0xC5C5C5);

            mSelectedIndicatorThickness = (int)(SELECTER_INDICATOR_THICKNESS_DIPS * density);
            mSelectedIndicatorPaint = new Paint();

            mDividerHeight = DEFAULT_DIVIDER_HEIGHT;
            mDividerPaint = new Paint();
            mDividerPaint.StrokeWidth = (int)(DEFAULT_DIVIDER_THICKNESS_DISPS * density);
        }
        public SlidingTabScrollView.TabColorizer CustomTabColorizer
        {
            set
            {
                mCustomTabColorizer = value;
                this.Invalidate();
            }
        }
        public int[] SelectedIndicatorColors
        {
            set
            {
                mCustomTabColorizer = null;
                mDefaultTabColorizer.IndicatorColors = value;
                this.Invalidate();
            }
        }
        public int[] DividerColors
        {
            set
            {
                mCustomTabColorizer = null;
                mDefaultTabColorizer.DividerColors = value;
                this.Invalidate();
            }
        }
        private Color GetColorFromInteger(int color)
        {
            return Color.Rgb(Color.GetRedComponent(color), Color.GetGreenComponent(color), Color.GetBlueComponent(color));
        }
        private int SetColorAlpha(int color, byte alpha)
        {
            return Color.Argb(alpha, Color.GetRedComponent(color), Color.GetGreenComponent(color), Color.GetBlueComponent(color));

        }

        public void OnViewPagerPageChanged(int position,float positionOffSet)
        {
            mSelectedPosition = position;
            mSelectionOffSet = positionOffSet;
            this.Invalidate();
        }

        protected override void OnDraw(Canvas canvas)
        {
            int height = Height;
            int tabCount = ChildCount;
            int dividerHeightPx = (int)(Math.Min(Math.Max(0f, mDividerHeight), 1f) * height);
            SlidingTabScrollView.TabColorizer tabcolorizer = mCustomTabColorizer != null ? mCustomTabColorizer : mDefaultTabColorizer;

            if (tabCount > 0)
            {
                View selectedTitle = GetChildAt(mSelectedPosition);
                int left = selectedTitle.Left;
                int right = selectedTitle.Right;
                int color = tabcolorizer.GetIndicatorColor(mSelectedPosition);

                if (mSelectionOffSet>0f && mSelectedPosition<(tabCount-1))
                {
                    int nextColor = tabcolorizer.GetIndicatorColor(mSelectedPosition + 1);
                    if (color != nextColor)
                    {
                        color = blendColor(nextColor,color,mSelectionOffSet);
                    }

                    View nextTitle = GetChildAt(mSelectedPosition + 1);
                    left = (int)(mSelectionOffSet * nextTitle.Left + (1.0f - mSelectionOffSet) * left);
                    right = (int)(mSelectionOffSet * nextTitle.Right + (1.0f - mSelectionOffSet) * right);
                }

                mSelectedIndicatorPaint.Color = GetColorFromInteger(color);

                canvas.DrawRect(left,height-mSelectedIndicatorThickness,right,height,mSelectedIndicatorPaint);

                int separatorTop = (height - dividerHeightPx) / 2;
                for (int i=0;i<ChildCount;i++)
                {
                    View child = GetChildAt(i);
                    mDividerPaint.Color = GetColorFromInteger(tabcolorizer.GetDividerColor(i));
                    canvas.DrawLine(child.Right,separatorTop,child.Right,separatorTop+dividerHeightPx,mDividerPaint);

                }

                canvas.DrawRect(0,height - mBottomBorderThickness,Width,height,mBottomBorderPaint);

            }

        }

        private int blendColor(int color1, int color2, float ratio)
        {
            float inverseRatio = 1f - ratio;
            float r = (Color.GetRedComponent(color1) * ratio) + (Color.GetRedComponent(color2) * inverseRatio);
            float g = (Color.GetGreenComponent(color1) * ratio) + (Color.GetGreenComponent(color2) * inverseRatio);
            float b = (Color.GetBlueComponent(color1) * ratio) + (Color.GetBlueComponent(color2) * inverseRatio);

            return Color.Rgb((int)r,(int)g,(int)b);

        }
        private class SimpleTabColorizer : SlidingTabScrollView.TabColorizer
        {
            private int[] mIndicatorColors;
            private int[] mDividerColors;
            
            public int GetIndicatorColor(int position)
            {
                return mIndicatorColors[position % mIndicatorColors.Length];
            } 

            public int GetDividerColor(int position)
            {
                return mDividerColors[position % mDividerColors.Length];
            }

            public int[] IndicatorColors
            {
                set { mIndicatorColors = value; }
            }
            
            public int[] DividerColors
            {
                set { mDividerColors = value; }
            }



        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
////using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using Sec = System.Security.Cryptography;
using System.IO;

namespace SR_Class
{
    public class SR_Security
    {
        //    Private enc As System.Text.UTF8Encoding
        //Private encryptor As Sec.ICryptoTransform
        //Private decryptor As Sec.ICryptoTransform
        protected System.Text.UTF8Encoding enc;
        protected Sec.ICryptoTransform encryptor;
        protected Sec.ICryptoTransform decryptor;

        public string Key_Encrypt(string oValue)
        {
            SR_Core oCore = new SR_Core();
            //var appSettings = System.Configuration.ConfigurationManager.AppSettings;

            Byte[] Key_128 = { 42, 1, 52, 67, 231, 13, 94, 101, 123, 6, 0, 12, 32, 91, 4, 111, 31, 70, 21, 141, 123, 142, 234, 82, 95, 129, 187, 162, 12, 55, 98, 23 };
            Byte[] IV_128 = { 234, 12, 52, 44, 214, 222, 200, 109, 2, 98, 45, 76, 88, 53, 23, 78};


            Sec.RijndaelManaged symmetricKey = new Sec.RijndaelManaged();
            symmetricKey.Mode = Sec.CipherMode.CBC;


            enc = new System.Text.UTF8Encoding();
            encryptor = symmetricKey.CreateEncryptor(Key_128,IV_128);
            decryptor = symmetricKey.CreateDecryptor(Key_128, IV_128);

            string sPlainText = oValue;
            string Rtn = string.Empty;
            if (!String.IsNullOrEmpty(sPlainText))
            {
                MemoryStream memoryStream = new MemoryStream();
                Sec.CryptoStream cryptoStream = new Sec.CryptoStream(memoryStream, encryptor, Sec.CryptoStreamMode.Write);
                cryptoStream.Write(enc.GetBytes(sPlainText), 0, sPlainText.Length);
                cryptoStream.FlushFinalBlock();
                Rtn = Convert.ToBase64String(memoryStream.ToArray());
                memoryStream.Close();
                cryptoStream.Close();
            }
            return Rtn;
        }
    }
}
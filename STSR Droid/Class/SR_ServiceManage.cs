using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Services.Protocols;
using System.Security.Cryptography.X509Certificates;
using SR_Class;
using SRWS_ASMX;
using STSR_Droid.SRWSServices;
using System.Data;



namespace SR_Class
{
    public partial class SR_ServiceManage : ISoapServices
    {
        STSR_Droid.SRWSServices.SRWSServices SRWS_Services;

        public List<SR_GetOfficeItem> OfficeItems { get; private set; }
        public List<SRWS_ASMX.SR_UserItem> UserItems { get; private set; }
        public List<SR_RequestItem> RequestItems { get; private set; }
        public List<SR_CustomerItem> CustomerItems { get; private set; }
        public List<SRWS_ASMX.SR_G4SApprove> ApprovalItems { get; private set; }
        public List<SR_SiteItem> SiteItems { get; private set; }
        public List<SR_SiteAddressItem> SiteAddressItem { get; private set; }
        public List<SR_LookUpItem> LookUpItems { get; private set; }
        public List<SR_LookUpItem> DepartmentItems { get; private set; }
        public List<SR_StateItem> StateItems { get; private set; }


        public SR_ServiceManage()
        {
            SRWS_Services = new STSR_Droid.SRWSServices.SRWSServices();
        }

        //TESTING
        //public void SR_GetOfficeList()
        //{
        //    STSR_Droid.SRWSServices.SRWSServices Proxis = new STSR_Droid.SRWSServices.SRWSServices();
        //    Proxis.SR_GetOfficeListCompleted += new STSR_Droid.SRWSServices.SR_GetOfficeListCompletedEventHandler(proxis_GetOfficeListCompleted);
        //    Proxis.SR_GetOfficeListAsync();
        //}
        //void proxis_GetOfficeListCompleted(object sender, STSR_Droid.SRWSServices.SR_GetOfficeListCompletedEventArgs e)
        //{
        //    List<STSR_Droid.SRWSServices.SR_Office> oreturn = new List<STSR_Droid.SRWSServices.SR_Office>();
        //    foreach (STSR_Droid.SRWSServices.SR_Office item in e.Result) { oreturn.Add(item); }
            
        //}
        //END TESTING
        //Task<List<SR_GetOfficeItem>> SR_GetOfficeListAsync();
        //Task<List<SR_UserItem>> SR_LoginAuthentication(string oUserID, string oPassword);
        //Task<List<SR_RequestItem>> SR_SPRequestSelect(string oUserID);
        //Task<List<SR_RequestItem>> SR_GetRequestbyID(string oRequestID);




        public List<SR_GetOfficeItem> SR_GetOfficeListAsync()
        {
            SR_DataConversion dc = new SR_DataConversion();
            OfficeItems = new List<SR_GetOfficeItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getofficeitem =   oService.SR_GetOfficeList();
                foreach (var item in getofficeitem)
                {
                    OfficeItems.Add(dc.ASMX_OfficeItem_to_item((STSR_Droid.SRWSServices.SR_Office)item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return OfficeItems;
        }
        public  List<SRWS_ASMX.SR_UserItem> SR_LoginAuthentication(string oUserID, string oPassword)
        {
            //SRWSServices oService = new SRWSServices();
            //return oService.SR_LoginAuthentication(oUserID, oPassword);
            //STSR_Droid.SRWSServices.SR_GetOfficeListCompletedEventArgs t;
            //STSR_Droid.SRWSServices.SR_GetOfficeListCompletedEventHandler h;
            //STSR_Droid.SRWSServices.SRWSServices Proxis = new STSR_Droid.SRWSServices.SRWSServices();
            //Proxis.SR_GetOfficeList();
            //Proxis.SR_GetOfficeListAsync();
            //Proxis.SR_GetOfficeListCompleted();

            SR_DataConversion dc = new SR_DataConversion();
            UserItems = new List<SRWS_ASMX.SR_UserItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getuseritem = oService.SR_LoginAuthentication(oUserID, oPassword);
                foreach (var item in getuseritem)
                {
                    UserItems.Add(dc.ASMX_UserItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return UserItems;
        }

        //private static SR_WebService SRWSAPIServices = new SR_WebService("http://10.91.1.136/SRWS%202.0/SRWS.asmx");

        //public SR_GetOfficeItem ServiceConnection()
        //{

        //    SR_Class.SR_Security oSecurity = new SR_Security();

        //    SR_AuthenticationHeader Credential = new SR_AuthenticationHeader();
        //    SRWSServices CallSOAP = new SRWSServices();
        //    CallSOAP.UseDefaultCredentials = false;
        //    CallSOAP.Url = "http://10.91.1.136/SRWS%202.0/SRWS.asmx";
        //    //CallSOAP.Url = "http://localhost/SRWS%202.0/SRWS.asmx";

        //    Credential.KeyCode = oSecurity.Key_Encrypt("Z7L_MjqBpU8");
        //    Credential.Token = "21d24ea38e49bc16b898c1e95930e044";
        //    Credential.RefDateTime = oSecurity.Key_Encrypt(DateTime.Now.ToString());
        //    //SRWS.SRWSServicesSoapClient CallSOAP = new SRWS.SRWSServicesSoapClient();
        //    CallSOAP.SR_AuthenticationHeaderValue = Credential;
        //    return CallSOAP;
        //}

        //public SR_WebService ServiceAltConnection() {
        //    SR_Class.SR_Security oSecurity = new SR_Security();

        //    SR_AuthenticationHeader Credential = new SR_AuthenticationHeader();
        //    //SRWSServices CallSOAP = new SRWSServices();




        //    SRWSAPIServices.UseDefaultAPICredentials = false;

        //    Credential.KeyCode = oSecurity.Key_Encrypt("Z7L_MjqBpU8");
        //    Credential.Token = "21d24ea38e49bc16b898c1e95930e044";
        //    Credential.RefDateTime = oSecurity.Key_Encrypt(DateTime.Now.ToString());
        //    //SRWS.SRWSServicesSoapClient CallSOAP = new SRWS.SRWSServicesSoapClient();
        //    //SRWSAPIServices.SR_AuthenticationHeaderValue = Credential;
        //    return SRWSAPIServices;
        //}
        private SR_AuthenticationHeader SetCredentials(SRWSServices oService)
        {
            SR_Class.SR_Security oSecurity = new SR_Security();
            SR_AuthenticationHeader Credential = new SR_AuthenticationHeader();
            Credential.KeyCode = oSecurity.Key_Encrypt("Z7L_MjqBpU8");
            Credential.Token = "21d24ea38e49bc16b898c1e95930e044";
            Credential.RefDateTime = oSecurity.Key_Encrypt(DateTime.Now.ToString());
            return Credential;
        }
    }
    public static class SSLValidator
    {


        private static bool OnValidateCertificate(object sender, X509Certificate certificate, X509Chain chain,
                                                  System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        public static void OverrideValidation()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = OnValidateCertificate;
            System.Net.ServicePointManager.Expect100Continue = true;
        }
    }
}
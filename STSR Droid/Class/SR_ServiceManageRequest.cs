using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Services.Protocols;
using System.Security.Cryptography.X509Certificates;
using SR_Class;
using SRWS_ASMX;
using STSR_Droid.SRWSServices;
using System.Data;



namespace SR_Class
{
    public partial class SR_ServiceManage : ISoapServices
    {
       

        public List<SR_RequestItem> SR_SPRequestSelect(string oUserID, int oPage)
        {
            SR_DataConversion dc = new SR_DataConversion();
            RequestItems = new List<SRWS_ASMX.SR_RequestItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getrequestitem = oService.SR_SPRequestSelect(oUserID, oPage);
                foreach (var item in getrequestitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    RequestItems.Add(dc.ASMX_RequestItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return RequestItems;
        }
        public List<SR_RequestItem> SR_GetRequestbyID(string oRequestID)
        {
            SR_DataConversion dc = new SR_DataConversion();
            RequestItems = new List<SRWS_ASMX.SR_RequestItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getrequestitem = oService.SR_GetRequestbyID(oRequestID);
                foreach (var item in getrequestitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    RequestItems.Add(dc.ASMX_RequestItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return RequestItems;
        }

        public List<SR_CustomerItem> SR_GetCustomerList()
        {
            SR_DataConversion dc = new SR_DataConversion();
            CustomerItems = new List<SRWS_ASMX.SR_CustomerItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getcustomeritem = oService.SR_GetCustomerSiteList();
                foreach (var item in getcustomeritem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    CustomerItems.Add(dc.ASMX_CustomerItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return CustomerItems;
        }
        
        public List<SR_CustomerItem> SR_GetAllCustomersbyUserID(string oUserID)
        {
            SR_DataConversion dc = new SR_DataConversion();
            CustomerItems = new List<SRWS_ASMX.SR_CustomerItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getcustomeritem = oService.SR_GetAllCustomersbyUserID(oUserID);
                foreach (var item in getcustomeritem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    CustomerItems.Add(dc.ASMX_CustomerItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return CustomerItems;
        }
        public List<SR_SiteItem> SR_GetSiteList(string oCorpUserID)
        {
            SR_DataConversion dc = new SR_DataConversion();
            SiteItems = new List<SR_SiteItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getsiteitem = oService.SR_GetSiteList(oCorpUserID);
                foreach (var item in getsiteitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    SiteItems.Add(dc.ASMX_SiteItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return SiteItems;
        }

        public List<SR_SiteItem> SR_GetSiteListbyState(string oCorpUserID, string oState)
        {
            SR_DataConversion dc = new SR_DataConversion();
            SiteItems = new List<SR_SiteItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getsiteitem = oService.SR_GetSiteListbyState(oCorpUserID, oState);
                foreach (var item in getsiteitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    SiteItems.Add(dc.ASMX_SiteItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return SiteItems;
        }

        public List<SR_StateItem> SR_GetStateList()
        {
            SR_DataConversion dc = new SR_DataConversion();
            StateItems = new List<SR_StateItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getstateitem = oService.SR_GetStatesLookup();
                foreach (var item in getstateitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    StateItems.Add(dc.ASMX_StateItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return StateItems;
        }
        public SR_RequestItem SR_GetSiteAddressbyLocation(string oSiteID)
        {
            SR_DataConversion dc = new SR_DataConversion();
            RequestItems = new List<SR_RequestItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getsiteaddressitem = oService.SR_GetSiteAddressbyLocation(oSiteID);
                foreach (var item in getsiteaddressitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    RequestItems.Add(dc.ASMX_SiteAddressItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return RequestItems[0];
        }

        public SR_RequestItem SR_GetSiteAddressbySite(string oSiteID)
        {
            SR_DataConversion dc = new SR_DataConversion();
            RequestItems = new List<SR_RequestItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getsiteaddressitem = oService.SR_GetSiteAddressbySite(oSiteID);
                foreach (var item in getsiteaddressitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    RequestItems.Add(dc.ASMX_SiteAddressItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return RequestItems[0];
        }

        public List<SR_LookUpItem> SR_GetOfficerTypeList()
        {
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getofficertypeitem = oService.SR_GetOfficerTypeLookup();
                foreach (var item in getofficertypeitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    LookUpItems.Add(dc.ASMX_OfficerTypeItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return LookUpItems;
        }

        public List<SR_LookUpItem> SR_GetReasonServicesList()
        {
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getofficertypeitem = oService.SR_GetReasonServicesLookup();
                foreach (var item in getofficertypeitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    LookUpItems.Add(dc.ASMX_ReasonServicesItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return LookUpItems;
        }
        public List<SR_LookUpItem> SR_GetLocationTypeList()
        {
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getlocationtypeitem = oService.SR_GetLocationTypeLookup();
                foreach (var item in getlocationtypeitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    LookUpItems.Add(dc.ASMX_LocationTypeItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return LookUpItems;
        }
        public List<SR_LookUpItem> SR_GetReasonDeny()
        {
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getdenyreasonitem = oService.SR_GetDenyReasonList();
                foreach (var item in getdenyreasonitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    LookUpItems.Add(dc.ASMX_DenyReasonItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return LookUpItems;
        }
        
        public Boolean SaveNewHoldRequest(SR_Request oRequest)
        {
            Boolean Rtn = false;
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);
                Rtn = oService.SR_SaveNewHoldRequest(oRequest);
            }
            catch (Exception ex)
            {
                Rtn = false;
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }

        public String SR_SaveHoldRequests(STSR_Droid.Models.SR_Session oSession, SR_Request oRequest)
        {
            String Rtn = "";
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);
                Rtn = oService.SR_btnSaveHoldRequests(oSession.ToSRWSServices(), oRequest);
            }
            catch (Exception ex)
            {
                Rtn = "Failed";
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }
        

        public Boolean DeleteHoldRequest(SR_Request oRequest)
        {
            Boolean Rtn = false;
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                Rtn = oService.SR_DeleteHoldRequest(oRequest.RequestID.ToString());

            }
            catch (Exception ex)
            {
                Rtn = false;
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }
        public Boolean SaveApprovedRequest(SR_Request oRequest)
        {
            Boolean Rtn = false;
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                Rtn = oService.SR_SaveApprovedRequest(oRequest);

            }
            catch (Exception ex)
            {
                Rtn = false;
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }
        public Boolean SaveCancelledRequestbyID(SR_Request oRequest)
        {
            Boolean Rtn = false;
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                Rtn = oService.SR_SaveCancelledRequestbyID (oRequest);

            }
            catch (Exception ex)
            {
                Rtn = false;
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }
        public Boolean SaveHoldRequest(SR_Request oRequest)
        {
            Boolean Rtn = false;
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                Rtn = oService.SR_SaveHoldRequest(oRequest);

            }
            catch (Exception ex)
            {
                Rtn = false;
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }
        public int SaveNewRequest(SR_Request oRequest)
        {
            int Rtn = 0;
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                Rtn = Convert.ToInt32(oService.SR_SaveNewRequest (oRequest));

            }
            catch (Exception ex)
            {
                Rtn = 0;
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }
        public Boolean SaveCancelledRequest(SR_Request oRequest)
        {
            Boolean Rtn = false;
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                Rtn = oService.SR_SaveCancelledRequest (oRequest);

            }
            catch (Exception ex)
            {
                Rtn = false;
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }
        public Boolean SaveTerminatedRequest(SR_Request oRequest)
        {
            Boolean Rtn = false;
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                Rtn = oService.SR_SaveTerminatedRequest (oRequest);

            }
            catch (Exception ex)
            {
                Rtn = false;
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }
        public Boolean SaveRequest(SR_Request oRequest)
        {
            Boolean Rtn = false;
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                Rtn = oService.SR_SaveRequest(oRequest);

            }
            catch (Exception ex)
            {
                Rtn = false;
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }
        public int GetHoldRequestID()
        {
            int Rtn = -1;
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                int.TryParse(oService.SR_GetHoldRequestID(), out Rtn);
                //Rtn = oService.SR_GetHoldRequestID();

            }
            catch (Exception ex)
            {
                Rtn = -1;
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }
        public List<SRWS_ASMX.SR_RequestItem> SR_GetRequestHoldList()
        {
            List<SRWS_ASMX.SR_RequestItem> Rtn = new List<SRWS_ASMX.SR_RequestItem>();
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var Rec = oService.SR_GetRequestHoldList();
                if (Rec != null)
                {
                    foreach (var item in Rec)
                    {
                        SRWS_ASMX.SR_RequestItem newItem = new SRWS_ASMX.SR_RequestItem();
                        //newItem.LoadModel(item);
                        Rtn.Add(newItem);
                    }
                }
                //Rtn = oService.SR_GetHoldRequestID();

            }
            catch (Exception ex)
            {
                Rtn = null;
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }
        public void SR_RequestExtension(SR_Request oRequest)
        {
            List<SRWS_ASMX.SR_RequestItem> Rtn = new List<SRWS_ASMX.SR_RequestItem>();
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                oService.SR_GetRequestHoldList();
                //Rtn = oService.SR_GetHoldRequestID();

            }
            catch (Exception ex)
            {
                Rtn = null;
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            
        }
        public void SR_RequestNotes(int oSource, int oTarget)
        {
            List<SRWS_ASMX.SR_RequestItem> Rtn = new List<SRWS_ASMX.SR_RequestItem>();
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                oService.SR_GetRequestHoldList();
                //Rtn = oService.SR_GetHoldRequestID();

            }
            catch (Exception ex)
            {
                Rtn = null;
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
           
        }
        public string SR_ResetRequest(STSR_Droid.Models.SR_Session oSession,SR_Request oRequest)
        {
            String Rtn = "";
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                Rtn = oService.SR_btnResetServiceRequest(oSession.ToSRWSServices(), oRequest);
                //Rtn = oService.SR_GetHoldRequestID();

            }
            catch (Exception ex)
            {
                Rtn = "";
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }
        public string SR_SubmitRequest(STSR_Droid.Models.SR_Session oSession, SR_Request oRequest)
        {
            String Rtn = "";
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                Rtn = oService.SR_btnSubmitServiceRequest(oSession.ToSRWSServices(), oRequest);
                //Rtn = oService.SR_GetHoldRequestID();

            }
            catch (Exception ex)
            {
                Rtn = "Failed!!";
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }
        public string SR_SaveRequestService(STSR_Droid.Models.SR_Session oSession, SR_Request oRequest)
        {
            String Rtn = "";
            SR_DataConversion dc = new SR_DataConversion();
            LookUpItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                Rtn = oService.SR_btnSaveRequestService(oSession.ToSRWSServices(), oRequest);
                //Rtn = oService.SR_GetHoldRequestID();

            }
            catch (Exception ex)
            {
                Rtn = "";
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }
    }
}
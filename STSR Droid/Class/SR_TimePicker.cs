using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace STSR_Droid.Class
{
    public class TimePickerDialogFragment : DialogFragment
    {
        private readonly Context _context;
        private DateTime _date;
        private readonly Android.App.TimePickerDialog.IOnTimeSetListener _listener;
        public string _classitem { get; set; }

        public TimePickerDialogFragment(Context context, DateTime date, Android.App.TimePickerDialog.IOnTimeSetListener listener, string classitem)
        {
            _context = context;
            _date = date;
            _listener = listener;
            _classitem = classitem;
        }

        public override Dialog OnCreateDialog(Bundle savedState)
        {
            var dialog = new Android.App.TimePickerDialog(_context, _listener, _date.Hour, _date.Minute, true);
            return dialog;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using Java.Lang;
using STSR_Droid.Models;
using STSR_Droid.ORM;

using SR_Class;
using SQLite;
using STSR_Droid.SQLiteTables;


namespace STSR_Droid.Class
{
    public partial class SlidingTabFragment : Fragment, DatePickerDialog.IOnDateSetListener, TimePickerDialog.IOnTimeSetListener
    {
        #region "Init Request Control"
        //*************************************************************************************************************
        //  Init G4S Approval Control
        //*************************************************************************************************************

        private static void InitG4SControl_01(View oview)
        {
            DBLckUpRepository dbr = new DBLckUpRepository();
            // Load Department List
            // *****************************************************************************************
            oLckDepartment.Lckitem = dbr.DepartmentList();
            Spinner cboDepartment = oview.FindViewById<Spinner>(Resource.Id.cboDepartment);
            cboDepartment.Adapter = oLckDepartment.GetAdapter(oview);
            // cboLocationType
            cboDepartment.SetSelection(oLckDepartment.GetPosition(mod_SaveG4SApprove.DeptID.ToString(), oLckDepartment));

            cboDepartment.ItemSelected += (sender, args) =>
            {
                mod_SaveG4SApprove.DeptID = oLckDepartment.Lckitem[args.Position].Index;
            };
        }
        private static void InitG4SControl_02(View oview)
        {

            CheckBox chkAdditionalCharges = oview.FindViewById<CheckBox>(Resource.Id.chkAdditionalCharges);

            TextView txtHotelAmount = oview.FindViewById<TextView>(Resource.Id.txtHotel);
            TextView txtPerDiemAmount = oview.FindViewById<TextView>(Resource.Id.txtPerDiem);
            TextView txtMileageAmount = oview.FindViewById<TextView>(Resource.Id.txtMileage);
            TextView txtCarRentalAmount = oview.FindViewById<TextView>(Resource.Id.txtCarRental);
            TextView txtOtherAmount = oview.FindViewById<TextView>(Resource.Id.txtOther);

            //TextView txtPONumber = oview.FindViewById<TextView>(Resource.Id.txtPONumber);
            //TextView txtCostCenterCode = oview.FindViewById<TextView>(Resource.Id.txtCostCenterCode);
            //TextView txtWorkOrderNumber = oview.FindViewById<TextView>(Resource.Id.txtWorkOrderNumber);

            TextView txtHotel = oview.FindViewById<TextView>(Resource.Id.lblHotel);
            TextView txtPerDiem = oview.FindViewById<TextView>(Resource.Id.lblPerDiem);
            TextView txtMileage = oview.FindViewById<TextView>(Resource.Id.lblMileage);
            TextView txtCarRental = oview.FindViewById<TextView>(Resource.Id.lblCarRental);
            TextView txtOther = oview.FindViewById<TextView>(Resource.Id.lblOther);

            if (mod_SaveG4SApprove.HotelAmount != 0 ||
                mod_SaveG4SApprove.CarRentalAmount != 0 ||
                mod_SaveG4SApprove.MileageAmount != 0 ||
                mod_SaveG4SApprove.PerDiemAmount != 0 ||
                mod_SaveG4SApprove.OtherAmount != 0
                )
            {
                chkAdditionalCharges.Checked = true;
                //chkAdditionalCharges.Enabled = false;

                txtHotelAmount.Visibility = ViewStates.Visible;
                txtPerDiemAmount.Visibility = ViewStates.Visible;
                txtMileageAmount.Visibility = ViewStates.Visible;
                txtCarRentalAmount.Visibility = ViewStates.Visible;
                txtOtherAmount.Visibility = ViewStates.Visible;

                txtHotel.Visibility = ViewStates.Visible;
                txtPerDiem.Visibility = ViewStates.Visible;
                txtMileage.Visibility = ViewStates.Visible;
                txtCarRental.Visibility = ViewStates.Visible;
                txtOther.Visibility = ViewStates.Visible;

            }
            else
            {
                //chkAdditionalCharges.Enabled = true;

                txtHotelAmount.Visibility = ViewStates.Invisible;
                txtPerDiemAmount.Visibility = ViewStates.Invisible;
                txtMileageAmount.Visibility = ViewStates.Invisible;
                txtCarRentalAmount.Visibility = ViewStates.Invisible;
                txtOtherAmount.Visibility = ViewStates.Invisible;

                txtHotel.Visibility = ViewStates.Invisible;
                txtPerDiem.Visibility = ViewStates.Invisible;
                txtMileage.Visibility = ViewStates.Invisible;
                txtCarRental.Visibility = ViewStates.Invisible;
                txtOther.Visibility = ViewStates.Invisible;
            }

            chkAdditionalCharges.Click += (sender, args) =>
            {
                if (chkAdditionalCharges.Checked)
                {
                    txtHotelAmount.Visibility = ViewStates.Visible;
                    txtPerDiemAmount.Visibility = ViewStates.Visible;
                    txtMileageAmount.Visibility = ViewStates.Visible;
                    txtCarRentalAmount.Visibility = ViewStates.Visible;
                    txtOtherAmount.Visibility = ViewStates.Visible;

                    txtHotel.Visibility = ViewStates.Visible;
                    txtPerDiem.Visibility = ViewStates.Visible;
                    txtMileage.Visibility = ViewStates.Visible;
                    txtCarRental.Visibility = ViewStates.Visible;
                    txtOther.Visibility = ViewStates.Visible;
                }
                else
                {
                    txtHotelAmount.Visibility = ViewStates.Invisible;
                    txtPerDiemAmount.Visibility = ViewStates.Invisible;
                    txtMileageAmount.Visibility = ViewStates.Invisible;
                    txtCarRentalAmount.Visibility = ViewStates.Invisible;
                    txtOtherAmount.Visibility = ViewStates.Invisible;

                    txtHotel.Visibility = ViewStates.Invisible;
                    txtPerDiem.Visibility = ViewStates.Invisible;
                    txtMileage.Visibility = ViewStates.Invisible;
                    txtCarRental.Visibility = ViewStates.Invisible;
                    txtOther.Visibility = ViewStates.Invisible;
                }



            };

            //EditText txtHotelAmount = oview.FindViewById<EditText>(Resource.Id.txtHotel);
            //EditText txtPerDiemAmount = oview.FindViewById<EditText>(Resource.Id.txtPerDiem);
            //EditText txtMileageAmount = oview.FindViewById<EditText>(Resource.Id.txtMileage);
            //EditText txtCarRentalAmount = oview.FindViewById<EditText>(Resource.Id.txtCarRental);
            //EditText txtOtherAmount = oview.FindViewById<EditText>(Resource.Id.txtOther);

            //txtHotelAmount.AfterTextChanged += (sender, args) => { if (txtHotelAmount != null) { txtHotelAmount.Text = (txtHotelAmount.Text.IndexOf("$") == -1) ? txtHotelAmount.Text : string.Format("{0:C}", Decimal.Parse(txtHotelAmount.Text)); } };
            //txtPerDiemAmount.AfterTextChanged += (sender, args) => { if (txtPerDiemAmount != null) { txtPerDiemAmount.Text = (txtPerDiemAmount.Text.IndexOf("$") == -1) ? txtPerDiemAmount.Text : string.Format("{0:C}", Decimal.Parse(txtPerDiemAmount.Text)); } };
            //txtMileageAmount.AfterTextChanged += (sender, args) => { if (txtMileageAmount != null) { txtMileageAmount.Text = (txtMileageAmount.Text.IndexOf("$") == -1) ? txtMileageAmount.Text : string.Format("{0:C}", Decimal.Parse(txtMileageAmount.Text)); } };
            //txtCarRentalAmount.AfterTextChanged += (sender, args) => { if (txtCarRentalAmount != null) { txtCarRentalAmount.Text = (txtCarRentalAmount.Text.IndexOf("$") == -1) ? txtCarRentalAmount.Text : string.Format("{0:C}", Decimal.Parse(txtCarRentalAmount.Text)); } };
            //txtOtherAmount.AfterTextChanged += (sender, args) => { if (txtOtherAmount != null) { txtOtherAmount.Text = (txtOtherAmount.Text.IndexOf("$") == -1) ? txtOtherAmount.Text : string.Format("{0:C}", Decimal.Parse(txtOtherAmount.Text)); } };

        }
        private static void InitG4SControl_03(View oview)
        {
            DBLckUpRepository dbr = new DBLckUpRepository();
            // Load Deny Reason List
            // *****************************************************************************************
            oLckReasonDeny.Lckitem = dbr.ReasonDenyList();
            Spinner cboReason_Deny = oview.FindViewById<Spinner>(Resource.Id.cboReasonDeny);
            cboReason_Deny.Adapter = oLckReasonDeny.GetAdapter(oview);
            // cboLocationType

            cboReason_Deny.SetSelection(oLckReasonDeny.GetPosition(mod_SaveG4SApprove.ReasonDenyID.ToString(), oLckReasonDeny));

            cboReason_Deny.ItemSelected += (sender, args) =>
            {
                mod_SaveG4SApprove.ReasonDenyID = Convert.ToInt32(oLckReasonDeny.Lckitem[args.Position].Index);
            };
        }
        private static void InitG4SControl_04(View oview)
        {
            if (!Loaded_Page4)
            {
                Button btnG4S_Save = oview.FindViewById<Button>(Resource.Id.btnUpdate);
                Button btnG4S_Cancel = oview.FindViewById<Button>(Resource.Id.btnCancel);
               
                btnG4S_Save.Click += BtnG4S_Save_Click;
                btnG4S_Cancel.Click += btnG4S_Cancel_Click;

                Loaded_Page4 = true;
            };
        }
        #region "User Interface Renders"
        private static void BtnG4S_Save_Click(object sender, EventArgs e)
        {
            string oMessage = "Cannot reach database. System detected a problem.";
            bool IsChargeChanged = false;
            bool IsSaved = true;
            //bool IsDeptChanged = false;

            CheckBox chkApprove = xActivity.FindViewById<CheckBox>(Resource.Id.chkApprove);
            CheckBox chkDeny = xActivity.FindViewById<CheckBox>(Resource.Id.chkDeny);
            Spinner cboReasonDeny = xActivity.FindViewById<Spinner>(Resource.Id.cboReasonDeny);

            if (chkDeny.Checked)
            {
                mod_SaveG4SApprove.RequestStatusID= (int)StatusID.Denied;
                if (cboReasonDeny.SelectedItem.ToString()=="")
                {
                    oMessage = "An error was detected: Must select the reason for deny.";
                    IsSaved = false;
                    goto exit;
                }
            }
            if (chkApprove.Checked)
            {
                if (mod_SaveG4SApprove.RequestStatusID == (int)StatusID.Approved)
                {
                    mod_SaveG4SApprove.RequestStatusID = (int)StatusID.Approved_Updated;
                    if (mod_SaveG4SApprove.IsGuard) { mod_SaveG4SApprove.RequestStatusID = (int)StatusID.Approved_Updated_GuardAssigned; }
                }
                else
                {
                    mod_SaveG4SApprove.RequestStatusID = (int)StatusID.Approved;
                    if (mod_SaveG4SApprove.IsGuard) { mod_SaveG4SApprove.RequestStatusID = (int)StatusID.Approved_GuardAssigned; }
                }
            }
            

                if (mod_SaveG4SApprove.HotelAmount != mod_G4SApprove.HotelAmount ||
                mod_SaveG4SApprove.CarRentalAmount != mod_G4SApprove.CarRentalAmount ||
                mod_SaveG4SApprove.MileageAmount != mod_G4SApprove.MileageAmount ||
                mod_SaveG4SApprove.PerDiemAmount != mod_G4SApprove.PerDiemAmount ||
                mod_SaveG4SApprove.OtherAmount != mod_G4SApprove.OtherAmount
                )
            {
                
                IsChargeChanged = true;
                if (!chkDeny.Checked && !chkApprove.Checked) {
                    mod_SaveG4SApprove.AppdCharges = false;
                    mod_SaveG4SApprove.DeniedCharges = false;
                    mod_SaveG4SApprove.RequestStatusID = (int)StatusID.Pending;
                }
            }
            //if (mod_SaveG4SApprove.DeptID != mod_G4SApprove.DeptID) { IsDeptChanged = true; }

            ORM.DBG4SRepository orb = new DBG4SRepository();
            oMessage = orb.SaveG4SApproveRequest(mod_SaveG4SApprove.ToSaveWorkFlowApprove(IsChargeChanged), mod_G4SApprove.ToSaveWorkFlowApprove(false));
            //oDatabox.Query = oQuery.SaveNewHoldRequest(oRequest)
            //if (oRequestID >= 0)
            //{
            //    oMessage = "Request saved";
            //};
            if (oMessage.ToLower().IndexOf("fail")>-1) { IsSaved = false; }
            exit:
            Toast.MakeText(xActivity, oMessage, ToastLength.Short).Show();
            if (IsSaved) { xActivity.Finish(); }
        }

        private static void btnG4S_Cancel_Click(object sender, EventArgs e)
        {
            Toast.MakeText(xActivity, "Request Cancelled", ToastLength.Short).Show();
            xActivity.Finish();
        }
        #endregion
        #endregion
        #region "Load Request Control"
        //*************************************************************************************************************
        //  Load G4S Approval Control
        //*************************************************************************************************************
        private static void LoadG4SControl_01(View oview)
        {
            TextView txtRequestID = oview.FindViewById<TextView>(Resource.Id.txtRequestID);
            TextView txtSiteName = oview.FindViewById<TextView>(Resource.Id.txtSiteName);
            TextView txtAddress1 = oview.FindViewById<TextView>(Resource.Id.txtAddress1);
            TextView txtCity = oview.FindViewById<TextView>(Resource.Id.txtCity);
            TextView txtState = oview.FindViewById<TextView>(Resource.Id.txtState);
            TextView txtContact = oview.FindViewById<TextView>(Resource.Id.txtContact);
            TextView txtOfficerType = oview.FindViewById<TextView>(Resource.Id.txtOfficerType);
            TextView txtOfficerRequired = oview.FindViewById<TextView>(Resource.Id.txtOfficerRequired);
            TextView txtStartDate = oview.FindViewById<TextView>(Resource.Id.txtStartDate);
            TextView txtEndDate = oview.FindViewById<TextView>(Resource.Id.txtEndDate);
            TextView txtWageRate = oview.FindViewById<TextView>(Resource.Id.txtWageRate);
            TextView txtBillRate = oview.FindViewById<TextView>(Resource.Id.txtBillRate);
            TextView txtMessageRate = (TextView)oview.FindViewById(Resource.Id.lblNote2);

            txtRequestID.Text = mod_SaveG4SApprove.SR_RequestID.ToString();
            txtSiteName.Text = mod_SaveG4SApprove.CustName.ToString();
            txtAddress1.Text = mod_SaveG4SApprove.Address1.ToString();
            txtCity.Text = mod_SaveG4SApprove.City.ToString();
            txtState.Text = mod_SaveG4SApprove.State.ToString();
            txtContact.Text = mod_SaveG4SApprove.Contact.ToString();
            txtOfficerType.Text = mod_SaveG4SApprove.OfficerType.ToString();
            txtOfficerRequired.Text = mod_SaveG4SApprove.OfficerNum.ToString();
            txtStartDate.Text = mod_SaveG4SApprove.StartDate.ToString();
            txtEndDate.Text = mod_SaveG4SApprove.EndDate.ToString();
            //txtWageRate.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.RequestRate.ToString()));
            if (mod_SaveG4SApprove.RequestRate == 0) { txtWageRate.Text = ""; } else { txtWageRate.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.RequestRate.ToString())); }

            //txtBillRate.Text = mod_SaveG4SApprove.bill.ToString();
            if (mod_SaveG4SApprove.CorpCustID.Trim()!="0") { 
                txtMessageRate.Text = "Please consult the applicable " + mod_SaveG4SApprove.CustName.ToString().Trim() + " Rate Sheet.";
            }

            txtWageRate.Click += (sender, args) => 
            {
                Dialog dialog = new Dialog(xActivity);
                dialog.SetTitle("Wage Rate:");
                dialog.SetContentView(Resource.Layout.SR_PopNumBox);
                dialog.Show();

                Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
                Button btnclear = (Button)dialog.FindViewById(Resource.Id.btnClear);
                Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);

                EditText txtNotes = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                if (mod_SaveG4SApprove.RequestRate == 0) { txtNotes.Text = ""; } else { txtNotes.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.RequestRate.ToString())); }

                if (btnset != null)
                {
                    btnset.Click += (osender, Args) =>
                    {
                        //txtTargetNote.Text = cboDialogSite.SelectedItem;
                        //String NoteText = txtNote.Text;
                        if (txtWageRate.Text == "") { mod_SaveG4SApprove.RequestRate = 0; } else { mod_SaveG4SApprove.RequestRate = double.Parse(txtNotes.Text, System.Globalization.NumberStyles.Currency); }
                        txtWageRate.Text = string.Format("{0:C}", mod_SaveG4SApprove.RequestRate);
                        Toast.MakeText(xActivity, "Set Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }
                if (btnclear != null)
                {
                    btnclear.Click += (osender, Args) =>
                    {
                        txtNotes.Text = "";
                        Toast.MakeText(xActivity, "Clear Editor", ToastLength.Short).Show();
                        //dialog.Cancel();
                    };
                }
                if (btncancel != null)
                {
                    btncancel.Click += (osender, Args) =>
                    {
                        Toast.MakeText(xActivity, "Cancel Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }
            };
        }
        private static void LoadG4SControl_02(View oview)
        {
            CheckBox chkAdditionalCharges = oview.FindViewById<CheckBox>(Resource.Id.chkAdditionalCharges);

            //EditText txtPONumber = oview.FindViewById<EditText>(Resource.Id.txtPONumber);
            //EditText txtCostCenterCode = oview.FindViewById<EditText>(Resource.Id.txtCostCenterCode);
            //EditText txtWorkOrderNumber = oview.FindViewById<EditText>(Resource.Id.txtWorkOrderNumber);


            TextView txtHotel = oview.FindViewById<TextView>(Resource.Id.txtHotel);
            TextView txtPerDiem = oview.FindViewById<TextView>(Resource.Id.txtPerDiem);
            TextView txtMileage = oview.FindViewById<TextView>(Resource.Id.txtMileage);
            TextView txtCarRental = oview.FindViewById<TextView>(Resource.Id.txtCarRental);
            TextView txtOther = oview.FindViewById<TextView>(Resource.Id.txtOther);

            TextView txtNotes = oview.FindViewById<TextView>(Resource.Id.txtNotes);

            chkAdditionalCharges.Checked = mod_SaveG4SApprove.AddlCharges;

            //txtPONumber.Text = mod_SaveG4SApprove.PONumber;
            //txtCostCenterCode.Text =  mod_SaveG4SApprove.CostCenterCode;
            //txtWorkOrderNumber.Text = mod_SaveG4SApprove.WorkOrderNumber;

            if (mod_SaveG4SApprove.HotelAmount == 0) { txtHotel.Text = ""; } else { txtHotel.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.HotelAmount.ToString())); }
            if (mod_SaveG4SApprove.PerDiemAmount == 0) { txtPerDiem.Text = ""; } else { txtPerDiem.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.PerDiemAmount.ToString())); }
            if (mod_SaveG4SApprove.MileageAmount == 0) { txtMileage.Text = ""; } else { txtMileage.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.MileageAmount.ToString())); }
            if (mod_SaveG4SApprove.CarRentalAmount == 0) { txtCarRental.Text = ""; } else { txtCarRental.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.CarRentalAmount.ToString())); }
            if (mod_SaveG4SApprove.OtherAmount == 0) { txtOther.Text = ""; } else { txtOther.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.OtherAmount.ToString())); }

            //txtHotel.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.HotelAmount.ToString()));
            //txtPerDiem.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.PerDiemAmount.ToString()));
            //txtMileage.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.MileageAmount.ToString()));
            //txtCarRental.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.CarRentalAmount.ToString()));
            //txtOther.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.OtherAmount.ToString()));

            txtNotes.Text = mod_SaveG4SApprove.Notes;

            txtHotel.Click += (sender, args) => 
            {
                Dialog dialog = new Dialog(xActivity);
                dialog.SetTitle("Hotel Amount:");
                dialog.SetContentView(Resource.Layout.SR_PopNumBox);
                dialog.Show();

                Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
                Button btnclear = (Button)dialog.FindViewById(Resource.Id.btnClear);
                Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);

                EditText txtNotes1 = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                if (mod_SaveG4SApprove.HotelAmount == 0) { txtNotes1.Text = ""; } else { txtNotes1.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.HotelAmount.ToString())); }

                if (btnset != null)
                {
                    btnset.Click += (osender, Args) =>
                    {
                        //txtTargetNote.Text = cboDialogSite.SelectedItem;
                        //String NoteText = txtNote.Text;
                        if (txtHotel.Text == "") { mod_SaveG4SApprove.HotelAmount = 0; } else { mod_SaveG4SApprove.HotelAmount = double.Parse(txtNotes1.Text, System.Globalization.NumberStyles.Currency); }
                        txtHotel.Text = string.Format("{0:C}", mod_SaveG4SApprove.HotelAmount);
                        Toast.MakeText(xActivity, "Set Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }
                if (btnclear != null)
                {
                    btnclear.Click += (osender, Args) =>
                    {
                        txtNotes1.Text = "";
                        Toast.MakeText(xActivity, "Clear Editor", ToastLength.Short).Show();
                        //dialog.Cancel();
                    };
                }
                if (btncancel != null)
                {
                    btncancel.Click += (osender, Args) =>
                    {
                        Toast.MakeText(xActivity, "Cancel Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }

            };
            txtPerDiem.Click += (sender, args) => 
            {
                Dialog dialog = new Dialog(xActivity);
                dialog.SetTitle("Per Diem Amount:");
                dialog.SetContentView(Resource.Layout.SR_PopNumBox);
                dialog.Show();

                Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
                Button btnclear = (Button)dialog.FindViewById(Resource.Id.btnClear);
                Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);

                EditText txtNotes2 = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                if (mod_SaveG4SApprove.PerDiemAmount == 0) { txtNotes2.Text = ""; } else { txtNotes2.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.PerDiemAmount.ToString())); }

                if (btnset != null)
                {
                    btnset.Click += (osender, Args) =>
                    {
                        //txtTargetNote.Text = cboDialogSite.SelectedItem;
                        //String NoteText = txtNote.Text;
                        if (txtPerDiem.Text == "") { mod_SaveG4SApprove.PerDiemAmount = 0; } else { mod_SaveG4SApprove.PerDiemAmount = double.Parse(txtNotes2.Text, System.Globalization.NumberStyles.Currency); }
                        txtPerDiem.Text = string.Format("{0:C}", mod_SaveG4SApprove.PerDiemAmount);
                        Toast.MakeText(xActivity, "Set Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }
                if (btnclear != null)
                {
                    btnclear.Click += (osender, Args) =>
                    {
                        txtNotes2.Text = "";
                        Toast.MakeText(xActivity, "Clear Editor", ToastLength.Short).Show();
                        //dialog.Cancel();
                    };
                }
                if (btncancel != null)
                {
                    btncancel.Click += (osender, Args) =>
                    {
                        Toast.MakeText(xActivity, "Cancel Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }

            };
            txtMileage.Click += (sender, args) => 
            {
                Dialog dialog = new Dialog(xActivity);
                dialog.SetTitle("Mileage Amount:");
                dialog.SetContentView(Resource.Layout.SR_PopNumBox);
                dialog.Show();

                Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
                Button btnclear = (Button)dialog.FindViewById(Resource.Id.btnClear);
                Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);

                EditText txtNotes3 = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                if (mod_SaveG4SApprove.MileageAmount == 0) { txtNotes3.Text = ""; } else { txtNotes3.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.MileageAmount.ToString())); }

                if (btnset != null)
                {
                    btnset.Click += (osender, Args) =>
                    {
                        //txtTargetNote.Text = cboDialogSite.SelectedItem;
                        //String NoteText = txtNote.Text;
                        if (txtMileage.Text == "") { mod_SaveG4SApprove.MileageAmount = 0; } else { mod_SaveG4SApprove.MileageAmount = double.Parse(txtNotes3.Text, System.Globalization.NumberStyles.Currency); }
                        txtMileage.Text = string.Format("{0:C}", mod_SaveG4SApprove.MileageAmount);
                        Toast.MakeText(xActivity, "Set Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }
                if (btnclear != null)
                {
                    btnclear.Click += (osender, Args) =>
                    {
                        txtNotes3.Text = "";
                        Toast.MakeText(xActivity, "Clear Editor", ToastLength.Short).Show();
                        //dialog.Cancel();
                    };
                }
                if (btncancel != null)
                {
                    btncancel.Click += (osender, Args) =>
                    {
                        Toast.MakeText(xActivity, "Cancel Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }

            };
            txtCarRental.Click += (sender, args) => 
            {
                Dialog dialog = new Dialog(xActivity);
                dialog.SetTitle("Car Rental Amount:");
                dialog.SetContentView(Resource.Layout.SR_PopNumBox);
                dialog.Show();

                Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
                Button btnclear = (Button)dialog.FindViewById(Resource.Id.btnClear);
                Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);

                EditText txtNotes4 = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                if (mod_SaveG4SApprove.CarRentalAmount == 0) { txtNotes4.Text = ""; } else { txtNotes4.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.CarRentalAmount.ToString())); }

                if (btnset != null)
                {
                    btnset.Click += (osender, Args) =>
                    {
                        //txtTargetNote.Text = cboDialogSite.SelectedItem;
                        //String NoteText = txtNote.Text;
                        if (txtCarRental.Text == "") { mod_SaveG4SApprove.CarRentalAmount = 0; } else { mod_SaveG4SApprove.CarRentalAmount = double.Parse(txtNotes4.Text, System.Globalization.NumberStyles.Currency); }
                        txtCarRental.Text = string.Format("{0:C}", mod_SaveG4SApprove.CarRentalAmount);
                        Toast.MakeText(xActivity, "Set Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }
                if (btnclear != null)
                {
                    btnclear.Click += (osender, Args) =>
                    {
                        txtNotes4.Text = "";
                        Toast.MakeText(xActivity, "Clear Editor", ToastLength.Short).Show();
                        //dialog.Cancel();
                    };
                }
                if (btncancel != null)
                {
                    btncancel.Click += (osender, Args) =>
                    {
                        Toast.MakeText(xActivity, "Cancel Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }

            };
            txtOther.Click += (sender, args) => 
            {
                Dialog dialog = new Dialog(xActivity);
                dialog.SetTitle("Other Amount:");
                dialog.SetContentView(Resource.Layout.SR_PopNumBox);
                dialog.Show();

                Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
                Button btnclear = (Button)dialog.FindViewById(Resource.Id.btnClear);
                Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);

                EditText txtNotes5 = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                if (mod_SaveG4SApprove.OtherAmount == 0) { txtNotes5.Text = ""; } else { txtNotes5.Text = string.Format("{0:C}", Decimal.Parse(mod_SaveG4SApprove.OtherAmount.ToString())); }

                if (btnset != null)
                {
                    btnset.Click += (osender, Args) =>
                    {
                        //txtTargetNote.Text = cboDialogSite.SelectedItem;
                        //String NoteText = txtNote.Text;
                        if (txtOther.Text == "") { mod_SaveG4SApprove.OtherAmount = 0; } else { mod_SaveG4SApprove.OtherAmount = double.Parse(txtNotes5.Text, System.Globalization.NumberStyles.Currency); }
                        txtOther.Text = string.Format("{0:C}", mod_SaveG4SApprove.OtherAmount);
                        Toast.MakeText(xActivity, "Set Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }
                if (btnclear != null)
                {
                    btnclear.Click += (osender, Args) =>
                    {
                        txtNotes5.Text = "";
                        Toast.MakeText(xActivity, "Clear Editor", ToastLength.Short).Show();
                        //dialog.Cancel();
                    };
                }
                if (btncancel != null)
                {
                    btncancel.Click += (osender, Args) =>
                    {
                        Toast.MakeText(xActivity, "Cancel Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }

            };

            txtNotes.Click += (sender, args) => 
            {
                Dialog dialog = new Dialog(xActivity);
                dialog.SetTitle("Notes:");
                dialog.SetContentView(Resource.Layout.SR_PopEditBox);
                dialog.Show();

                Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
                Button btnclear = (Button)dialog.FindViewById(Resource.Id.btnClear);
                Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);

                EditText txtNotes6 = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                txtNotes6.Text = mod_SaveG4SApprove.Notes.ToString();

                if (btnset != null)
                {
                    btnset.Click += (osender, Args) =>
                    {
                        //txtTargetNote.Text = cboDialogSite.SelectedItem;
                        //String NoteText = txtNote.Text;
                        mod_SaveG4SApprove.Notes = txtNotes6.Text;
                        txtNotes.Text = txtNotes6.Text;
                        Toast.MakeText(xActivity, "Set Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }
                if (btnclear != null)
                {
                    btnclear.Click += (osender, Args) =>
                    {
                        txtNotes6.Text = "";
                        Toast.MakeText(xActivity, "Clear Editor", ToastLength.Short).Show();
                        //dialog.Cancel();
                    };
                }
                if (btncancel != null)
                {
                    btncancel.Click += (osender, Args) =>
                    {
                        Toast.MakeText(xActivity, "Cancel Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }

            };

        }
        private static void LoadG4SControl_03(View oview)
        {
            CheckBox chkApprove = oview.FindViewById<CheckBox>(Resource.Id.chkApprove);
            CheckBox chkDeny = oview.FindViewById<CheckBox>(Resource.Id.chkDeny);

            mod_SaveG4SApprove.Approved = false;
            mod_SaveG4SApprove.Denied = false;

            if (mod_SaveG4SApprove.RequestStatusID == (int)StatusID.Approved_Updated_GuardAssigned
                || mod_SaveG4SApprove.RequestStatusID == (int)StatusID.Approved_Updated
                || mod_SaveG4SApprove.RequestStatusID == (int)StatusID.Approved) { mod_SaveG4SApprove.Approved = true; }
            if (mod_SaveG4SApprove.RequestStatusID == (int)StatusID.Denied) { mod_SaveG4SApprove.Denied = true; }

            chkApprove.Checked = mod_SaveG4SApprove.Approved;
            chkDeny.Checked = mod_SaveG4SApprove.Denied;
        }
        private static void LoadG4SControl_04(View oview)
        {
        }
        #endregion
        #region "Load Save Module"
        static void LoadSaveG4SApproval(SR_G4SApprove oRequest, View oView_00, View oView_01, View oView_02)
        {

            

            // *** Page 01 ***************************************************************************
            
            TextView txtRequestID = oView_00.FindViewById<TextView>(Resource.Id.txtRequestID);
            TextView txtSiteName = oView_00.FindViewById<TextView>(Resource.Id.txtSiteName);
            TextView txtAddress1 = oView_00.FindViewById<TextView>(Resource.Id.txtAddress1);
            TextView txtCity = oView_00.FindViewById<TextView>(Resource.Id.txtCity);
            TextView txtState = oView_00.FindViewById<TextView>(Resource.Id.txtState);
            TextView txtContact = oView_00.FindViewById<TextView>(Resource.Id.txtContact);
            TextView txtOfficerType = oView_00.FindViewById<TextView>(Resource.Id.txtOfficerType);
            TextView txtOfficerRequired = oView_00.FindViewById<TextView>(Resource.Id.txtOfficerRequired);
            TextView txtStartDate = oView_00.FindViewById<TextView>(Resource.Id.txtStartDate);
            TextView txtEndDate = oView_00.FindViewById<TextView>(Resource.Id.txtEndDate);
            TextView txtWageRate = oView_00.FindViewById<TextView>(Resource.Id.txtWageRate);
            TextView txtBillRate = oView_00.FindViewById<TextView>(Resource.Id.txtBillRate);

            // *** Page 02 ***************************************************************************
            CheckBox chkAdditionalCharges = oView_01.FindViewById<CheckBox>(Resource.Id.chkAdditionalCharges);

            EditText txtPONumber = oView_01.FindViewById<EditText>(Resource.Id.txtPONumber);
            EditText txtCostCenterCode = oView_01.FindViewById<EditText>(Resource.Id.txtCostCenterCode);
            EditText txtWorkOrderNumber = oView_01.FindViewById<EditText>(Resource.Id.txtWorkOrderNumber);

            TextView txtHotel = oView_01.FindViewById<TextView>(Resource.Id.txtHotel);
            TextView txtPerDiem = oView_01.FindViewById<TextView>(Resource.Id.txtPerDiem);
            TextView txtMileage = oView_01.FindViewById<TextView>(Resource.Id.txtMileage);
            TextView txtCarRental = oView_01.FindViewById<TextView>(Resource.Id.txtCarRental);
            TextView txtOther = oView_01.FindViewById<TextView>(Resource.Id.txtOther);

            TextView txtNotes = oView_01.FindViewById<TextView>(Resource.Id.txtNotes);

            // *** Page 03 ***************************************************************************

            CheckBox chkApprove = oView_01.FindViewById<CheckBox>(Resource.Id.chkApprove);
            CheckBox chkDeny = oView_01.FindViewById<CheckBox>(Resource.Id.chkDeny);

            // *** Loading Module ********************************************************************
            // *** Page 01 ***************************************************************************
            //if (cboDepartment != null) { };

            if (txtRequestID != null) { mod_SaveG4SApprove.SR_RequestID = Convert.ToInt32( txtRequestID.Text); };
            if (txtSiteName  != null) { mod_SaveG4SApprove.CustName = txtSiteName.Text; };
            if (txtAddress1  != null) { mod_SaveG4SApprove.Address1 = txtAddress1.Text; };
            if (txtCity  != null) { mod_SaveG4SApprove.City = txtCity.Text; };
            if (txtState  != null) { mod_SaveG4SApprove.State = txtState.Text; };
            if (txtContact  != null) { mod_SaveG4SApprove.Contact = txtContact.Text; };
            if (txtOfficerType != null) { mod_SaveG4SApprove.OfficerType = txtOfficerType.Text; };
            if (txtOfficerRequired  != null) { mod_SaveG4SApprove.OfficerNum = txtOfficerRequired.Text; };
            if (txtStartDate  != null) { mod_SaveG4SApprove.StartDate = txtStartDate.Text; };
            if (txtEndDate  != null) { mod_SaveG4SApprove.EndDate = txtEndDate.Text; };

            if (txtWageRate != null) { if (txtWageRate.Text == "") { mod_SaveG4SApprove.RequestRate = 0; } else { mod_SaveG4SApprove.RequestRate = double.Parse(txtWageRate.Text, System.Globalization.NumberStyles.Currency); } };
            //if (txtWageRate  != null) { mod_SaveG4SApprove.RequestRate = double.Parse(txtWageRate.Text, System.Globalization.NumberStyles.Currency); };
            //if (txtBillRate  != null) { mod_SaveG4SApprove.BillRate = txtBillRate.Text; };

            // *** Page 02 ***************************************************************************

            if (chkAdditionalCharges != null) { mod_SaveG4SApprove.AddlCharges = chkAdditionalCharges.Checked; };

            //if (txtHotel != null) { mod_SaveG4SApprove.HotelAmount= double.Parse(txtHotel.Text,System.Globalization.NumberStyles.Currency); };
            //if (txtPerDiem != null) { mod_SaveG4SApprove.PerDiemAmount = double.Parse(txtPerDiem.Text, System.Globalization.NumberStyles.Currency); };
            //if (txtMileage != null) { mod_SaveG4SApprove.MileageAmount= double.Parse(txtMileage.Text, System.Globalization.NumberStyles.Currency); };
            //if (txtCarRental != null) { mod_SaveG4SApprove.CarRentalAmount= double.Parse(txtCarRental.Text, System.Globalization.NumberStyles.Currency); };
            //if (txtOther != null) { mod_SaveG4SApprove.OtherAmount= double.Parse(txtOther.Text, System.Globalization.NumberStyles.Currency); };

            if (txtPONumber != null) { mod_SaveG4SApprove.PONumber = txtPONumber.Text; };
            if (txtCostCenterCode != null) { mod_SaveG4SApprove.CostCenterCode = txtCostCenterCode.Text; };
            if (txtWorkOrderNumber != null) { mod_SaveG4SApprove.WorkOrderNumber = txtWorkOrderNumber.Text; };

            if (txtHotel != null) { if (txtHotel.Text == "") { mod_SaveG4SApprove.HotelAmount = 0; } else { mod_SaveG4SApprove.HotelAmount = double.Parse(txtHotel.Text, System.Globalization.NumberStyles.Currency); } };
            if (txtPerDiem != null) { if (txtPerDiem.Text == "") { mod_SaveG4SApprove.PerDiemAmount = 0; } else { mod_SaveG4SApprove.PerDiemAmount = double.Parse(txtPerDiem.Text, System.Globalization.NumberStyles.Currency); } };
            if (txtMileage != null) { if (txtMileage.Text == "") { mod_SaveG4SApprove.MileageAmount = 0; } else { mod_SaveG4SApprove.MileageAmount = double.Parse(txtMileage.Text, System.Globalization.NumberStyles.Currency); } };
            if (txtCarRental != null) { if (txtCarRental.Text == "") { mod_SaveG4SApprove.CarRentalAmount = 0; } else { mod_SaveG4SApprove.CarRentalAmount = double.Parse(txtCarRental.Text, System.Globalization.NumberStyles.Currency); } };
            if (txtOther != null) { if (txtOther.Text == "") { mod_SaveG4SApprove.OtherAmount = 0; } else { mod_SaveG4SApprove.OtherAmount = double.Parse(txtOther.Text, System.Globalization.NumberStyles.Currency); } };

            // *** Page 03 ***************************************************************************

            if (chkApprove != null) { mod_SaveG4SApprove.AppdCharges = chkApprove.Checked; };
            if (chkDeny != null) { mod_SaveG4SApprove.DeniedCharges = chkDeny.Checked; };

            //Spinner cboReasonDeny = oView.FindViewById<Spinner>(Resource.Id.cboReasonDeny);


        }
        #endregion
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using Java.Lang;
using STSR_Droid.Models;
using STSR_Droid.ORM;

using STSR_Droid.Business;
using SR_Class;
using SQLite;
using STSR_Droid.SQLiteTables;


namespace STSR_Droid.Class
{

    public partial  class SlidingTabFragment : Fragment, DatePickerDialog.IOnDateSetListener, TimePickerDialog.IOnTimeSetListener
    {


        #region "Init Request Control"


        //*************************************************************************************************************
        //  Init Request Control
        //*************************************************************************************************************


        private static void InitRequestControl_01(View oview)
        {
            //xSliding = oSliding;
            // Assign Values
            // *****************************************************************************************
            //if (!Loaded_Page1)
            //{
            //    mod_Request = oRequest;
            //    mod_SaveRequest = oRequest;
            //}
            // Load Customer List in function of User Access
            // *****************************************************************************************
            TextView txtSite = oview.FindViewById<TextView>(Resource.Id.txtSite);
            View xView = oview;

            DBLckUpRepository dbr = new DBLckUpRepository();
            oLckCustomer.Lckitem = dbr.CustomerList();
            Spinner cboCustomer = oview.FindViewById<Spinner>(Resource.Id.cboCustomer);
            cboCustomer.Adapter = oLckCustomer.GetAdapter(oview);

            // When the customer is unique
            // *****************************************************************************************
            if (cboCustomer.Count == 1)
            {
                cboCustomer.SetSelection(0);
                cboCustomer.Enabled = false;

                // When the customer is unique, the dropdown list is not enabled
                // *****************************************************************************************
                string oCode = oLckCustomer.Lckitem[0].Index;
                oLckSite.Lckitem = dbr.SiteList(oCode);
                //EditText txtSite = oview.FindViewById<EditText>(Resource.Id.txtSite);
                //cboSite.Adapter = oLckSite.GetAdapter(oview);

                // If it is a new Request the site is positioned to the begining
                // *****************************************************************************************
                int initialSitePosition;
                if (mod_SaveRequest.RequestID == 0)
                {
                    initialSitePosition = 0;  // cboSite.SelectedItemPosition;
                } else
                {
                    // The site position is indicated by the Request ID
                    // *****************************************************************************************

                    initialSitePosition = oLckSite.GetPosition(mod_SaveRequest.SiteID, oLckSite);
                    //cboSite.SetSelection(oLckSite.GetPosition(mod_SaveRequest.SiteID, oLckSite));
                    //initialSitePosition = cboSite.SelectedItemPosition;
                }

                // Set the Click method for the dropdown Site
                // *****************************************************************************************
                //cboSite.ItemSelected += (sender, args) =>
                //{
                //    if (args.Position != initialSitePosition)
                //    {
                //        cboSite_ItemClickListener(oview, (int)(args.Id));
                //    }
                //};


            } else
            {
                // *****************************************************************************************

                // When the customer is not unique, the dropdown list is enabled and positioned to 0
                // *****************************************************************************************
                int initialCustomerPosition;
                if (mod_SaveRequest.RequestID == 0)
                {
                     initialCustomerPosition = cboCustomer.SelectedItemPosition;
                }
                else
                {
                    cboCustomer.SetSelection(oLckCustomer.GetPosition(mod_SaveRequest.CorpCustID, oLckCustomer));
                     initialCustomerPosition = cboCustomer.SelectedItemPosition;

                    //// When the customer is unique, the dropdown list is not enabled
                    //// *****************************************************************************************
                    //string oCode = oLckCustomer.Lckitem[initialCustomerPosition].Index;
                    //oLckSite.Lckitem = dbr.SiteList(oCode);
                    //Spinner cboSite = oview.FindViewById<Spinner>(Resource.Id.cboSite);
                    //cboSite.Adapter = oLckSite.GetAdapter(oview);

                    //cboSite.SetSelection(oLckSite.GetPosition(mod_SaveRequest.SiteID, oLckSite));
                    //int initialSitePosition = cboSite.SelectedItemPosition;
                }
                cboCustomer.ItemSelected += (sender, args) =>
                {
                    if (args.Position != initialCustomerPosition)
                    {
                        cboCustomer_ItemClickListener(oview, (int)(args.Id));
                        
                    }
                };

                txtSite.Click += (sender, args) =>
                {



                    int CustPos = cboCustomer.SelectedItemPosition;
                    if (CustPos != 0)
                    {
                        // Invoke the SR_SiteLocator
                        Dialog dialog = new Dialog(xActivity);
                        dialog.SetTitle("Site Location:");
                        dialog.SetContentView(Resource.Layout.SR_SiteLocator);
                        dialog.Show();


                        Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
                        Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);
                        Spinner cboDialogState = (Spinner)dialog.FindViewById(Resource.Id.cboState);

                        //string oState = oLckState.Lckitem[Pos].Index;
                        //DBLckUpRepository dbr = new DBLckUpRepository();
                        oLckState.Lckitem = dbr.StateList();
                        cboDialogState.Adapter = oLckState.GetAdapter(oview);




                        Spinner cboDialogSite = (Spinner)dialog.FindViewById(Resource.Id.cboSite);
                        //Spinner cboDialogState = (Spinner)dialog.FindViewById(Resource.Id.cboState);
                        TextView txtTargetSite = (TextView)xActivity.FindViewById(Resource.Id.txtSite);

                        string oCode1 = oLckCustomer.Lckitem[CustPos].Index;
                        string oState1 = "AK";
                        oLckSite.Lckitem = dbr.SiteListbyState(oCode1, oState1);
                        cboDialogSite.Adapter = oLckSite.GetAdapter(oview);

                        int initStatePosition = cboDialogState.SelectedItemPosition;
                        cboDialogState.ItemSelected += (xsender, xargs) =>
                        {
                           
                            if (xargs.Position != initStatePosition)
                            {
                                cboDialogState_ItemClickListener(dialog, (int)(xargs.Id));
                                //DBLckUpRepository dbr = new DBLckUpRepository();
                                string oCode3 = oLckCustomer.Lckitem[CustPos].Index;
                                string oState3 = oLckState.Lckitem[xargs.Position].Index;
                                oLckSite.Lckitem = dbr.SiteListbyState(oCode3, oState3);
                                cboDialogSite.Adapter = oLckSite.GetAdapter(oview);
                                //txtTargetSite.Text = oLckSite.Lckitem[args.Position].Value;

                                // **************************************************************************************************
                                int initSitePosition = cboDialogSite.SelectedItemPosition;
                                cboDialogSite.ItemSelected += (osender, oargs) =>
                                {
                                    if (oargs.Position != initSitePosition)
                                    {
                                        cboDialogSite_ItemClickListener(dialog, (int)(oargs.Id));
                                        mod_SaveRequest.SiteID = oLckSite.Lckitem[oargs.Position].Index;
                                        txtTargetSite.Text = oLckSite.Lckitem[oargs.Position].Value;
                                    }
                                };

                            }

                        };



                        if (btncancel != null)
                        {
                            btncancel.Click += (osender, Args) =>
                            {
                                Toast.MakeText(xActivity, "Cancel Editor", ToastLength.Short).Show();
                                dialog.Cancel();
                            };
                        }
                        if (btnset != null)
                        {
                            btnset.Click += (osender, Args) =>
                            {
                                //txtTargetNote.Text = cboDialogSite.SelectedItem;
                                //String NoteText = txtNote.Text;
                                Toast.MakeText(xActivity, "Set Editor", ToastLength.Short).Show();
                                dialog.Cancel();
                            };
                        }



                        // Get the data source (Move the Data Source to SR_SiteLocator) Store the oCode Value
                        string oCode = oLckCustomer.Lckitem[CustPos].Index;
                        //DBLckUpRepository dbr = new DBLckUpRepository();
                        oLckSite.Lckitem = dbr.SiteList(oCode);

                    }
                };
            }
            
            // *****************************************************************************************

        }


        private static void InitRequestControl_02(View oview)
        {
            //xActivity = oActivity;
            DBLckUpRepository dbr = new DBLckUpRepository();

            // Load Officer Type List
            // *****************************************************************************************
            oLckOfficerType.Lckitem = dbr.OfficerTypeList();
            Spinner cboOfficerType = oview.FindViewById<Spinner>(Resource.Id.cboOfficerType);
            cboOfficerType.Adapter = oLckOfficerType.GetAdapter(oview);
            // cboOfficerType
            cboOfficerType.SetSelection(oLckOfficerType.GetPosition(mod_SaveRequest.OfficerType, oLckOfficerType));

            cboOfficerType.ItemSelected += (sender, args) =>
            {
                mod_SaveRequest.OfficerType = oLckOfficerType.Lckitem[args.Position].Index;
            };



            // *****************************************************************************************
            // Manage Officer Reason of Services
            // *****************************************************************************************
            oLckReasonServices.Lckitem = dbr.ReasonServicesList();
            Spinner cboReasonServices = oview.FindViewById<Spinner>(Resource.Id.cboReasonServices);
            Button btnReasonServices = oview.FindViewById<Button>(Resource.Id.btnReasonServices);
            TextView txtReasonServices = oview.FindViewById<TextView>(Resource.Id.txtReasonServices);
            cboReasonServices.Adapter = oLckReasonServices.GetAdapter(oview);
            // cboReasonServices
            cboReasonServices.SetSelection(oLckReasonServices.GetPosition(mod_SaveRequest.ReasonServiceID.ToString(), oLckReasonServices));

            btnReasonServices.Click += (rs_sender, rs_args) => { cboReasonServices.PerformClick(); };
            txtReasonServices.Click += (rs_sender, rs_args) => 
            {
                // Invoke the SR_SiteLocator
                Dialog dialog = new Dialog(xActivity);
                dialog.SetTitle("Reason Services Description:");
                dialog.SetContentView(Resource.Layout.SR_PopTextBox);
                dialog.Show();

                Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
                Button btnclear = (Button)dialog.FindViewById(Resource.Id.btnClear);
                Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);

                EditText txtNotes = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                txtNotes.Text = mod_SaveRequest.ReasonService;
                int rs_position = mod_SaveRequest.ReasonServiceID;

                if (btnset != null)
                {
                    btnset.Click += (osender, Args) =>
                    {
                        //txtTargetNote.Text = cboDialogSite.SelectedItem;
                        //String NoteText = txtNote.Text;                        
                        mod_SaveRequest.ReasonService = txtNotes.Text;
                        txtReasonServices.Text = txtNotes.Text;
                        
                        //if (mod_SaveRequest.ReasonServiceID == 4) { Toast.MakeText(xActivity, "Please note you have chosen Emergency / Disaster Services and you would be billed a the rate of $42.00 per hour for the Service.", ToastLength.Short).Show(); }
                        //Toast.MakeText(xActivity, "Set Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }
                if (btnclear != null)
                {
                    btnclear.Click += (osender, Args) =>
                    {
                        txtNotes.Text = "";
                        Toast.MakeText(xActivity, "Clear Editor", ToastLength.Short).Show();
                        //dialog.Cancel();
                    };
                }
                if (btncancel != null)
                {
                    btncancel.Click += (osender, Args) =>
                    {
                        Toast.MakeText(xActivity, "Cancel Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }

            };

            int initialReasonServicesPosition;

            initialReasonServicesPosition = cboReasonServices.SelectedItemPosition;
            bool LoadComplete = false;
            cboReasonServices.ItemSelected += (sender, args) =>
            {
                //initialReasonServicesPosition = oLckReasonServices.GetPosition(mod_SaveRequest.ReasonServiceID.ToString(), oLckReasonServices);
                mod_SaveRequest.ReasonServiceID = Convert.ToInt32(oLckReasonServices.Lckitem[args.Position].Index);

                if (mod_SaveRequest.ReasonServiceID == 99)
                {
                    btnReasonServices.Visibility = ViewStates.Visible;
                    txtReasonServices.Visibility = ViewStates.Visible;
                    cboReasonServices.Visibility = ViewStates.Invisible;

                }
                else
                {
                    btnReasonServices.Visibility = ViewStates.Invisible;
                    txtReasonServices.Visibility = ViewStates.Invisible;
                    cboReasonServices.Visibility = ViewStates.Visible;
                };

                if (oLckReasonServices.Lckitem[args.Position].Index!= initialReasonServicesPosition.ToString())
                {
                    if (oLckReasonServices.Lckitem[args.Position].Index == "4" )
                    {
                        Toast.MakeText(xActivity, "Please note you have chosen Emergency / Disaster Services and you would be billed a the rate of $42.00 per hour for the Service", ToastLength.Long).Show();
                    }


                    if (oLckReasonServices.Lckitem[args.Position].Index == "99" && LoadComplete)
                    {
                        // Invoke the SR_SiteLocator
                        Dialog dialog = new Dialog(xActivity);
                        dialog.SetTitle("Reason Services Description:");
                        dialog.SetContentView(Resource.Layout.SR_PopTextBox);
                        dialog.Show();

                        Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
                        Button btnclear = (Button)dialog.FindViewById(Resource.Id.btnClear);
                        Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);

                        EditText txtNotes = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                        txtNotes.Text = mod_SaveRequest.ReasonService;

                        if (btnset != null)
                        {
                            btnset.Click += (osender, Args) =>
                            {
                                //txtTargetNote.Text = cboDialogSite.SelectedItem;
                                //String NoteText = txtNote.Text;
                                mod_SaveRequest.ReasonService = txtNotes.Text;
                                txtReasonServices.Text = txtNotes.Text;
                                Toast.MakeText(xActivity, "Set Editor", ToastLength.Short).Show();
                                dialog.Cancel();
                            };
                        }
                        if (btnclear != null)
                        {
                            btnclear.Click += (osender, Args) =>
                            {
                                txtNotes.Text = "";
                                Toast.MakeText(xActivity, "Clear Editor", ToastLength.Short).Show();
                                //dialog.Cancel();
                            };
                        }
                        if (btncancel != null)
                        {
                            btncancel.Click += (osender, Args) =>
                            {
                                Toast.MakeText(xActivity, "Cancel Editor", ToastLength.Short).Show();
                                dialog.Cancel();
                            };
                        }
                    }
                    else { LoadComplete = true; }


                }
            };

            // *****************************************************************************************
            // Manage Location Type List
            // *****************************************************************************************
            oLckLocationType.Lckitem = dbr.LocationTypeList();
            Spinner cboLocationType = oview.FindViewById<Spinner>(Resource.Id.cboLocationType);
            Button btnLocationType = oview.FindViewById<Button>(Resource.Id.btnLocationType);
            TextView txtLocationType = oview.FindViewById<TextView>(Resource.Id.txtLocationType);
            cboLocationType.Adapter = oLckLocationType.GetAdapter(oview);
            // cboLocationType
            cboLocationType.SetSelection(oLckLocationType.GetPosition(mod_SaveRequest.LocationTypeID.ToString(), oLckLocationType));

            btnLocationType.Click += (lt_sender, lt_args) => { cboLocationType.PerformClick(); };
            txtLocationType.Click += (lt_sender, lt_args) => 
            {
                Dialog dialog = new Dialog(xActivity);
                dialog.SetTitle("Location Type Description:");
                dialog.SetContentView(Resource.Layout.SR_PopTextBox);
                dialog.Show();

                Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
                Button btnclear = (Button)dialog.FindViewById(Resource.Id.btnClear);
                Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);

                EditText txtNotes = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                txtNotes.Text = mod_SaveRequest.LocationType;

                if (btnset != null)
                {
                    btnset.Click += (osender, Args) =>
                    {
                        //txtTargetNote.Text = cboDialogSite.SelectedItem;
                        //String NoteText = txtNote.Text;
                        mod_SaveRequest.LocationType = txtNotes.Text;
                        txtLocationType.Text = txtNotes.Text;
                        Toast.MakeText(xActivity, "Set Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }
                if (btnclear != null)
                {
                    btnclear.Click += (osender, Args) =>
                    {
                        txtNotes.Text = "";
                        Toast.MakeText(xActivity, "Clear Editor", ToastLength.Short).Show();
                        //dialog.Cancel();
                    };
                }
                if (btncancel != null)
                {
                    btncancel.Click += (osender, Args) =>
                    {
                        Toast.MakeText(xActivity, "Cancel Editor", ToastLength.Short).Show();
                        dialog.Cancel();
                    };
                }

            };

            int initialLocationTypePosition;

            initialLocationTypePosition = cboLocationType.SelectedItemPosition;
            bool LoadLocationTypeComplete = false;
            cboLocationType.ItemSelected += (sender, args) =>
            {
                mod_SaveRequest.LocationTypeID = Convert.ToInt32(oLckLocationType.Lckitem[args.Position].Index);

                if (mod_SaveRequest.LocationTypeID == 99)
                {
                    btnLocationType.Visibility = ViewStates.Visible;
                    txtLocationType.Visibility = ViewStates.Visible;
                    cboLocationType.Visibility = ViewStates.Invisible;

                }
                else
                {
                    btnLocationType.Visibility = ViewStates.Invisible;
                    txtLocationType.Visibility = ViewStates.Invisible;
                    cboLocationType.Visibility = ViewStates.Visible;
                };

                if (oLckLocationType.Lckitem[args.Position].Index != initialLocationTypePosition.ToString())
                {


                    if (oLckLocationType.Lckitem[args.Position].Index == "99" && LoadLocationTypeComplete)
                    {
                        // Invoke the SR_SiteLocator
                        Dialog dialog = new Dialog(xActivity);
                        dialog.SetTitle("Location Type Description:");
                        dialog.SetContentView(Resource.Layout.SR_PopTextBox);
                        dialog.Show();

                        Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
                        Button btnclear = (Button)dialog.FindViewById(Resource.Id.btnClear);
                        Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);

                        EditText txtNotes = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                        txtNotes.Text = mod_SaveRequest.LocationType;

                        if (btnset != null)
                        {
                            btnset.Click += (osender, Args) =>
                            {
                                //txtTargetNote.Text = cboDialogSite.SelectedItem;
                                //String NoteText = txtNote.Text;
                                mod_SaveRequest.LocationType = txtNotes.Text;
                                txtLocationType.Text = txtNotes.Text;
                                Toast.MakeText(xActivity, "Set Editor", ToastLength.Short).Show();
                                dialog.Cancel();
                            };
                        }
                        if (btnclear != null)
                        {
                            btnclear.Click += (osender, Args) =>
                            {
                                txtNotes.Text = "";
                                Toast.MakeText(xActivity, "Clear Editor", ToastLength.Short).Show();
                                //dialog.Cancel();
                            };
                        }
                        if (btncancel != null)
                        {
                            btncancel.Click += (osender, Args) =>
                            {
                                Toast.MakeText(xActivity, "Cancel Editor", ToastLength.Short).Show();
                                dialog.Cancel();
                            };
                        }
                    }
                    else { LoadLocationTypeComplete = true; }


                }

            };



            // ***************************************************************************************************
            Button btnRequestDate = oview.FindViewById<Button>(Resource.Id.btnRequestDate);
            Button btnStartDate = oview.FindViewById<Button>(Resource.Id.btnStartDate);
            Button btnEndDate = oview.FindViewById<Button>(Resource.Id.btnEndDate);
            if (btnRequestDate != null)
            {
                btnRequestDate.Click += (sender, args) =>
                {
                    var dialog = new DatePickerDialogFragment(xActivity, DateTime.Now, xSliding, "btnRequestDate");
                    SlidingTabFragment.Refclassitem = "btnRequestDate";
                    dialog.Show(xFragment, null);
                };
            }
            if (btnStartDate != null)
            {
                btnStartDate.Click += (sender, args) =>
                {
                    var dialog = new DatePickerDialogFragment(xActivity, DateTime.Now, xSliding, "btnStartDate");
                    SlidingTabFragment.Refclassitem = "btnStartDate";
                    dialog.Show(xFragment, null);
                };
            }
            if (btnEndDate != null)
            {
                btnEndDate.Click += (sender, args) =>
                {
                    var dialog = new DatePickerDialogFragment(xActivity, DateTime.Now, xSliding, "btnEndDate");
                    SlidingTabFragment.Refclassitem = "btnEndDate";
                    dialog.Show(xFragment, null);
                };
            }
            // ***************************************************************************************************
            Button btnStartTime = oview.FindViewById<Button>(Resource.Id.btnStartTime);
            Button btnEndTime = oview.FindViewById<Button>(Resource.Id.btnEndTime);
            if (btnStartTime != null)
            {
                btnStartTime.Click += (sender, args) =>
                {
                    var dialog = new TimePickerDialogFragment(xActivity, DateTime.Now, xSliding, "btnStartTime");
                    SlidingTabFragment.Refclassitem = "btnStartTime";
                    dialog.Show(xFragment, null);
                };
            }
            if (btnEndTime != null)
            {
                btnEndTime.Click += (sender, args) =>
                {
                    var dialog = new TimePickerDialogFragment(xActivity, DateTime.Now, xSliding, "btnEndTime");
                    SlidingTabFragment.Refclassitem = "btnEndTime";
                    dialog.Show(xFragment, null);
                };
            }

        }
        private static void InitRequestControl_03(View oview)
        {
            TextView txtPONumber = oview.FindViewById<TextView>(Resource.Id.txtPONumber);
            TextView txtCostCenterCode = oview.FindViewById<TextView>(Resource.Id.txtCostCenterCode);
            TextView txtWorkOrderNumber = oview.FindViewById<TextView>(Resource.Id.txtWorkOrderNumber);



        }
        private  static void InitRequestControl_04(View oview)
        {
            // ***************************************************************************************************
            Button btnTerminateDate = oview.FindViewById<Button>(Resource.Id.btnTerminateDate);
            if (btnTerminateDate != null)
            {
                btnTerminateDate.Click += (sender, args) =>
                {
                    var dialog = new DatePickerDialogFragment(xActivity, DateTime.Now, xSliding, "btnTerminateDate");
                    SlidingTabFragment.Refclassitem = "btnTerminateDate";
                    dialog.Show(xFragment, null);
                };
            }
            // ***************************************************************************************************
            Button btnTerminateTime = oview.FindViewById<Button>(Resource.Id.btnTerminateTime);
            if (btnTerminateTime != null)
            {
                btnTerminateTime.Click += (sender, args) =>
                {
                    var dialog = new TimePickerDialogFragment(xActivity, DateTime.Now, xSliding, "btnTerminateTime");
                    SlidingTabFragment.Refclassitem = "btnTerminateTime";
                    dialog.Show(xFragment, null);
                };
            }


            // *****************************************************************************************

            TextView txtNotes = oview.FindViewById<TextView>(Resource.Id.txtNote);
            txtNotes.Click += TxtNotes_Click1;
      

        }
        //private static void btnReasonServices_Click(object sender, EventArgs e)
        //{

        //}
        //private static void btnLocationType_Click(object sender, EventArgs e)
        //{

        //}
        private static void TxtNotes_Click1(object sender, EventArgs e)
        {
            Dialog dialog = new Dialog(xActivity);
            dialog.SetTitle("Notes:");
            dialog.SetContentView(Resource.Layout.SR_PopEditBox);
            dialog.Show();

            EditText txtSource = (EditText)dialog.FindViewById(Resource.Id.txtNote);
            TextView txtTarget = (TextView)xActivity.FindViewById(Resource.Id.txtNote);
            Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
            Button btnclear = (Button)dialog.FindViewById(Resource.Id.btnClear);
            Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);

            txtSource.Text = txtTarget.Text;

            if (btncancel != null)
            {
                btncancel.Click += (osender, Args) =>
                {
                    Toast.MakeText(xActivity, "Cancel Editor", ToastLength.Short).Show();
                    dialog.Cancel();
                };
            }
            if (btnclear != null)
            {
                btnclear.Click += (osender, Args) =>
                {
                    EditText txtSourceNote = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                    txtSourceNote.Text = "";
                    Toast.MakeText(xActivity, "Clear Editor", ToastLength.Short).Show();
                };
            }
            if (btnset != null)
            {
                btnset.Click += (osender, Args) =>
                {
                    EditText txtSourceNote = (EditText)dialog.FindViewById(Resource.Id.txtNote);
                    TextView txtTargetNote = (TextView)xActivity.FindViewById(Resource.Id.txtNote);
                    txtTargetNote.Text = txtSourceNote.Text;
                    //String NoteText = txtNote.Text;
                    Toast.MakeText(xActivity, "Set Editor", ToastLength.Short).Show();
                    dialog.Cancel();
                };
            }
        }

        private static void InitRequestControl_05(View oview)
        {

            if (!Loaded_Page5)
            {
                Button btnSaveHold = oview.FindViewById<Button>(Resource.Id.btnSaveHold);
                Button btnSave = oview.FindViewById<Button>(Resource.Id.btnSave);
                Button btnSubmit = oview.FindViewById<Button>(Resource.Id.btnSubmit);
                Button btnClose = oview.FindViewById<Button>(Resource.Id.btnClose);
                Button btnReset = oview.FindViewById<Button>(Resource.Id.btnReset);

                btnSaveHold.Click += btnSaveHold_Click;
                btnSave.Click += btnSave_Click;
                btnSubmit.Click += btnSubmit_Click;
                btnClose.Click += btnClose_Click;
                btnReset.Click += btnReset_Click;

                Loaded_Page5 = true;
            }
        }

        #region "User Interface Renders"

        // *****************************************************************************************

        public static void cboCustomer_ItemClickListener(View oView,int Pos)
        {
            string oCode = oLckCustomer.Lckitem[Pos].Index;
            //DBLckUpRepository dbr = new DBLckUpRepository();
            mod_SaveRequest.CorpCustID = oCode;

            // Invoke the SR_SiteLocator
            Dialog dialog = new Dialog(xActivity);
            dialog.SetTitle("Site Location:");
            dialog.SetContentView(Resource.Layout.SR_SiteLocator);
            dialog.Show();



            Button btnset = (Button)dialog.FindViewById(Resource.Id.btnSet);
            Button btncancel = (Button)dialog.FindViewById(Resource.Id.btnCancel);
            Spinner cboDialogState = (Spinner)dialog.FindViewById(Resource.Id.cboState);

            //string oState = oLckState.Lckitem[Pos].Index;
            DBLckUpRepository dbr = new DBLckUpRepository();
            oLckState.Lckitem = dbr.StateList();
            cboDialogState.Adapter = oLckState.GetAdapter(oView);

            Spinner cboDialogSite = (Spinner)dialog.FindViewById(Resource.Id.cboSite);
            //Spinner cboDialogState = (Spinner)dialog.FindViewById(Resource.Id.cboState);
            TextView txtTargetSite = (TextView)xActivity.FindViewById(Resource.Id.txtSite);

            string oCode2 = oLckCustomer.Lckitem[Pos].Index;
            string oState2 = "AK";
            oLckSite.Lckitem = dbr.SiteListbyState(oCode2, oState2);
            cboDialogSite.Adapter = oLckSite.GetAdapter(oView);

            int initStatePosition = cboDialogState.SelectedItemPosition;
            cboDialogState.ItemSelected += (sender, args) =>
            {
                if (args.Position != initStatePosition)
                {
                    cboDialogState_ItemClickListener(dialog, (int)(args.Id));
                    //DBLckUpRepository dbr = new DBLckUpRepository();
                    string oCode1 = oLckCustomer.Lckitem[Pos].Index;
                    string oState = oLckState.Lckitem[args.Position].Index;
                    oLckSite.Lckitem = dbr.SiteListbyState(oCode1,oState);
                    cboDialogSite.Adapter = oLckSite.GetAdapter(oView);
                    //txtTargetSite.Text = oLckSite.Lckitem[args.Position].Value;

                    // **************************************************************************************************
                    int initSitePosition = cboDialogSite.SelectedItemPosition;
                    cboDialogSite.ItemSelected += (osender, oargs) =>
                    {
                        if (oargs.Position != initSitePosition)
                        {
                            cboDialogSite_ItemClickListener(dialog, (int)(oargs.Id));
                            mod_SaveRequest.SiteID = oLckSite.Lckitem[oargs.Position].Index;
                            txtTargetSite.Text = oLckSite.Lckitem[oargs.Position].Value;
                        }
                    };

                }
            };



            if (btncancel != null)
            {
                btncancel.Click += (osender, Args) =>
                {
                    Toast.MakeText(xActivity, "Cancel Editor", ToastLength.Short).Show();
                    dialog.Cancel();
                };
            }
            if (btnset != null)
            {
                btnset.Click += (osender, Args) =>
                {
                    //txtTargetNote.Text = cboDialogSite.SelectedItem;
                    //String NoteText = txtNote.Text;
                    Toast.MakeText(xActivity, "Set Editor", ToastLength.Short).Show();
                    dialog.Cancel();
                };
            }



            // Get the data source (Move the Data Source to SR_SiteLocator) Store the oCode Value
            //string oCode = oLckCustomer.Lckitem[Pos].Index;
            ////DBLckUpRepository dbr = new DBLckUpRepository();
            //oLckSite.Lckitem = dbr.SiteList(oCode);
            //mod_SaveRequest.CorpCustID = oCode;


            // Call the SR_SiteLocator
            //Spinner cboSite = oView.FindViewById<Spinner>(Resource.Id.cboSite);
            //cboSite.Adapter = oLckSite.GetAdapter(oView);



            //int initialSitePosition = cboSite.SelectedItemPosition;
            //cboSite.ItemSelected += (sender, args) =>
            //{
            //    if (args.Position != initialSitePosition)
            //    {
            //        cboSite_ItemClickListener(oView, (int)(args.Id));
            //        mod_SaveRequest.SiteID = oLckSite.Lckitem[args.Position].Index;
            //    }
            //};

        }
        // *****************************************************************************************
        public static void cboDialogState_ItemClickListener(Dialog oDialog, int Pos)
        {

        }
        public static void cboDialogSite_ItemClickListener(Dialog oDialog, int Pos)
        {
            SR_SiteAddress vData = new SR_SiteAddress();
            string oSiteID = oLckSite.Lckitem[Pos].Index;
            DBRequestRepository dbr = new DBRequestRepository();
            if (oSiteID.ToUpper().IndexOf("CU-") > -1)
            {
                vData = dbr.GetSiteAddressbyLocation(oSiteID.Trim());
            }
            else
            {
                vData = dbr.GetSiteAddressbySite(oSiteID.Trim());
            }

            TextView mtxtSite = xActivity.FindViewById<TextView>(Resource.Id.txtSite);
            TextView mtxtAddress1 = xActivity.FindViewById<TextView>(Resource.Id.txtAddress1);
            TextView mtxtAddress2 = xActivity.FindViewById<TextView>(Resource.Id.txtAddress2);
            TextView mtxtCity = xActivity.FindViewById<TextView>(Resource.Id.txtCity);
            TextView mtxtState = xActivity.FindViewById<TextView>(Resource.Id.txtState);
            TextView mtxtZip = xActivity.FindViewById<TextView>(Resource.Id.txtZip);
            TextView mtxtContact = xActivity.FindViewById<TextView>(Resource.Id.txtContact);
            TextView mtxtPhone = xActivity.FindViewById<TextView>(Resource.Id.txtPhone);
            TextView mtxtDepartment = xActivity.FindViewById<TextView>(Resource.Id.txtDepartment);

            //string oCode = oLckCustomer.Lckitem[Pos].Index;
            //mod_SaveRequest.CorpCustID = oCode;

            mod_SaveRequest.DeptID = vData.DeptID;
            mod_SaveRequest.Address1 = vData.Address1;
            mod_SaveRequest.Address2 = vData.Address2;
            mod_SaveRequest.City = vData.City;
            mod_SaveRequest.State = vData.State;
            mod_SaveRequest.ZipCode = vData.ZipCode;
            mod_SaveRequest.Contact = vData.Contact;
            mod_SaveRequest.ContactPhone = vData.ContactPhone;
            mod_SaveRequest.ContactDepartment = vData.ContactDepartment;

            mtxtSite.Text = string.Format("{0} - {1}, {2}, {3}", mod_SaveRequest.SiteID, vData.State, vData.City, vData.Address1);
            //mtxtSite.Text = GetSiteCode(mod_SaveRequest.SiteID);
            mtxtAddress1.Text = vData.Address1;
            mtxtAddress2.Text = vData.Address2;
            mtxtCity.Text = vData.City;
            mtxtState.Text = vData.State;
            mtxtZip.Text = vData.ZipCode;
            mtxtContact.Text = vData.Contact;
            mtxtPhone.Text = vData.ContactPhone;
            mtxtDepartment.Text = vData.ContactDepartment;
        }
        // *****************************************************************************************

        public static void cboSite_ItemClickListener(View oView, int Pos)
        {
            SR_SiteAddress vData = new SR_SiteAddress();
            string oSiteID = oLckSite.Lckitem[Pos].Index;
            DBRequestRepository dbr = new DBRequestRepository();
            if (oSiteID.ToUpper().IndexOf("CU-") > -1) {
                 vData = dbr.GetSiteAddressbyLocation(oSiteID.Trim());
            }
            else {
                 vData = dbr.GetSiteAddressbySite(oSiteID.Trim());
            }

            TextView mtxtSite = oView.FindViewById<TextView>(Resource.Id.txtSite);
            TextView mtxtAddress1 = oView.FindViewById<TextView>(Resource.Id.txtAddress1);
            TextView mtxtAddress2 = oView.FindViewById<TextView>(Resource.Id.txtAddress2);
            TextView mtxtCity = oView.FindViewById<TextView>(Resource.Id.txtCity);
            TextView mtxtState = oView.FindViewById<TextView>(Resource.Id.txtState);
            TextView mtxtZip = oView.FindViewById<TextView>(Resource.Id.txtZip);
            TextView mtxtContact = oView.FindViewById<TextView>(Resource.Id.txtContact);
            TextView mtxtPhone = oView.FindViewById<TextView>(Resource.Id.txtPhone);
            TextView mtxtDepartment = oView.FindViewById<TextView>(Resource.Id.txtDepartment);

            mod_SaveRequest.DeptID = vData.DeptID;
            mod_SaveRequest.Address1 = vData.Address1;
            mod_SaveRequest.Address2 = vData.Address2;
            mod_SaveRequest.City = vData.City;
            mod_SaveRequest.State = vData.State;
            mod_SaveRequest.ZipCode = vData.ZipCode;
            mod_SaveRequest.Contact = vData.Contact;
            mod_SaveRequest.ContactPhone = vData.ContactPhone;
            mod_SaveRequest.ContactDepartment = vData.ContactDepartment;

            mtxtSite.Text = string.Format("{0} - {1}, {2}, {3}", mod_SaveRequest.SiteID, vData.State, vData.City, vData.Address1);
            //mtxtSite.Text = GetSiteCode(mod_SaveRequest.SiteID);
            mtxtAddress1.Text = vData.Address1;
            mtxtAddress2.Text = vData.Address2;
            mtxtCity.Text = vData.City;
            mtxtState.Text = vData.State;
            mtxtZip.Text = vData.ZipCode;
            mtxtContact.Text = vData.Contact;
            mtxtPhone.Text = vData.ContactPhone;
            mtxtDepartment.Text = vData.ContactDepartment;

        }
        // *****************************************************************************************
        static void btnSaveHold_Click(object sender, EventArgs e)
        {
            bool IsSaved = true;
            string oMessage = "Cannot reach database. System detected a problem.";

            oMessage = GetValidation();
            if (oMessage.ToUpper() != "SUCCESS")
            {
                IsSaved = false;
                goto exit;
            }

            mod_SaveRequest.RequestStatusID = 18;

            ORM.DBRequestRepository orb = new DBRequestRepository();
            oMessage = orb.SaveHoldRequests(mod_SaveRequest);

            exit:
            Toast.MakeText(xActivity, oMessage, ToastLength.Short).Show();
            if (IsSaved) { xActivity.Finish(); }

            //'==============================================================================================='

        }
        // *****************************************************************************************
        static void btnSave_Click(object sender, EventArgs e)
        {
            bool IsSaved = true;
            string oMessage = "Cannot reach database. System detected a problem.";

            oMessage = GetValidation();
            if (oMessage.ToUpper() != "SUCCESS")
            {
                IsSaved = false;
                goto exit;
            }

            Button btnSave = xActivity.FindViewById<Button>(Resource.Id.btnSave);

            ORM.DBRequestRepository orb = new DBRequestRepository();
            

            if (btnSave.Text == "Final Save")
            {
                btnSave.Text = "Save";
                oMessage = orb.SaveRequestService(mod_SaveRequest,"Hold");
            }
            else
            {
                if (mod_SaveRequest.RequestID>0)
                {
                    oMessage = orb.SaveRequestService(mod_SaveRequest, "Edit");
                } else
                {
                    oMessage = orb.SaveRequestService(mod_SaveRequest, "New");
                }
                
            }

            exit:
            Toast.MakeText(xActivity, oMessage, ToastLength.Short).Show();
            if (IsSaved) { xActivity.Finish(); }
        }
        // *****************************************************************************************
        static void btnSubmit_Click(object sender, EventArgs e)
        {
            bool IsSaved = true;
            string oMessage = "Cannot reach database. System detected a problem.";

            oMessage = GetValidation();
            if (oMessage.ToUpper() != "SUCCESS")
            {
                IsSaved = false;
                goto exit;
            }

            if (mod_SaveRequest == mod_Request)
            {
                mod_SaveRequest.RequestStatusID = 4;
            } else
            {
                mod_SaveRequest.RequestStatusID = 14;
            }
            

            ORM.DBRequestRepository orb = new DBRequestRepository();
            oMessage = orb.SubmitRequest(mod_SaveRequest);

            //oDatabox.Query = oQuery.SaveNewHoldRequest(oRequest)


            exit:
            Toast.MakeText(xActivity, oMessage, ToastLength.Short).Show();
            if (IsSaved) { xActivity.Finish(); }
        }
        // *****************************************************************************************
        static void btnClose_Click(object sender, EventArgs e)
        {
            xActivity.Finish();
        }
        // *****************************************************************************************
        static void btnReset_Click(object sender, EventArgs e)
        {
            string oMessage = "Cannot reach database. System detected a problem.";


            ORM.DBRequestRepository orb = new DBRequestRepository();
            oMessage = orb.ResetRequest(mod_SaveRequest);
            //oDatabox.Query = oQuery.SaveNewHoldRequest(oRequest)

            Toast.MakeText(xActivity, oMessage, ToastLength.Short).Show();
            xActivity.Finish();
        }

        static void PushHoldRequest()
        {
            string oMessage = "An error was detected in the PushHoldRequest. Please contact Administrator.";
            ORM.DBRequestRepository orb = new DBRequestRepository();
            oMessage = orb.MigrateHoldRequests(mod_SaveRequest);

            Toast.MakeText(xActivity, oMessage, ToastLength.Short).Show();
            xActivity.Finish();
        }
        #endregion
        #endregion
        #region "Load Request Control"
        //*************************************************************************************************************
        //  Load Request Control
        //*************************************************************************************************************

        private static void LoadRequestControl_01(View oView)
        {
            if (mod_Request.RequestID !=0 && !Loaded_Page1)
            {
                //Spinner cboCustomer = oView.FindViewById<Spinner>(Resource.Id.cboCustomer);
                //Spinner cboSite = oView.FindViewById<Spinner>(Resource.Id.cboSite);

                TextView mtxtSite = oView.FindViewById<TextView>(Resource.Id.txtSite);
                TextView mtxtAddress1 = oView.FindViewById<TextView>(Resource.Id.txtAddress1);
                TextView mtxtAddress2 = oView.FindViewById<TextView>(Resource.Id.txtAddress2);
                TextView mtxtCity = oView.FindViewById<TextView>(Resource.Id.txtCity);
                TextView mtxtState = oView.FindViewById<TextView>(Resource.Id.txtState);
                TextView mtxtZip = oView.FindViewById<TextView>(Resource.Id.txtZip);
                TextView mtxtContact = oView.FindViewById<TextView>(Resource.Id.txtContact);
                TextView mtxtPhone = oView.FindViewById<TextView>(Resource.Id.txtPhone);
                TextView mtxtDepartment = oView.FindViewById<TextView>(Resource.Id.txtDepartment);

                //int CustPosition = cboCustomer.getpo
                //cboCustomer.SetSelection()

                mtxtSite.Text = string.Format("{0} - {1}, {2}, {3}", mod_SaveRequest.SiteID, mod_SaveRequest.State, mod_SaveRequest.City, mod_SaveRequest.Address1);
                //mtxtSite.Text = GetSiteCode(mod_SaveRequest.SiteID);
                mtxtAddress1.Text = mod_SaveRequest.Address1;
                mtxtAddress2.Text = mod_SaveRequest.Address2;
                mtxtCity.Text = mod_SaveRequest.City;
                mtxtState.Text = mod_SaveRequest.State;
                mtxtZip.Text = mod_SaveRequest.ZipCode;
                mtxtContact.Text = mod_SaveRequest.Contact;
                mtxtPhone.Text = string.Format("{0:(###) ###-####}", mod_SaveRequest.ContactPhone);
                mtxtDepartment.Text = mod_SaveRequest.ContactDepartment;
                Loaded_Page1 = true;
            }
        }
        private static void LoadRequestControl_02(View oView)
        {
            if (mod_Request.RequestID != 0 && !Loaded_Page2)
            {
                //Spinner cboOfficerType = oView.FindViewById<Spinner>(Resource.Id.cboOfficerType);
                Spinner cboReasonServices = oView.FindViewById<Spinner>(Resource.Id.cboReasonServices);
                Button btnReasonServices = oView.FindViewById<Button>(Resource.Id.btnReasonServices);
                TextView txtReasonServices = oView.FindViewById<TextView>(Resource.Id.txtReasonServices);
                Spinner cboLocationType = oView.FindViewById<Spinner>(Resource.Id.cboLocationType);
                Button btnLocationType = oView.FindViewById<Button>(Resource.Id.btnLocationType);
                TextView txtLocationType = oView.FindViewById<TextView>(Resource.Id.txtLocationType);

                EditText txtOfficersRequired = oView.FindViewById<EditText>(Resource.Id.txtOfficersRequired);
                EditText txtAuthorizedAreas = oView.FindViewById<EditText>(Resource.Id.txtAuthorizedAreas);
                EditText txtKeys = oView.FindViewById<EditText>(Resource.Id.txtKeys);
                TextView txtRequestDate = oView.FindViewById<TextView>(Resource.Id.txtRequestDate);
                TextView txtStartDate = oView.FindViewById<TextView>(Resource.Id.txtStartDate);
                TextView txtStartTime = oView.FindViewById<TextView>(Resource.Id.txtStartTime);
                TextView txtEndDate = oView.FindViewById<TextView>(Resource.Id.txtEndDate);
                TextView txtEndTime = oView.FindViewById<TextView>(Resource.Id.txtEndTime);

                CheckBox chkArmed = oView.FindViewById<CheckBox>(Resource.Id.chkArmed);
                CheckBox chkUniform = oView.FindViewById<CheckBox>(Resource.Id.chkUniform);
                CheckBox chkECR = oView.FindViewById<CheckBox>(Resource.Id.chkECR);
                CheckBox chkDMO = oView.FindViewById<CheckBox>(Resource.Id.chkDMO);
                CheckBox chkDMOG = oView.FindViewById<CheckBox>(Resource.Id.chkDMOG);
                //int CustPosition = cboCustomer.getpo
                //cboCustomer.SetSelection()


                if (mod_SaveRequest.OfficerNum == 0) { txtOfficersRequired.Text = ""; } else { txtOfficersRequired.Text = string.Format("{0}", int.Parse(mod_SaveRequest.OfficerNum.ToString())); }
                //txtOfficersRequired.Text = mod_SaveRequest.OfficerNum.ToString();
                txtAuthorizedAreas.Text = mod_SaveRequest.AuthorizedArea;
                txtKeys.Text = mod_SaveRequest.Keys;
                txtRequestDate.Text = mod_SaveRequest.RequestDate.ToString("yyyy-MMM-dd");
                txtStartDate.Text = mod_SaveRequest.StartDate.ToString("yyyy-MMM-dd");
                txtStartTime.Text = GetTime(mod_SaveRequest.StartDate);
                txtEndDate.Text = mod_SaveRequest.EndDate.ToString("yyyy-MMM-dd");
                txtEndTime.Text = GetTime(mod_SaveRequest.EndDate);


                chkArmed.Checked = mod_SaveRequest.ArmGuard;
                chkUniform.Checked = mod_SaveRequest.Uniform;
                chkECR.Checked = mod_SaveRequest.EmergencyConstruction;
                chkDMO.Checked = mod_SaveRequest.DMOPreApproved;
                chkDMOG.Checked = mod_SaveRequest.DMOGranted;

                if (mod_SaveRequest.ReasonServiceID == 99)
                {
                    btnReasonServices.Visibility = ViewStates.Visible;
                    txtReasonServices.Visibility = ViewStates.Visible;
                    cboReasonServices.Visibility = ViewStates.Invisible;

                }
                else
                {
                    btnReasonServices.Visibility = ViewStates.Invisible;
                    txtReasonServices.Visibility = ViewStates.Invisible;
                    cboReasonServices.Visibility = ViewStates.Visible;
                };

                if (mod_SaveRequest.LocationTypeID == 99)
                {
                    btnLocationType.Visibility = ViewStates.Visible;
                    txtLocationType.Visibility = ViewStates.Visible;
                    cboLocationType.Visibility = ViewStates.Invisible;

                }
                else
                {
                    btnLocationType.Visibility = ViewStates.Invisible;
                    txtLocationType.Visibility = ViewStates.Invisible;
                    cboLocationType.Visibility = ViewStates.Visible;
                };

                txtReasonServices.Text = mod_SaveRequest.ReasonService;
                txtLocationType.Text = mod_SaveRequest.LocationType;
                Loaded_Page2 = true;
            }
        }

        private static void LoadRequestControl_03(View oView)
        {
            if (mod_Request.RequestID != 0 && !Loaded_Page3)
            {

                EditText txtRequesterName = oView.FindViewById<EditText>(Resource.Id.txtRequesterName);
                EditText txtRequesterPhone = oView.FindViewById<EditText>(Resource.Id.txtRequesterPhone);
                EditText txtTechnicianName = oView.FindViewById<EditText>(Resource.Id.txtTechnicianName);
                EditText txtTechnicianPhone = oView.FindViewById<EditText>(Resource.Id.txtTechnicianPhone);
                EditText txtGuardName = oView.FindViewById<EditText>(Resource.Id.txtGuardName);
                EditText txtGuardPhone = oView.FindViewById<EditText>(Resource.Id.txtGuardPhone);

                EditText txtPONumber = oView.FindViewById<EditText>(Resource.Id.txtPONumber);
                EditText txtCostCenterCode = oView.FindViewById<EditText>(Resource.Id.txtCostCenterCode);
                EditText txtWorkOrderNumber = oView.FindViewById<EditText>(Resource.Id.txtWorkOrderNumber);


                //int CustPosition = cboCustomer.getpo
                //cboCustomer.SetSelection()


                txtRequesterName.Text = mod_SaveRequest.RequesterName;
                txtRequesterPhone.Text = mod_SaveRequest.RequesterPhone;
                txtTechnicianName.Text = mod_SaveRequest.TechnicianName;
                txtTechnicianPhone.Text = mod_SaveRequest.TechnicianPhone;
                txtGuardName.Text = mod_SaveRequest.GuardName;
                txtGuardPhone.Text = mod_SaveRequest.GuardPhone;

                txtPONumber.Text = mod_SaveRequest.PONumber;
                txtCostCenterCode.Text = mod_SaveRequest.CostCenterCode;
                txtWorkOrderNumber.Text = mod_SaveRequest.WorkOrderNumber;

                Loaded_Page3 = true;
            }
        }

        private static void LoadRequestControl_04(View oView)
        {
            if (mod_Request.RequestID != 0 && !Loaded_Page4)
            {

                TextView txtNote = oView.FindViewById<TextView>(Resource.Id.txtNote);
                TextView dpTerminateDate = oView.FindViewById<TextView>(Resource.Id.dpTerminateDate);
                TextView txtTerminateTime = oView.FindViewById<TextView>(Resource.Id.txtTerminateTime);

                CheckBox chkCancel = oView.FindViewById<CheckBox>(Resource.Id.chkCancel);
                CheckBox chkTerminated = oView.FindViewById<CheckBox>(Resource.Id.chkTerminated);

                //int CustPosition = cboCustomer.getpo
                //cboCustomer.SetSelection()


                txtNote.Text = mod_SaveRequest.Notes;
                dpTerminateDate.Text = mod_SaveRequest.RequestTerminateDate.ToString("yyyy-MMM-dd");
                txtTerminateTime.Text = GetTime(mod_SaveRequest.RequestTerminateDate);

                chkCancel.Checked = mod_SaveRequest.RequestCancel;
                chkTerminated.Checked = mod_SaveRequest.RequestTerminate;

                Loaded_Page4 = true;
            }
        }
        private static void LoadRequestControl_05(View oView)
        {
            Button btnSaveHold = oView.FindViewById<Button>(Resource.Id.btnSaveHold);
            Button btnSave = oView.FindViewById<Button>(Resource.Id.btnSave);
            Button btnSubmit = oView.FindViewById<Button>(Resource.Id.btnSubmit);
            Button btnClose = oView.FindViewById<Button>(Resource.Id.btnClose);
            Button btnReset = oView.FindViewById<Button>(Resource.Id.btnReset);
            if (xSliding.SR_Session.SR_Action == "New")
            {
                btnSave.Text = "Save";

                btnSaveHold.Visibility = ViewStates.Visible;
                btnSave.Visibility = ViewStates.Visible;
                btnSubmit.Visibility = ViewStates.Visible;
                btnClose.Visibility = ViewStates.Visible;
                btnReset.Visibility = ViewStates.Visible;
            }
            else
            {
                if (mod_Request.RequestID < 0)
                {
                    btnSave.Text = "Final Save";

                    btnSaveHold.Visibility = ViewStates.Invisible;
                    btnSave.Visibility = ViewStates.Visible;
                    btnSubmit.Visibility = ViewStates.Invisible;
                    btnClose.Visibility = ViewStates.Visible;
                    btnReset.Visibility = ViewStates.Invisible;
                }
                else
                {
                    btnSave.Text = "Save";

                    btnSaveHold.Visibility = ViewStates.Invisible;
                    btnSave.Visibility = ViewStates.Visible;
                    btnSubmit.Visibility = ViewStates.Visible;
                    btnClose.Visibility = ViewStates.Visible;
                    btnReset.Visibility = ViewStates.Invisible;
                }
            }
            string strStatus = mod_SaveRequest.Status;
            switch (strStatus)
            {
                case "OPEN":
                    //btnSaveHold.Visibility = ViewStates.Visible;
                    btnSave.Visibility = ViewStates.Visible;
                    btnSubmit.Visibility = ViewStates.Visible;
                    btnClose.Visibility = ViewStates.Visible;
                    //btnReset.Visibility = ViewStates.Visible;
                    break;
                //btnUpdate.Enabled = True
                //btnSubmit.Enabled = True
                case "SUBMITTED":
                case "SUBMITTED & UPDATED":
                    //btnSaveHold.Visibility = ViewStates.Visible;
                    btnSave.Visibility = ViewStates.Visible;
                    btnSubmit.Visibility = ViewStates.Invisible;
                    btnClose.Visibility = ViewStates.Visible;
                    //btnReset.Visibility = ViewStates.Visible;
                    break;
                //btnUpdate.Enabled = True
                //btnSubmit.Enabled = False
                case "APPROVED":
                case "APPROVED & UPDATED":
                case "APPROVED & GUARD ASSIGNED":
                case "APPROVED, UPDATED & GUARD ASSIGNED":
                    //btnSaveHold.Visibility = ViewStates.Visible;
                    btnSave.Visibility = ViewStates.Visible;
                    btnSubmit.Visibility = ViewStates.Invisible;
                    btnClose.Visibility = ViewStates.Visible;
                    //btnReset.Visibility = ViewStates.Visible;
                    break;
                //btnUpdate.Enabled = True
                //btnSubmit.Enabled = False
                case "COMPLETE":
                    //btnSaveHold.Visibility = ViewStates.Visible;
                    btnSave.Visibility = ViewStates.Invisible;
                    btnSubmit.Visibility = ViewStates.Invisible;
                    btnClose.Visibility = ViewStates.Visible;
                    //btnReset.Visibility = ViewStates.Visible;
                    break;
                //btnUpdate.Enabled = False
                //btnSubmit.Enabled = False
                case "CANCELLED":
                    //btnSaveHold.Visibility = ViewStates.Visible;
                    btnSave.Visibility = ViewStates.Visible;
                    btnSubmit.Visibility = ViewStates.Invisible;
                    btnClose.Visibility = ViewStates.Visible;
                    //btnReset.Visibility = ViewStates.Visible;
                    break;
                //btnUpdate.Enabled = True
                //btnSubmit.Enabled = False
                case "TERMINATED EARLY":
                    //btnSaveHold.Visibility = ViewStates.Visible;
                    btnSave.Visibility = ViewStates.Invisible;
                    btnSubmit.Visibility = ViewStates.Invisible;
                    btnClose.Visibility = ViewStates.Visible;
                    //btnReset.Visibility = ViewStates.Visible;
                    break;
                //btnUpdate.Enabled = False
                //btnSubmit.Enabled = False
                case "DENIED":
                    //btnSaveHold.Visibility = ViewStates.Visible;
                    btnSave.Visibility = ViewStates.Invisible;
                    btnSubmit.Visibility = ViewStates.Invisible;
                    btnClose.Visibility = ViewStates.Visible;
                    //btnReset.Visibility = ViewStates.Visible;
                    break;
                //btnUpdate.Enabled = False
                //btnSubmit.Enabled = False
                case "CLOSED":
                    //btnSaveHold.Visibility = ViewStates.Visible;
                    btnSave.Visibility = ViewStates.Invisible;
                    btnSubmit.Visibility = ViewStates.Invisible;
                    btnClose.Visibility = ViewStates.Visible;
                    //btnReset.Visibility = ViewStates.Visible;
                    break;
                //btnUpdate.Enabled = False
                //btnSubmit.Enabled = False
            }
        }
        #endregion
        #region "Load Save Module"
        static void LoadSaveRequest(SR_Request oRequest, View oView_00, View oView_01, View oView_02, View oView_03)
        {
            // *** Page 01 ***************************************************************************
            Spinner cboCustomer = oView_00.FindViewById<Spinner>(Resource.Id.cboCustomer);
            Spinner cboSite = oView_00.FindViewById<Spinner>(Resource.Id.cboSite);

            TextView mtxtAddress1 = oView_00.FindViewById<TextView>(Resource.Id.txtAddress1);
            TextView mtxtAddress2 = oView_00.FindViewById<TextView>(Resource.Id.txtAddress2);
            TextView mtxtCity = oView_00.FindViewById<TextView>(Resource.Id.txtCity);
            TextView mtxtState = oView_00.FindViewById<TextView>(Resource.Id.txtState);
            TextView mtxtZip = oView_00.FindViewById<TextView>(Resource.Id.txtZip);
            TextView mtxtContact = oView_00.FindViewById<TextView>(Resource.Id.txtContact);
            TextView mtxtPhone = oView_00.FindViewById<TextView>(Resource.Id.txtPhone);
            TextView mtxtDepartment = oView_00.FindViewById<TextView>(Resource.Id.txtDepartment);

            // *** Page 02 ***************************************************************************
            Spinner cboOfficerType = oView_01.FindViewById<Spinner>(Resource.Id.cboOfficerType);
            Spinner cboReasonServices = oView_01.FindViewById<Spinner>(Resource.Id.cboReasonServices);
            Spinner cboLocationType = oView_01.FindViewById<Spinner>(Resource.Id.cboLocationType);

            EditText txtOfficersRequired = oView_01.FindViewById<EditText>(Resource.Id.txtOfficersRequired);
            EditText txtAuthorizedAreas = oView_01.FindViewById<EditText>(Resource.Id.txtAuthorizedAreas);
            EditText txtKeys = oView_01.FindViewById<EditText>(Resource.Id.txtKeys);
            TextView txtRequestDate = oView_01.FindViewById<TextView>(Resource.Id.txtRequestDate);
            TextView txtStartDate = oView_01.FindViewById<TextView>(Resource.Id.txtStartDate);
            TextView txtStartTime = oView_01.FindViewById<TextView>(Resource.Id.txtStartTime);
            TextView txtEndDate = oView_01.FindViewById<TextView>(Resource.Id.txtEndDate);
            TextView txtEndTime = oView_01.FindViewById<TextView>(Resource.Id.txtEndTime);

            CheckBox chkArmed = oView_01.FindViewById<CheckBox>(Resource.Id.chkArmed);
            CheckBox chkUniform = oView_01.FindViewById<CheckBox>(Resource.Id.chkUniform);
            CheckBox chkECR = oView_01.FindViewById<CheckBox>(Resource.Id.chkECR);
            CheckBox chkDMO = oView_01.FindViewById<CheckBox>(Resource.Id.chkDMO);
            CheckBox chkDMOG = oView_01.FindViewById<CheckBox>(Resource.Id.chkDMOG);

            // *** Page 03 ***************************************************************************
            EditText txtRequesterName = oView_02.FindViewById<EditText>(Resource.Id.txtRequesterName);
            EditText txtRequesterPhone = oView_02.FindViewById<EditText>(Resource.Id.txtRequesterPhone);
            EditText txtTechnicianName = oView_02.FindViewById<EditText>(Resource.Id.txtTechnicianName);
            EditText txtTechnicianPhone = oView_02.FindViewById<EditText>(Resource.Id.txtTechnicianPhone);
            EditText txtGuardName = oView_02.FindViewById<EditText>(Resource.Id.txtGuardName);
            EditText txtGuardPhone = oView_02.FindViewById<EditText>(Resource.Id.txtGuardPhone);

            EditText txtPONumber = oView_02.FindViewById<EditText>(Resource.Id.txtPONumber);
            EditText txtCostCenterCode = oView_02.FindViewById<EditText>(Resource.Id.txtCostCenterCode);
            EditText txtWorkOrderNumber = oView_02.FindViewById<EditText>(Resource.Id.txtWorkOrderNumber);

            // *** Page 04 ***************************************************************************
            TextView txtNote = oView_03.FindViewById<TextView>(Resource.Id.txtNote);
            TextView dpTerminateDate = oView_03.FindViewById<TextView>(Resource.Id.dpTerminateDate);
            TextView txtTerminateTime = oView_03.FindViewById<TextView>(Resource.Id.txtTerminateTime);

            CheckBox chkCancel = oView_03.FindViewById<CheckBox>(Resource.Id.chkCancel);
            CheckBox chkTerminated = oView_03.FindViewById<CheckBox>(Resource.Id.chkTerminated);

            // *** Loading Module ********************************************************************
            // *** Page 01 ***************************************************************************
            //if (cboCustomer != null) { mod_SaveRequest.CorpCustID = cboCustomer.ItemSelected(); };
            //if (cboSite != null) { };

            if (mtxtAddress1 != null) { mod_SaveRequest.Address1 = mtxtAddress1.Text; };
            if (mtxtAddress2 != null) { mod_SaveRequest.Address2 = mtxtAddress2.Text; };
            if (mtxtCity != null) { mod_SaveRequest.City = mtxtCity.Text; };
            if (mtxtState != null) { mod_SaveRequest.State = mtxtState.Text; };
            if (mtxtZip != null) { mod_SaveRequest.ZipCode = mtxtZip.Text; };
            if (mtxtContact != null) { mod_SaveRequest.Contact = mtxtContact.Text; };
            if (mtxtPhone != null) { mod_SaveRequest.ContactPhone = mtxtPhone.Text; };
            if (mtxtDepartment != null) { mod_SaveRequest.ContactDepartment = mtxtDepartment.Text; };

            // *** Page 02 ***************************************************************************
            if (cboOfficerType != null) { };
            if (cboReasonServices != null) { };
            if (cboLocationType != null) { };

            if (txtOfficersRequired.Text == "") { txtOfficersRequired.Text = "0"; };
            if (txtRequestDate.Text == "") { txtRequestDate.Text = DateTime.MinValue.ToString("yyyy-MMM-dd"); };
            if (txtStartDate.Text == "") { txtStartDate.Text = DateTime.MinValue.ToString("yyyy-MMM-dd"); };
            if (txtEndDate.Text == "") { txtEndDate.Text = DateTime.MinValue.ToString("yyyy-MMM-dd"); };

            if (txtOfficersRequired != null) { mod_SaveRequest.OfficerNum = Convert.ToInt32(txtOfficersRequired.Text); };
            if (txtAuthorizedAreas != null) { mod_SaveRequest.AuthorizedArea = txtAuthorizedAreas.Text; };
            if (txtKeys != null) { mod_SaveRequest.Keys = txtKeys.Text; };
            if (txtRequestDate != null) { mod_SaveRequest.RequestDate = Convert.ToDateTime(txtRequestDate.Text); };
            if (txtStartDate != null) { mod_SaveRequest.StartDate = Convert.ToDateTime(txtStartDate.Text + " " + txtStartTime.Text); };
            //if (txtStartDate != null) { mod_SaveRequest.StartDate = Convert.ToDateTime(string.Format("{0} {1}", txtStartDate.Text, txtStartTime.Text)); };
            //if (txtStartTime != null) { mod_SaveRequest.StartDate = txtStartTime.Text; };
            if (txtEndDate != null) { mod_SaveRequest.EndDate = Convert.ToDateTime(string.Format("{0} {1}", txtEndDate.Text, txtEndTime.Text)); };
            //if (txtEndTime != null) { mod_SaveRequest.EndDate = txtEndTime.Text; };

            if (chkArmed != null) { mod_SaveRequest.ArmGuard = chkArmed.Checked; };
            if (chkUniform != null) { mod_SaveRequest.Uniform = chkUniform.Checked; };
            if (chkECR != null) { mod_SaveRequest.EmergencyConstruction = chkECR.Checked; };
            if (chkDMO != null) { mod_SaveRequest.DMOPreApproved = chkDMO.Checked; };
            if (chkDMOG != null) { mod_SaveRequest.DMOGranted = chkDMOG.Checked; };

            // *** Page 03 ***************************************************************************
            if (txtRequesterName != null) { mod_SaveRequest.RequesterName = txtRequesterName.Text; };
            if (txtRequesterPhone != null) { mod_SaveRequest.RequesterPhone = txtRequesterPhone.Text; };
            if (txtTechnicianName != null) { mod_SaveRequest.TechnicianName = txtTechnicianName.Text; };
            if (txtTechnicianPhone != null) { mod_SaveRequest.TechnicianPhone = txtTechnicianPhone.Text; };
            if (txtGuardName != null) { mod_SaveRequest.GuardName = txtGuardName.Text; };
            if (txtGuardPhone != null) { mod_SaveRequest.GuardPhone = txtGuardPhone.Text; };

            if (txtPONumber != null) { mod_SaveRequest.PONumber = txtPONumber.Text; };
            if (txtCostCenterCode != null) { mod_SaveRequest.CostCenterCode = txtCostCenterCode.Text; };
            if (txtWorkOrderNumber != null) { mod_SaveRequest.WorkOrderNumber = txtWorkOrderNumber.Text; };

            // *** Page 04 ***************************************************************************
            if (dpTerminateDate.Text == "") { dpTerminateDate.Text = DateTime.MinValue.ToString("yyyy-MMM-dd"); };

            if (txtNote != null) { mod_SaveRequest.Notes = txtNote.Text; };
            if (dpTerminateDate != null) { mod_SaveRequest.RequestTerminateDate = Convert.ToDateTime(dpTerminateDate.Text); };
            if (txtTerminateTime != null) { mod_SaveRequest.RequestTerminateTime = txtTerminateTime.Text; };

            if (chkCancel != null) { mod_SaveRequest.RequestCancel = chkCancel.Checked; };
            if (chkTerminated != null) { mod_SaveRequest.RequestTerminate = chkTerminated.Checked; };
        }
        private static string GetSiteCode(string oSiteID)
        {
            string Rtn = "";
            foreach (SR_LookUp item in oLckSite.Lckitem)
            {
                if (item.Index == oSiteID)
                {
                    Rtn = item.Value;
                    break;
                }
            }
            return Rtn;
        }
        private static string GetValidation()
        {
            // PAGE 01
            string Rtn = "Success";


            if (mod_SaveRequest.CorpCustID=="") { Rtn = "Please, select a Customer."; goto exit;}
            if (mod_SaveRequest.SiteID == ""){ Rtn = "Please, select a Site."; goto exit;}
            // PAGE 02

            if (mod_SaveRequest.OfficerNum == 0) { Rtn = "Please, indicate how many officers are required."; goto exit; }
            if (mod_SaveRequest.AuthorizedArea == "") { Rtn = "Please, indicate authorized areas."; goto exit; }
            if (mod_SaveRequest.Keys == "") { Rtn = "Please, indicate the keys provided. If there no key indicate 'NONE'"; goto exit; }

            if (mod_SaveRequest.RequestDate.ToString() == "") { Rtn = "Request Date is missed."; goto exit; }
            if (mod_SaveRequest.StartDate.ToString() == "") { Rtn = "Start Date is missed."; goto exit; }
            if (GetTime(mod_SaveRequest.StartDate) == "") { Rtn = "Start Time is missed."; goto exit; }
            if (mod_SaveRequest.EndDate.ToString() == "") { Rtn = "End Date is missed."; goto exit; }
            if (GetTime(mod_SaveRequest.EndDate) == "") { Rtn = "End Time is missed."; goto exit; }

            // PAGE 03
            // PAGE 04
            //TextView txtNote = xActivity.FindViewById<TextView>(Resource.Id.txtNote);
            TextView dpTerminateDate = xActivity.FindViewById<TextView>(Resource.Id.dpTerminateDate);
            TextView txtTerminateTime = xActivity.FindViewById<TextView>(Resource.Id.txtTerminateTime);

            //CheckBox chkCancel = xActivity.FindViewById<CheckBox>(Resource.Id.chkCancel);
            CheckBox chkTerminated = xActivity.FindViewById<CheckBox>(Resource.Id.chkTerminated);

            if (mod_SaveRequest.RequestTerminate)
            {
                if (mod_SaveRequest.RequestTerminateDate.ToString() == "") { Rtn = "Terminated Date is missed."; goto exit; }
                if (mod_SaveRequest.RequestTerminateTime == "") { Rtn = "Terminated Time is missed."; goto exit; }
            }

            exit:
            return Rtn;
        }
        #endregion
    }
}
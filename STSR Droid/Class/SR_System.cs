using System;
using System.Data;
using System.IO;
using SQLite;
//using System.Web.Services;
using SR_Class;
using STSR_Droid;
using STSR_Droid.SQLiteTables;
//using STSR_Droid.SRWSServices;

namespace SR_Class
{
    public class SR_System : SR_Class.SR_DataAccess
    {
        public TblSession GetSession() {
            TblSession Rtn = new TblSession();
            SQLiteConnection DBSystemConnection = CreateSystemDB();
            DBSystemConnection.CreateTable<TblSession>();
            var tbl = DBSystemConnection.Table<TblSession>();
            var db = tbl.FirstOrDefault();
            if (db == null)
            {

                var output = "";
                string oResult = CreateSessionTable();
                if (oResult != "SUCCESS")
                {
                    output += "\n" + oResult;
                    return null;
                }
                oResult = CleanSessionTable();

                if (oResult == "SUCCESS")
                {
                    TblSession rec = new TblSession();
                    rec.SessionID = 0;
                    rec.SR_RoleID = "";
                    rec.SR_UserID = "";
                    rec.SR_UserName = "";
                    rec.SR_UserFullName = "";
                    rec.SR_UserCustAccess = "";
                    rec.SR_CorpCustID = "";
                    rec.SR_Approve = false;
                    rec.SR_Extend = false;
                    rec.SR_CustApprove = false;
                    rec.SR_CustExtend = false;
                    rec.SR_LoggedIn = false;
                    rec.SR_QAS_Mode = false;
                    rec.SR_UserAttempt = 0;
                    rec.SR_EULA = IsEULA;

                    DBSystemConnection.Insert(rec);

                    //// testing performance
                    //var table = odb.Table<SR_Session>();
                    //foreach (var item in table)
                    //{
                    //    var it = item.SR_UserFullName;
                    //}
                    //// end testing performance
                }
                else
                {
                    output += "\n" + oResult;
                    return null;
                }

                db = DBSystemConnection.Table<TblSession>().FirstOrDefault();

            }

            try
            {
                Rtn.SessionID = db.SessionID;
                Rtn.SR_RoleID = db.SR_RoleID;
                Rtn.SR_UserID = db.SR_UserID;
                Rtn.SR_UserName = db.SR_UserName;
                Rtn.SR_UserFullName = db.SR_UserFullName;
                Rtn.SR_UserCustAccess = db.SR_UserCustAccess;
                Rtn.SR_CorpCustID = db.SR_CorpCustID;
                Rtn.SR_Approve = db.SR_Approve;
                Rtn.SR_Extend = db.SR_Extend;
                Rtn.SR_CustApprove = db.SR_CustApprove;
                Rtn.SR_CustExtend = db.SR_CustExtend;
                Rtn.SR_LoggedIn = db.SR_LoggedIn;
                Rtn.SR_QAS_Mode = db.SR_QAS_Mode;
                Rtn.SR_UserAttempt = db.SR_UserAttempt;
                Rtn.SR_EULA = db.SR_EULA;
            }
            catch
            {
                //Rtn.SessionID = 0;
                //Rtn.SR_RoleID = "";
                //Rtn.SR_UserID = "";
                //Rtn.SR_UserName = "";
                //Rtn.SR_UserFullName = "";
                //Rtn.SR_UserCustAccess = "";
                //Rtn.SR_CorpCustID = "";
                //Rtn.SR_Approve = false;
                //Rtn.SR_Extend = false;
                //Rtn.SR_CustApprove = false;
                //Rtn.SR_CustExtend = false;
                //Rtn.SR_LoggedIn = false;
                //Rtn.SR_QAS_Mode = false;
                //Rtn.SR_UserAttempt = 0;
                //Rtn.SR_EULA = false;
            }




            return Rtn;
        }
        public bool SaveSession(TblSession oDb)
        {
            bool Rtn =false;
            SQLiteConnection DBSystemConnection = CreateSystemDB();
            TblSession db = DBSystemConnection.Table<TblSession>().FirstOrDefault();
            db.SessionID = oDb.SessionID;
            db.SR_RoleID = oDb.SR_RoleID;
            db.SR_UserID = oDb.SR_UserID;
            db.SR_UserName = oDb.SR_UserName;
            db.SR_UserFullName = oDb.SR_UserFullName;
            db.SR_UserCustAccess = oDb.SR_UserCustAccess;
            db.SR_CorpCustID = oDb.SR_CorpCustID;
            db.SR_Approve = oDb.SR_Approve;
            db.SR_Extend = oDb.SR_Extend;
            db.SR_CustApprove = oDb.SR_CustApprove;
            db.SR_CustExtend = oDb.SR_CustExtend;
            db.SR_LoggedIn = oDb.SR_LoggedIn;
            db.SR_QAS_Mode = oDb.SR_QAS_Mode;
            db.SR_UserAttempt = oDb.SR_UserAttempt;
            db.SR_EULA = oDb.SR_EULA;
            DBSystemConnection.Update(db);

            return Rtn;
        }
        public void Delete(TblSession oDb)
        {
         
            SQLiteConnection DBSystemConnection = CreateSystemDB();
            TblSession db = DBSystemConnection.Table<TblSession>().FirstOrDefault();

            DBSystemConnection.Delete(db);

            
        }
        

    }
}
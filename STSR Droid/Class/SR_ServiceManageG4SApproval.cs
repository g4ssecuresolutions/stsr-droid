using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Services.Protocols;
using System.Security.Cryptography.X509Certificates;
using SR_Class;
using SRWS_ASMX;
using STSR_Droid.SRWSServices;
using System.Data;



namespace SR_Class
{
    public partial class SR_ServiceManage : ISoapServices
    {
        public List<SRWS_ASMX.SR_RequestItem> SR_SPWorkflow_Select(string oUserID , string oUserCustAccess  )
        {
            SR_DataConversion dc = new SR_DataConversion();
            RequestItems = new List<SRWS_ASMX.SR_RequestItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getrequestitem = oService.SR_SPWorkflow_Select(oUserID, oUserCustAccess);
                foreach (var item in getrequestitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    RequestItems.Add(dc.ASMX_RequestItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return RequestItems;
        }

        public List<SRWS_ASMX.SR_G4SApprove> SR_SPWorkflow_SubmitbyRequestID(string oUserID, string oRequestID)
        {
            SR_DataConversion dc = new SR_DataConversion();
            List<SRWS_ASMX.SR_G4SApprove> ApprovalItems = new List<SRWS_ASMX.SR_G4SApprove>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getrequestitem = oService.SR_SPWorkflow_SubmitbyRequestID(oUserID, oRequestID);
                foreach (var item in getrequestitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    ApprovalItems.Add(dc.ASMX_ApprovalItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return ApprovalItems;
        }


        public List<SRWS_ASMX.SR_CustomerApprove> SR_SPWorkflow_SubmitbyCustRequestID(string oUserID, string oRequestID)
        {
            SR_DataConversion dc = new SR_DataConversion();
            List<SRWS_ASMX.SR_CustomerApprove> ApprovalItems = new List<SRWS_ASMX.SR_CustomerApprove>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                var getrequestitem = oService.SR_SPWorkflow_SubmitbyRequestID(oUserID, oRequestID);
                foreach (var item in getrequestitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    ApprovalItems.Add(dc.ASMX_CustApprovalItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return ApprovalItems;
        }



        //oDataboxDept.Query = oQueryDept.GetListDeptID
        //oDept = oDataboxDept.GetDataTable(SR_Databox.DataSource.QueryString)
        //cboDept.DataSource = oDept.DefaultView
        //cboDept.DataTextField = "Descr"
        //cboDept.DataValueField = "DeptID"

        public List<SR_LookUpItem> SR_GetDepartmentList()
        {
            SR_DataConversion dc = new SR_DataConversion();
            DepartmentItems = new List<SR_LookUpItem>();
            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                SR_LckOffice[] getdepartmentitem = oService.SR_GetListDeptID(true);
                foreach (SR_LckOffice item in getdepartmentitem)
                {
                    //public List<SR_RequestItem> RequestItems { get; private set; }
                    DepartmentItems.Add(dc.ASMX_DepartmentItem_to_item(item));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return DepartmentItems;
        }
        public string SaveG4SApproveRequest(STSR_Droid.Models.SR_Session oSession, STSR_Droid.Models.SR_SaveWorkflowApprove oWorkflow, STSR_Droid.Models.SR_SaveWorkflowApprove oUnchangedWorkflow)
        {
            string Rtn = "";
            SR_DataConversion dc = new SR_DataConversion();

            try
            {
                SSLValidator.OverrideValidation();
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = (se, cert, chain, sslerror) => { return true; };
                SRWSServices oService = new SRWSServices();
                oService.SR_AuthenticationHeaderValue = SetCredentials(oService);

                Rtn =oService.SR_btnSaveG4SApprove(oSession.ToSRWSServices(), oWorkflow.ToSRWSServices(), oUnchangedWorkflow.ToSRWSServices());

            }
            catch (Exception ex)
            {
                Rtn = "Services had failed.";
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            return Rtn;
        }

    }
}
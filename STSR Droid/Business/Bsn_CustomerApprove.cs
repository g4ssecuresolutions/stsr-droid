using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Database.Sqlite;

using STSR_Droid.Models;
using SR_Class;
using SQLite;
using STSR_Droid.SQLiteTables;

namespace STSR_Droid.Business
{
    public class Bsn_CustomerApprove
    {
        public List<SR_CustomerApprove> LoadModelFromService(List<SRWS_ASMX.SR_RequestItem> oRec)
        {
            List<SR_CustomerApprove> Rtn = new List<SR_CustomerApprove>();

            foreach (var item in oRec)
            {
                SR_CustomerApprove rec = new SR_CustomerApprove();
                rec.SR_RequestID = item.RequestID;
                rec.CorpCustID = item.CorpCustID;
                rec.Address1 = item.Address1;
                rec.City = item.City;
                rec.State = item.State;
                rec.Contact = item.Contact;

                rec.OfficerType = item.OfficerType;
                rec.OfficerNum = item.OfficerNum;
                rec.StartDate = item.StartDate;
                rec.EndDate = item.EndDate;
                rec.Notes = item.Notes;
                rec.RequestRate = (Double)item.RequestRate;
                rec.AddlCharges = item.AddlCharges;
                rec.AppdCharges = item.AppdCharges;
                rec.DeptID = item.DeptID;
                rec.RequestStatusID = item.RequestStatusID;
                rec.Status = item.Status;

                Rtn.Add(rec);
            }

            return Rtn;
        }
        public string LoadTable(List<SR_CustomerApprove> oRec)
        {
            string oResult = string.Empty;
            try
            {
                SR_DataAccess oDB = new SR_DataAccess();
                SQLiteConnection DBSystemConnection = oDB.CreateBackupDB();
                oResult = SR_DataAccess.CreateTable<TblCustomerRequest>();

                oResult = SR_DataAccess.CleanTable<TblCustomerRequest>("TblCustomerRequest");

                TableQuery<TblCustomerRequest> db = DBSystemConnection.Table<TblCustomerRequest>();

                foreach (var item in oRec)
                {
                    TblCustomerRequest rec = new TblCustomerRequest();
                    rec.CorpCustID = item.CorpCustID;
                    rec.Address1 = item.Address1;
                    rec.City = item.City;
                    rec.State = item.State;
                    rec.Contact = item.Contact;

                    rec.OfficerType = item.OfficerType;
                    rec.OfficerNum = item.OfficerNum;
                    rec.StartDate = item.StartDate;
                    rec.EndDate = item.EndDate;
                    rec.Notes = item.Notes;
                    rec.RequestRate = (Double)item.RequestRate;
                    rec.DeptID = item.DeptID;
                    rec.RequestStatusID = item.RequestStatusID;
                    rec.Status = item.Status;
                    DBSystemConnection.Insert(rec);
                }


            }
            catch (Exception e)
            {
                oResult = e.Message;
            }

            return oResult;
        }

        public List<SR_CustomerApprove> LoadModelFromBackup()
        {
            List<SR_CustomerApprove> Rtn = new List<SR_CustomerApprove>();
            SR_DataAccess oDB = new SR_DataAccess();
            SQLiteConnection DBSystemConnection = oDB.CreateBackupDB();
            TableQuery<TblCustomerRequest> db = DBSystemConnection.Table<TblCustomerRequest>();

            foreach (TblCustomerRequest item in db)
            {
                SR_CustomerApprove rec = new SR_CustomerApprove();
                rec.CorpCustID = item.CorpCustID;
                rec.Address1 = item.Address1;
                rec.City = item.City;
                rec.State = item.State;
                rec.Contact = item.Contact;

                rec.OfficerType = item.OfficerType;
                rec.OfficerNum = item.OfficerNum;
                rec.StartDate = item.StartDate;
                rec.EndDate = item.EndDate;
                rec.Notes = item.Notes;
                rec.RequestRate = (Double)item.RequestRate;
                rec.AddlCharges = item.AddlCharges;
                rec.AppdCharges = item.AppdCharges;
                rec.DeptID = item.DeptID;
                rec.RequestStatusID = item.RequestStatusID;
                rec.Status = item.Status;
                Rtn.Add(rec);
            }

            return Rtn;
        }
        public List<SR_CustomerApprove> LoadModelFromItemService(List<SRWS_ASMX.SR_G4SApprove> oRec)
        {
            List<SR_CustomerApprove> Rtn = new List<SR_CustomerApprove>();

            foreach (var item in oRec)
            {
                SR_CustomerApprove rec = new SR_CustomerApprove();
                rec.CorpCustID = item.CorpCustID;
                rec.Address1 = item.Address1;
                rec.City = item.City;
                rec.State = item.State;
                rec.Contact = item.Contact;

                rec.OfficerType = item.OfficerType;
                //rec.OfficerNum = item.OfficerNum;
                //rec.StartDate = item.StartDate;
                //rec.EndDate = item.EndDate;
                rec.Notes = item.Notes;
                rec.RequestRate = (Double)item.RequestRate;
                rec.AddlCharges = item.AddlCharges;
                rec.AppdCharges = item.AppdCharges;
                rec.DeptID = item.DeptID;
                rec.RequestStatusID = item.RequestStatusID;
                rec.Status = item.Status;

                Rtn.Add(rec);
            }

            return Rtn;
        }
    }
}
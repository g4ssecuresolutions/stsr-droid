using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Database.Sqlite;

using STSR_Droid.Models;
using SR_Class;
using SQLite;
using STSR_Droid.SQLiteTables;

namespace STSR_Droid.Business
{
    public class Bsn_G4SApprove
    {

        public List<SR_G4SRequest> LoadModelFromService(List<SRWS_ASMX.SR_RequestItem> oRec)
        {
            List<SR_G4SRequest> Rtn = new List<SR_G4SRequest>();

            foreach (var item in oRec)
            {
                SR_G4SRequest rec = new SR_G4SRequest();

                //SR_RequestID DeptID  CorpCustID CustNotes   CustName DisplayName Address1 City    State Contact OfficerType 
                //OfficerNum  StartDate EndDate StartDate2 EndDate2    
                //Status RequestStatusID Notes RequestRate 
                //AddlCharges AppdCharges DeniedCharges ShowApprove CtrlEnabled 
                //EnableCtrlDenied    EnableCtrlAppd EnableCtrlAddl  EnableCtrlApprove 
                //TotalAdditionalAmount   Code Approved    Denied Questioned  Approvedby Deniedby    
                //Questionedby Description Amount SR_AdditionalChargebyRequestID


                rec.SR_RequestID = item.RequestID;
                rec.DeptID = item.DeptID;
                //rec.CorpCustID = item.CorpCustID;
                //rec.CustNotes = item.CustNotes;
                rec.CustName = item.CustName;
                //rec.DisplayName = item.DisplayName;
                //rec.Address1 = item.Address1;
                rec.City = item.City;
                rec.State = item.State;
                rec.Contact = item.Contact;
                rec.OfficerType = item.OfficerType;
                rec.OfficerNum = item.OfficerNum;
                rec.StartDate = item.StartDate;
                rec.EndDate = item.EndDate;
                rec.SiteName = item.SiteName;
                rec.Status = item.Status;
                //rec.RequestStatusID = item.RequestStatusID;
                //rec.Notes = item.Notes;
                //rec.RequestRate = item.RequestRate;
                //rec.AddlCharges = item.AddlCharges;
                //rec.AppdCharges = item.AppdCharges;
                //rec.DeniedCharges = item.DeniedCharges;
                //rec.ShowApprove = item.ShowApprove;
                //rec.CtrlEnabled = item.CtrlEnabled;
                //rec.EnableCtrlDenied = item.EnableCtrlDenied;
                //rec.EnableCtrlAppd = item.EnableCtrlAppd;
                //rec.EnableCtrlAddl = item.EnableCtrlAddl;
                //rec.EnableCtrlApprove = item.EnableCtrlApprove;

                Rtn.Add(rec);
            }

            return Rtn;
        }
        public string LoadTable(List<SR_G4SRequest> oRec)
        {
            string oResult = string.Empty;
            try
            {
                SR_DataAccess oDB = new SR_DataAccess();
                SQLiteConnection DBSystemConnection = oDB.CreateBackupDB();
                oResult = SR_DataAccess.CreateTable<TblG4SRequest>();

                oResult = SR_DataAccess.CleanTable<TblG4SRequest>("TblG4SRequest");

                TableQuery<TblG4SRequest> db = DBSystemConnection.Table<TblG4SRequest>();

                foreach (var item in oRec)
                {
                    TblG4SRequest rec = new TblG4SRequest();
                    rec.SiteName = item.SiteName;
                    rec.City = item.City;
                    rec.State = item.State;
                    rec.Contact = item.Contact;

                    rec.OfficerType = item.OfficerType;
                    rec.OfficerNum = item.OfficerNum;
                    rec.StartDate = item.StartDate;
                    rec.EndDate = item.EndDate;
                    rec.Notes = item.Notes;
                    rec.DeptID = item.DeptID;
                    rec.Status = item.Status;
                    DBSystemConnection.Insert(rec);
                }


            }
            catch (Exception e)
            {
                oResult = e.Message;
            }

            return oResult;
        }

        public List<SR_G4SRequest> LoadModelFromBackup()
        {
            List<SR_G4SRequest> Rtn = new List<SR_G4SRequest>();
            SR_DataAccess oDB = new SR_DataAccess();
            SQLiteConnection DBSystemConnection = oDB.CreateBackupDB();
            TableQuery<TblG4SRequest> db = DBSystemConnection.Table<TblG4SRequest>();

            foreach (var item in db)
            {
                SR_G4SRequest rec = new SR_G4SRequest();
                rec.SiteName = item.SiteName;
                rec.City = item.City;
                rec.State = item.State;
                rec.Contact = item.Contact;

                rec.OfficerType = item.OfficerType;
                rec.OfficerNum = item.OfficerNum;
                rec.StartDate = item.StartDate;
                rec.EndDate = item.EndDate;
                rec.Notes = item.Notes;
                rec.DeptID = item.DeptID;
                rec.Status = item.Status;
                Rtn.Add(rec);
            }

            return Rtn;
        }
        public List<SR_G4SRequest> LoadModelFromItemService(List<SRWS_ASMX.SR_G4SApprove> oRec)
        {
            List<SR_G4SRequest> Rtn = new List<SR_G4SRequest>();

            foreach (var item in oRec)
            {
                SR_G4SRequest rec = new SR_G4SRequest();

                //SR_RequestID DeptID  CorpCustID CustNotes   CustName DisplayName Address1 City    State Contact OfficerType 
                //OfficerNum  StartDate EndDate StartDate2 EndDate2    
                //Status RequestStatusID Notes RequestRate 
                //AddlCharges AppdCharges DeniedCharges ShowApprove CtrlEnabled 
                //EnableCtrlDenied    EnableCtrlAppd EnableCtrlAddl  EnableCtrlApprove 
                //TotalAdditionalAmount   Code Approved    Denied Questioned  Approvedby Deniedby    
                //Questionedby Description Amount SR_AdditionalChargebyRequestID


                //rec.RequestID = item.SR_RequestID;
                rec.DeptID = item.DeptID;
                //rec.CorpCustID = item.CorpCustID;
                //rec.CustNotes = item.CustNotes;
                rec.CustName = item.CustName;
                //rec.DisplayName = item.DisplayName;
                //rec.Address1 = item.Address1;
                rec.City = item.City;
                rec.State = item.State;
                rec.Contact = item.Contact;
                rec.OfficerType = item.OfficerType;
                //rec.OfficerNum = item.OfficerNum;
                //rec.StartDate = item.StartDate;
                //rec.EndDate = item.EndDate;
                //rec.SiteName = item.SiteName;
                rec.Status = item.Status;
                //rec.RequestStatusID = item.RequestStatusID;
                //rec.Notes = item.Notes;
                //rec.RequestRate = item.RequestRate;
                //rec.AddlCharges = item.AddlCharges;
                //rec.AppdCharges = item.AppdCharges;
                //rec.DeniedCharges = item.DeniedCharges;
                //rec.ShowApprove = item.ShowApprove;
                //rec.CtrlEnabled = item.CtrlEnabled;
                //rec.EnableCtrlDenied = item.EnableCtrlDenied;
                //rec.EnableCtrlAppd = item.EnableCtrlAppd;
                //rec.EnableCtrlAddl = item.EnableCtrlAddl;
                //rec.EnableCtrlApprove = item.EnableCtrlApprove;

                Rtn.Add(rec);
            }

            return Rtn;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Database.Sqlite;
using Android.App;
using Android.Widget;

using STSR_Droid.Models;
using SR_Class;
using SQLite;
using STSR_Droid.SQLiteTables;

namespace STSR_Droid.Business
{
    public class Bsn_Request
    {

        //List<SRWS_ASMX.SR_RequestItem>
        public List<SR_Request> LoadModelFromService(SRWSServices.SR_Request[] oRec)
        {
            List<SR_Request> Rtn = new List<SR_Request>();

            foreach (var item in oRec)
            {
                SR_Request rec = new SR_Request();
                rec.RequestID = item.RequestID;
                rec.SiteID = item.SiteID;
                rec.CorpCustID = item.CorpCustID;
                rec.ManualEntry = item.ManualEntry;
                rec.SiteName = item.SiteName;
                rec.Address1 = item.Address1;
                rec.Address2 = item.Address2;
                rec.City = item.City;
                rec.State = item.State;
                rec.ZipCode = item.ZipCode;
                rec.Latitude = (Double)item.Latitude;
                rec.Longitude = (Double)item.Longitude;
                rec.Contact = item.Contact;
                rec.ContactPhone = item.ContactPhone;

                rec.RequesterName = item.RequesterName;
                rec.RequesterPhone = item.RequesterPhone;
                rec.TechnicianName = item.TechnicianName;
                rec.TechnicianPhone = item.TechnicianPhone;
                rec.GuardName = item.GuardName;
                rec.GuardPhone = item.GuardPhone;

                rec.OfficerType = item.OfficerType;
                rec.OfficerNum = item.OfficerNum;
                rec.StartDate = item.StartDate;
                rec.EndDate = item.EndDate;
                rec.Notes = item.Notes;
                rec.RequestRate = (Double)item.RequestRate;
                rec.RequestCancel = item.RequestCancel;
                rec.RequestTerminate = item.RequestTerminate;
                rec.RequestTerminateDate = item.RequestTerminateDate;
                rec.RequestTerminateTime = item.RequestTerminateTime;
                rec.UpdatedBy = item.UpdatedBy;
                rec.UpdatedDTTM = item.UpdatedDTTM;
                rec.RequestedBy = item.RequestedBy;
                rec.RequestedDTTM = item.RequestedDTTM;
                rec.AddlCharges = item.AddlCharges;
                rec.AppdCharges = item.AppdCharges;
                rec.DeptID = item.DeptID;
                rec.AddlChargesDenied = item.AddlChargesDenied;
                rec.AddlChargesDeniedBy = item.AddlChargesDeniedBy;
                rec.RequestStatusID = item.RequestStatusID;
                rec.ArmGuard = item.ArmGuard;
                rec.ReasonService = item.ReasonService;
                rec.Uniform = item.Uniform;
                rec.Keys = item.Keys;
                rec.AuthorizedArea = item.AuthorizedArea;
                rec.EmergencyConstruction = item.EmergencyConstruction;
                rec.DMOPreApproved = item.DMOPreApproved;
                rec.DMOGranted = item.DMOGranted;
                rec.RequestDate = item.RequestDate;
                rec.ContactDepartment = item.ContactDepartment;
                rec.LocationType = item.LocationType;
                rec.ReasonServiceID = item.ReasonServiceID;
                rec.LocationTypeID = item.LocationTypeID;
                rec.Urgent = item.Urgent;
                rec.SaveNote = (item.SaveNote == "true") ? true : false;
                rec.ExtensionDate = item.ExtensionDate;
                rec.ExtensionNote = item.ExtensionNote;
                rec.TotalAdditionalAmount = (Double)item.TotalAdditionalAmount;
                rec.CustomerSiteEntry = item.CustomerSiteEntry;
                rec.Status = item.Status;

                Rtn.Add(rec);
            }

            return Rtn;
        }
public List<SR_Request> LoadModelFromService(List<SRWS_ASMX.SR_RequestItem> oRec)
        {
            List<SR_Request> Rtn = new List<SR_Request>();

            foreach (var item in oRec)
            {
                SR_Request rec = new SR_Request();
                rec.RequestID = item.RequestID;
                rec.SiteID = item.SiteID;
                rec.CorpCustID = item.CorpCustID;
                rec.ManualEntry = item.ManualEntry;
                rec.SiteName = item.SiteName;
                rec.Address1 = item.Address1;
                rec.Address2 = item.Address2;
                rec.City = item.City;
                rec.State = item.State;
                rec.ZipCode = item.ZipCode;
                rec.Latitude = (Double)item.Latitude;
                rec.Longitude = (Double)item.Longitude;
                rec.Contact = item.Contact;
                rec.ContactPhone = item.ContactPhone;

                rec.RequesterName = item.RequesterName;
                rec.RequesterPhone = item.RequesterPhone;
                rec.TechnicianName = item.TechnicianName;
                rec.TechnicianPhone = item.TechnicianPhone;
                rec.GuardName = item.GuardName;
                rec.GuardPhone = item.GuardPhone;

                rec.OfficerType = item.OfficerType;
                rec.OfficerNum = item.OfficerNum;
                rec.StartDate = item.StartDate;
                rec.EndDate = item.EndDate;
                rec.Notes = item.Notes;
                rec.RequestRate = (Double)item.RequestRate;
                rec.RequestCancel = item.RequestCancel;
                rec.RequestTerminate = item.RequestTerminate;
                rec.RequestTerminateDate = item.RequestTerminateDate;
                rec.RequestTerminateTime = item.RequestTerminateTime;
                rec.UpdatedBy = item.UpdatedBy;
                rec.UpdatedDTTM = item.UpdatedDTTM;
                rec.RequestedBy = item.RequestedBy;
                rec.RequestedDTTM = item.RequestedDTTM;
                rec.AddlCharges = item.AddlCharges;
                rec.AppdCharges = item.AppdCharges;
                rec.DeptID = item.DeptID;
                rec.AddlChargesDenied = item.AddlChargesDenied;
                rec.AddlChargesDeniedBy = item.AddlChargesDeniedBy;
                rec.RequestStatusID = item.RequestStatusID;
                rec.ArmGuard = item.ArmGuard;
                rec.ReasonService = item.ReasonService;
                rec.Uniform = item.Uniform;
                rec.Keys = item.Keys;
                rec.AuthorizedArea = item.AuthorizedArea;
                rec.EmergencyConstruction = item.EmergencyConstruction;
                rec.DMOPreApproved = item.DMOPreApproved;
                rec.DMOGranted = item.DMOGranted;
                rec.RequestDate = item.RequestDate;
                rec.ContactDepartment = item.ContactDepartment;
                rec.LocationType = item.LocationType;
                rec.ReasonServiceID = item.ReasonServiceID;
                rec.LocationTypeID = item.LocationTypeID;
                rec.Urgent = item.Urgent;
                rec.SaveNote = item.SaveNote;
                rec.ExtensionDate = item.ExtensionDate;
                rec.ExtensionNote = item.ExtensionNote;
                rec.TotalAdditionalAmount = (Double)item.TotalAdditionalAmount;
                rec.CustomerSiteEntry = item.CustomerSiteEntry;
                rec.Status = item.Status;
                rec.CodeName = item.CodeName;

                rec.PONumber = item.PONumber;
                rec.CostCenterCode = item.CostCenterCode;
                rec.WorkOrderNumber = item.WorkOrderNumber;

                Rtn.Add(rec);
            }

            return Rtn;
        }
        public string LoadTable(List<SR_Request> oRec)
        {
            string oResult = string.Empty;
            try {
                SR_DataAccess oDB = new SR_DataAccess();
                SQLiteConnection DBSystemConnection = oDB.CreateBackupDB();
                oResult = SR_DataAccess.CreateTable<TblRequest>();

                oResult = SR_DataAccess.CleanTable<TblRequest>("TblRequest");

                TableQuery<TblRequest> db = DBSystemConnection.Table<TblRequest>();

                foreach (var item in oRec)
                {
                    TblRequest rec = new TblRequest();
                    rec.RequestID = item.RequestID;
                    rec.SiteID = item.SiteID;
                    rec.CorpCustID = item.CorpCustID;
                    rec.ManualEntry = item.ManualEntry;
                    rec.SiteName = item.SiteName;
                    rec.Address1 = item.Address1;
                    rec.Address2 = item.Address2;
                    rec.City = item.City;
                    rec.State = item.State;
                    rec.ZipCode = item.ZipCode;
                    rec.Latitude = (Double)item.Latitude;
                    rec.Longitude = (Double)item.Longitude;
                    rec.Contact = item.Contact;
                    rec.ContactPhone = item.ContactPhone;

                    rec.RequesterName = item.RequesterName;
                    rec.RequesterPhone = item.RequesterPhone;
                    rec.TechnicianName = item.TechnicianName;
                    rec.TechnicianPhone = item.TechnicianPhone;
                    rec.GuardName = item.GuardName;
                    rec.GuardPhone = item.GuardPhone;

                    rec.OfficerType = item.OfficerType;
                    rec.OfficerNum = item.OfficerNum;
                    rec.StartDate = item.StartDate;
                    rec.EndDate = item.EndDate;
                    rec.Notes = item.Notes;
                    rec.RequestRate = (Double)item.RequestRate;
                    rec.RequestCancel = item.RequestCancel;
                    rec.RequestTerminate = item.RequestTerminate;
                    rec.RequestTerminateDate = item.RequestTerminateDate;
                    rec.RequestTerminateTime = item.RequestTerminateTime;
                    rec.UpdatedBy = item.UpdatedBy;
                    rec.UpdatedDTTM = item.UpdatedDTTM;
                    rec.RequestedBy = item.RequestedBy;
                    rec.RequestedDTTM = item.RequestedDTTM;
                    rec.AddlCharges = item.AddlCharges;
                    rec.AppdCharges = item.AppdCharges;
                    rec.DeptID = item.DeptID;
                    rec.AddlChargesDenied = item.AddlChargesDenied;
                    rec.AddlChargesDeniedBy = item.AddlChargesDeniedBy;
                    rec.RequestStatusID = item.RequestStatusID;
                    rec.ArmGuard = item.ArmGuard;
                    rec.ReasonService = item.ReasonService;
                    rec.Uniform = item.Uniform;
                    rec.Keys = item.Keys;
                    rec.AuthorizedArea = item.AuthorizedArea;
                    rec.EmergencyConstruction = item.EmergencyConstruction;
                    rec.DMOPreApproved = item.DMOPreApproved;
                    rec.DMOGranted = item.DMOGranted;
                    rec.RequestDate = item.RequestDate;
                    rec.ContactDepartment = item.ContactDepartment;
                    rec.LocationType = item.LocationType;
                    rec.ReasonServiceID = item.ReasonServiceID;
                    rec.LocationTypeID = item.LocationTypeID;
                    rec.Urgent = item.Urgent;
                    rec.SaveNote = item.SaveNote;
                    rec.ExtensionDate = item.ExtensionDate;
                    rec.ExtensionNote = item.ExtensionNote;
                    rec.TotalAdditionalAmount = (Double)item.TotalAdditionalAmount;
                    rec.CustomerSiteEntry = item.CustomerSiteEntry;
                    rec.Status = item.Status;
                    DBSystemConnection.Insert(rec);
                }


            }
            catch( Exception e) {
                oResult = e.Message;
            }

            return oResult;
        }

        public List<SR_Request> LoadModelFromBackup() {
            List<SR_Request> Rtn = new List<SR_Request>();
            SR_DataAccess oDB = new SR_DataAccess();
            SQLiteConnection DBSystemConnection = oDB.CreateBackupDB();
            TableQuery<TblRequest> db = DBSystemConnection.Table<TblRequest>();

            foreach (var item in db)
            {
                SR_Request rec = new SR_Request();
                rec.RequestID = item.RequestID;
                rec.SiteID = item.SiteID;
                rec.CorpCustID = item.CorpCustID;
                rec.ManualEntry = item.ManualEntry;
                rec.SiteName = item.SiteName;
                rec.Address1 = item.Address1;
                rec.Address2 = item.Address2;
                rec.City = item.City;
                rec.State = item.State;
                rec.ZipCode = item.ZipCode;
                rec.Latitude = (Double)item.Latitude;
                rec.Longitude = (Double)item.Longitude;
                rec.Contact = item.Contact;
                rec.ContactPhone = item.ContactPhone;

                rec.RequesterName = item.RequesterName;
                rec.RequesterPhone = item.RequesterPhone;
                rec.TechnicianName = item.TechnicianName;
                rec.TechnicianPhone = item.TechnicianPhone;
                rec.GuardName = item.GuardName;
                rec.GuardPhone = item.GuardPhone;

                rec.OfficerType = item.OfficerType;
                rec.OfficerNum = item.OfficerNum;
                rec.StartDate = item.StartDate;
                rec.EndDate = item.EndDate;
                rec.Notes = item.Notes;
                rec.RequestRate = (Double)item.RequestRate;
                rec.RequestCancel = item.RequestCancel;
                rec.RequestTerminate = item.RequestTerminate;
                rec.RequestTerminateDate = item.RequestTerminateDate;
                rec.RequestTerminateTime = item.RequestTerminateTime;
                rec.UpdatedBy = item.UpdatedBy;
                rec.UpdatedDTTM = item.UpdatedDTTM;
                rec.RequestedBy = item.RequestedBy;
                rec.RequestedDTTM = item.RequestedDTTM;
                rec.AddlCharges = item.AddlCharges;
                rec.AppdCharges = item.AppdCharges;
                rec.DeptID = item.DeptID;
                rec.AddlChargesDenied = item.AddlChargesDenied;
                rec.AddlChargesDeniedBy = item.AddlChargesDeniedBy;
                rec.RequestStatusID = item.RequestStatusID;
                rec.ArmGuard = item.ArmGuard;
                rec.ReasonService = item.ReasonService;
                rec.Uniform = item.Uniform;
                rec.Keys = item.Keys;
                rec.AuthorizedArea = item.AuthorizedArea;
                rec.EmergencyConstruction = item.EmergencyConstruction;
                rec.DMOPreApproved = item.DMOPreApproved;
                rec.DMOGranted = item.DMOGranted;
                rec.RequestDate = item.RequestDate;
                rec.ContactDepartment = item.ContactDepartment;
                rec.LocationType = item.LocationType;
                rec.ReasonServiceID = item.ReasonServiceID;
                rec.LocationTypeID = item.LocationTypeID;
                rec.Urgent = item.Urgent;
                rec.SaveNote = item.SaveNote;
                rec.ExtensionDate = item.ExtensionDate;
                rec.ExtensionNote = item.ExtensionNote;
                rec.TotalAdditionalAmount = (Double)item.TotalAdditionalAmount;
                rec.CustomerSiteEntry = item.CustomerSiteEntry;
                rec.Status = item.Status;
                Rtn.Add(rec);
            }

            return Rtn;
        }
        public SR_SiteAddress LoadAddressSiteFromService(SRWS_ASMX.SR_RequestItem oRec)
        {
            SR_SiteAddress Rtn = new SR_SiteAddress();

            Rtn.DeptID = oRec.DeptID;
            Rtn.Address1 = oRec.Address1;
            Rtn.Address2 = oRec.Address2;
            Rtn.City = oRec.City;
            Rtn.State = oRec.State;
            Rtn.ZipCode = oRec.ZipCode;
            Rtn.Latitude = oRec.Latitude.ToString();
            Rtn.Longitude = oRec.Longitude.ToString();
            Rtn.Contact = oRec.Contact;
            Rtn.ContactPhone = oRec.ContactPhone;

            return Rtn;
        }
        public int GetHoldRequestID()
        {
            int Rtn = 0;

            ORM.DBRequestRepository orb = new ORM.DBRequestRepository();
            Rtn = orb.GetHoldRequestID();

            return Rtn;
        }
        public SR_Status GetCurrentStatus(Activity oActivity, SR_Request oRequest )
        {
            SR_Status Rtn = new SR_Status();
            int intOrigStatusID = oRequest.RequestStatusID;
            CheckBox chkCancel = oActivity.FindViewById<CheckBox>(Resource.Id.chkCancel);
            CheckBox chkTerminated = oActivity.FindViewById<CheckBox>(Resource.Id.chkTerminated);
            TextView tbGuardName = oActivity.FindViewById<TextView>(Resource.Id.txtGuardName);

            if (chkCancel.Checked)
            {
                Rtn.StatusID = 0;
                Rtn.StatusName = "Cancelled";
                goto Over;
            }
            if (chkTerminated.Checked)
            {
                Rtn.StatusID = 10;
                Rtn.StatusName = "Terminated Early";
                goto Over;
            }
            if (oRequest.Status.ToUpper() == "CANCELLED")
            {
                Rtn.StatusID = 2;
                Rtn.StatusName = "Open";
                goto Over;
            }
            if (intOrigStatusID == 1)
            {
                Rtn.StatusID = 2;
                Rtn.StatusName = "Open";
                goto Over;
            }
            if (intOrigStatusID == 4 || intOrigStatusID == 14)
            {
                Rtn.StatusID = 14;
                Rtn.StatusName = "Submitted and Updated";
                goto Over;
            }
            if ((intOrigStatusID == 9 || intOrigStatusID == 15 || intOrigStatusID == 16 || intOrigStatusID == 17) && tbGuardName.Text == String.Empty)
            {
                Rtn.StatusID = 15;
                Rtn.StatusName = "Approved and Updated";
                goto Over;
            }
            if ((intOrigStatusID == 9 || intOrigStatusID == 15 || intOrigStatusID == 16 || intOrigStatusID == 17) && tbGuardName.Text != String.Empty)
            {
                Rtn.StatusID = 17;
                Rtn.StatusName = "Approved, Updated and Guard Assigned";
                goto Over;
            }
            Over:
            return Rtn;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using STSR_Droid.ORM;

namespace STSR_Droid
{
    public enum MasterBehavior
    {
        Default,
        Popover,
        Split,
        SplitOnLandscape,
        SplitOnPortrait
    }

    [Activity(Label = "MainActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.SR_Login);

            // Create your application here
        }
    }
}
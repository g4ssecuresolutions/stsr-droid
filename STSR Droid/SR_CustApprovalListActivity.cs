using System;
using System.Collections;
using Android.Text;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Android.OS;
using STSR_Droid.Views;
using STSR_Droid.Models;
using STSR_Droid.ORM;
using STSR_Droid.Class;
using Android.Graphics;
using Android.Content.PM;
using System.Collections.Generic;

namespace STSR_Droid
{
    [Activity(Label = "SR_CustApprovalListActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SR_CustApprovalListActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            try
            {
                base.OnCreate(bundle);
                SetContentView(Resource.Layout.SR_CustApprovalList);
                View DefaultView = FindViewById<View>(Resource.Id.RequestView);
                //Button btnRequest = FindViewById<Button>(Resource.Id.btnList);
                Button btnSearch = FindViewById<Button>(Resource.Id.btnSearch);

                //btnRequest.Click += btnRequest_Click;
                btnSearch.Click += btnSearch_Click;

                DefaultView.SetBackgroundColor(Color.White);

                var CustApprovalAdapter = new Adapter.SR_CustomerApprovalAdapter(this);
                var CustApprovalListView = FindViewById<ListView>(Resource.Id.lstRequest);
                var tvheader = FindViewById<TextView>(Resource.Id.tv_header);

                tvheader.SetTextColor(Color.Black);
                CustApprovalListView.Adapter = CustApprovalAdapter;
                CustApprovalAdapter.OnClickItem += CustApprovalAdapter_OnClickItem;

            }
            catch (Exception e)
            {
                string t = e.Message;
            }

        }
        //void btnRequest_Click(object sender, EventArgs e)
        //{
        //    var CustApprovalAdapter = new Adapter.SR_CustomerApprovalAdapter(this);
        //    var CustApprovalListView = FindViewById<ListView>(Resource.Id.lstRequest);
        //    var tvheader = FindViewById<TextView>(Resource.Id.tv_header);

        //    tvheader.SetTextColor(Color.Black);
        //    CustApprovalListView.Adapter = CustApprovalAdapter;
        //    CustApprovalAdapter.OnClickItem += CustApprovalAdapter_OnClickItem;
        //}
        void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ListView CustApprovalListView = FindViewById<ListView>(Resource.Id.lstRequest);
                EditText RequestID = FindViewById<EditText>(Resource.Id.txtSearchID);
                TextView tvheader = FindViewById<TextView>(Resource.Id.tv_header);
                int intRequestID;
                int.TryParse(RequestID.Text.ToString(), out intRequestID);
                var CustApprovalAdapter = new Adapter.SR_CustomerApprovalAdapter(this, intRequestID);

                tvheader.SetTextColor(Color.Black);
                CustApprovalListView.Adapter = CustApprovalAdapter;
                CustApprovalAdapter.OnClickItem += CustApprovalAdapter_OnClickItem;
            }
            catch (Exception re)
            {
                string t = re.Message;
            }
        }
        void CustApprovalAdapter_OnClickItem(int oIndex)
        {
            DBCustRepository orDb2 = new DBCustRepository();
            SR_Session oSession = new SR_Session();
            oSession = orDb2.SetSessionValues(oSession);
            string oUserID = oSession.SR_UserID;
            STSR_Droid.Models.SR_CustomerApprove oRequest = new STSR_Droid.Models.SR_CustomerApprove();
            SetContentView(Resource.Layout.SR_CustApprovalEdit);
            DBCustRepository dbr = new DBCustRepository();

            List<SR_CustomerApprove> lstRequest = new List<SR_CustomerApprove>();

            List<SRWS_ASMX.SR_CustomerApprove> vData = dbr.SR_SPWorkflow_SubmitbyCustRequestID(oUserID, oIndex.ToString());
            List<SRWS_ASMX.SR_CustomerApprove> itemData = new List<SRWS_ASMX.SR_CustomerApprove>();


            foreach (var item in vData)
            {
                List<SRWS_ASMX.SR_CustomerApprove> oProItem = new List<SRWS_ASMX.SR_CustomerApprove>();
                oProItem.Add(item);
                oRequest.LoadModel(oProItem);
                //oRequest.RequestID = item.RequestID;
                //oRequest.RequestStatusID = item.RequestStatusID;
                //oRequest.Status = item.Status;
                itemData.Add(item);
            }



            //STSR_Droid.Models.SR_Request oRequest = new STSR_Droid.Models.SR_Request();
            //SetContentView(Resource.Layout.SR_CustApprovalEdit);
            //DBCustRepository dbr = new DBCustRepository();

            ////var txtRequestID = FindViewById<TextView>(Resource.Id.txtRequestID);
            ////txtRequestID.Text = "Request ID: " + oIndex.ToString();
            ////StartActivity(typeof(SR_CustApprovalEditActivity));

            
            ////SetContentView(Resource.Layout.SR_RequestEdit);
            ////DBRequestRepository dbr = new DBRequestRepository();

            //List<SR_Request> lstRequest = new List<SR_Request>();
            //var vData = dbr.SR_GetRequestbyID(oIndex.ToString());

            //foreach (var item in vData)
            //{

            //    oRequest.RequestID = item.RequestID;
            //    oRequest.RequestStatusID = item.RequestStatusID;
            //    oRequest.Status = item.Status;
            //}

            TextView lblRequestID = FindViewById<TextView>(Resource.Id.lblRequestID);
            TextView txtRequestID = FindViewById<TextView>(Resource.Id.txtRequestID);
            TextView lblStatus = FindViewById<TextView>(Resource.Id.lblStatus);
            TextView txtStatus = FindViewById<TextView>(Resource.Id.txtStatus);
            TextView lblBranch = FindViewById<TextView>(Resource.Id.lblBranch);
            TextView txtBranch = FindViewById<TextView>(Resource.Id.txtBranch);

            txtRequestID.SetTextColor(Color.Red);
            txtStatus.SetTextColor(Color.Red);
            txtBranch.SetTextColor(Color.Red);
            lblRequestID.SetTextColor(Color.Black);
            lblStatus.SetTextColor(Color.Black);
            lblBranch.SetTextColor(Color.Black);

            //int RequestID = CInt(txtRequestID.Tag);

            txtRequestID.Text = "Request ID: " + oIndex.ToString();
            txtRequestID.Tag = oIndex.ToString();
            txtStatus.Text = oRequest.Status;
            //StartActivity(typeof(SR_RequestEditActivity));


            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            SlidingTabFragment fragment = new SlidingTabFragment();
            fragment.RequestID = oIndex.ToString();
            fragment.ModuleType = "CustomerApproval";
            fragment.mod_PortalCustomerApprove = itemData;
            transaction.Replace(Resource.Id.sample_content_fragment, fragment);
            transaction.Commit();

        }
    }
}
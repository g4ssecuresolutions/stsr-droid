using System;
using System.Data;
using System.IO;
using SQLite;
////using System.Web.Services;
using System.Collections.Generic;
using SR_Class;

using STSR_Droid.Business;
using STSR_Droid.SQLiteTables;
using STSR_Droid.Models;
using Android.Database.Sqlite;

namespace STSR_Droid.ORM
{
    public class DBCustRepository : SR_Class.SR_DataAccess
    {
        public List<SR_CustomerApprove> Request()
        {
            return Request(-1);
        }
        public List<SR_CustomerApprove> Request(int oRequestID)
        {
            // Declarations
            List<SR_CustomerApprove> Rtn = new List<SR_CustomerApprove>();
            try
            {
                // Declarations

                
                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                string oUserID = rec.SR_UserID.ToString();
                string oUserCustAccess = rec.SR_UserCustAccess.ToString();

                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                
                SR_ServiceManage sm = new SR_ServiceManage();
                SR_DataConversion dc = new SR_DataConversion();
                if (oRequestID == -1)
                {
                    // Listing
                    // Convert from SRWS_ASMX to STSR_Droid.Models.SR_CustomerApprove
                    Bsn_CustomerApprove oCustomer = new Bsn_CustomerApprove();
                    List<SRWS_ASMX.SR_RequestItem> oData = null;
                    var vData = oData;
                    vData = sm.SR_Workflow_ClientSelect(oUserID,oUserCustAccess);
                    //If the device is disconnected or not 
                    if (vData != null)  //Connected
                    {
                        // Load from Web Services
                        Rtn = oCustomer.LoadModelFromService(vData);
                        // Reload the Backup Table
                        string Result = oCustomer.LoadTable(Rtn);

                    }
                    else
                    {      //Disconnected
                           //Read from the backup
                        Rtn = oCustomer.LoadModelFromBackup();
                    }
                }
                else
                {
                    // Individual 
                    // Convert from SRWS_ASMX to STSR_Droid.Models.SR_CustomerApprove
                    Bsn_CustomerApprove oApproval = new Bsn_CustomerApprove();
                    List<SRWS_ASMX.SR_G4SApprove> oData = null;
                    var wData = oData;
                    wData = sm.SR_SPWorkflow_SubmitbyRequestID(oUserID,oRequestID.ToString());
                    //If the device is disconnected or not 
                    if (wData != null)  //Connected
                    {
                        // Load from Web Services
                        Rtn = oApproval.LoadModelFromItemService(wData);
                        // Reload the Backup Table
                        string Result = oApproval.LoadTable(Rtn);

                    }
                    else
                    {      //Disconnected
                           //Read from the backup
                        Rtn = oApproval.LoadModelFromBackup();
                    }
                }


            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }

        public string SaveCustApproveRequest(SR_CustomerApprove oRequest, SR_CustomerApprove oUnchangedRequest)
        {

            string Rtn = "";
            try
            {
                // Declarations
                DBCustRepository orDb2 = new DBCustRepository();
                SR_Session SRSession = new SR_Session();
                SRSession = orDb2.SetSessionValues(SRSession);
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services

                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.SaveCustApproveRequest(SRSession, oRequest, oUnchangedRequest, Convert.ToInt32(rec.SR_UserID));

            }
            catch (Exception e)
            {
                Rtn = e.Message;

            }
            //return Rtn;
            return Rtn;
        }

        public SR_Session SetSessionValues(SR_Session Rtn)
        {
            //SR_Session Rtn = new SR_Session();
            try
            {
                // Declarations

                Bsn_Request oRequest = new Bsn_Request();
                // Need to get the UserID from the system
                SR_Class.SR_System oSysSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSysSession.GetSession();

                Rtn.SR_RoleID = rec.SR_RoleID;
                Rtn.SR_UserID = rec.SR_UserID;
                Rtn.SR_UserName = rec.SR_UserName;
                Rtn.SR_UserFullName = rec.SR_UserFullName;
                Rtn.SR_UserCustAccess = rec.SR_UserCustAccess;
                //Rtn.SR_CorpCustID = rec.SR_CorpCustID;
                Rtn.SR_Approve = (rec.SR_Approve) ? "True" : "False";
                Rtn.SR_Extend = (rec.SR_Extend) ? "True" : "False";
                Rtn.SR_CustApprove = (rec.SR_CustApprove) ? "True" : "False";
                Rtn.SR_CustExtend = (rec.SR_CustExtend) ? "True" : "False";
                Rtn.SR_LoggedIn = (rec.SR_LoggedIn) ? "True" : "False";
                Rtn.SR_QAS_Mode = (rec.SR_QAS_Mode) ? "True" : "False";
                //Rtn.SR_UserAttempt = rec.SR_UserAttempt;
                //Rtn.SR_Action = rec.SR_Action;
            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public bool SendQuestionRequest(SR_CustomerApprove oRequest,string oMessageText)
        {

            bool Rtn = false;
            try
            {
                // Declarations

                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.SendQuestionRequest(oRequest, oMessageText, Convert.ToInt32(rec.SR_UserID));

            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
    }
}
using System;
using System.Data;
using System.IO;
using SQLite;
////using System.Web.Services;
using System.Collections.Generic;
using SR_Class;

using STSR_Droid.Business;
using STSR_Droid.SQLiteTables;
using STSR_Droid.Models;
using Android.Database.Sqlite;

namespace STSR_Droid.ORM
{
    public class DBLckUpRepository : SR_Class.SR_DataAccess
    {
        public List<SR_LookUp> CustomerList()
        {
            List<SR_LookUp> Rtn = new List<SR_LookUp>();
            try
            {
                // Declarations

                Bsn_Request oRequest = new Bsn_Request();
                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                List<SRWS_ASMX.SR_CustomerItem> oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                if (rec.SR_UserCustAccess.ToString() == "0")
                {
                    vData = sm.SR_GetCustomerList();
                } else
                {
                    vData = sm.SR_GetAllCustomersbyUserID(rec.SR_UserID.ToString().Trim());
                }
                

                //If the device is disconnected or not 
                if (vData != null)  //Connected
                {
                    // Load from Web Services

                    foreach (SRWS_ASMX.SR_CustomerItem itm in vData)
                    {
                        Rtn.Add(new SR_LookUp { Index= itm.CorpCustID,Value=itm.CustName });
                    }

                }
                else
                {      //Disconnected
                    //Read from the backup
                    Rtn = null;
                }
            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }

        public List<SR_LookUp> SiteList(string oCode)
        {
            List<SR_LookUp> Rtn = new List<SR_LookUp>();
            try
            {
                // Declarations

                Bsn_Request oRequest = new Bsn_Request();
                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                List<SRWS_ASMX.SR_SiteItem> oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();

                vData = sm.SR_GetSiteList(oCode);

                //If the device is disconnected or not 
                if (vData != null)  //Connected
                {
                    // Load from Web Services

                    foreach (SRWS_ASMX.SR_SiteItem itm in vData)
                    {
                        Rtn.Add(new SR_LookUp { Index = itm.SiteID, Value = itm.DisplaySite });
                    }

                }
                else
                {      //Disconnected
                    //Read from the backup
                    Rtn = null;
                }
            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }

        public List<SR_LookUp> SiteListbyState(string oCode, string oState)
        {
            List<SR_LookUp> Rtn = new List<SR_LookUp>();
            try
            {
                // Declarations

                Bsn_Request oRequest = new Bsn_Request();
                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                List<SRWS_ASMX.SR_SiteItem> oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();

                vData = sm.SR_GetSiteListbyState(oCode, oState);

                //If the device is disconnected or not 
                if (vData != null)  //Connected
                {
                    // Load from Web Services

                    foreach (SRWS_ASMX.SR_SiteItem itm in vData)
                    {
                        Rtn.Add(new SR_LookUp { Index = itm.SiteID, Value = itm.DisplaySite });
                    }

                }
                else
                {      //Disconnected
                    //Read from the backup
                    Rtn = null;
                }
            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }


        public List<SR_LookUp> StateList()
        {
            List<SR_LookUp> Rtn = new List<SR_LookUp>();
            try
            {
                // Declarations

                Bsn_Request oRequest = new Bsn_Request();
                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                List<SRWS_ASMX.SR_StateItem> oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();

                vData = sm.SR_GetStateList();

                //If the device is disconnected or not 
                if (vData != null)  //Connected
                {
                    // Load from Web Services

                    foreach (SRWS_ASMX.SR_StateItem itm in vData)
                    {
                        Rtn.Add(new SR_LookUp { Index = itm.Code, Value = itm.Description });
                    }

                }
                else
                {      //Disconnected
                    //Read from the backup
                    Rtn = null;
                }
            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public List<SR_LookUp> OfficerTypeList()
        {
            List<SR_LookUp> Rtn = new List<SR_LookUp>();
            try
            {
                // Declarations

                Bsn_Request oRequest = new Bsn_Request();
                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                List<SRWS_ASMX.SR_LookUpItem> oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();

                vData = sm.SR_GetOfficerTypeList();

                //If the device is disconnected or not 
                if (vData != null)  //Connected
                {
                    // Load from Web Services

                    foreach (SRWS_ASMX.SR_LookUpItem itm in vData)
                    {
                        Rtn.Add(new SR_LookUp { Index = itm.Code, Value = itm.Description });
                    }

                }
                else
                {      //Disconnected
                    //Read from the backup
                    Rtn = null;
                }
            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public List<SR_LookUp> ReasonServicesList()
        {
            List<SR_LookUp> Rtn = new List<SR_LookUp>();
            try
            {
                // Declarations

                Bsn_Request oRequest = new Bsn_Request();
                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                List<SRWS_ASMX.SR_LookUpItem> oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();

                vData = sm.SR_GetReasonServicesList();

                //If the device is disconnected or not 
                if (vData != null)  //Connected
                {
                    // Load from Web Services

                    foreach (SRWS_ASMX.SR_LookUpItem itm in vData)
                    {
                        Rtn.Add(new SR_LookUp { Index = itm.Code, Value = itm.Description });
                    }

                }
                else
                {      //Disconnected
                    //Read from the backup
                    Rtn = null;
                }
            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public List<SR_LookUp> LocationTypeList()
        {
            List<SR_LookUp> Rtn = new List<SR_LookUp>();
            try
            {
                // Declarations

                Bsn_Request oRequest = new Bsn_Request();
                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                List<SRWS_ASMX.SR_LookUpItem> oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();

                vData = sm.SR_GetLocationTypeList();

                //If the device is disconnected or not 
                if (vData != null)  //Connected
                {
                    // Load from Web Services

                    foreach (SRWS_ASMX.SR_LookUpItem itm in vData)
                    {
                        Rtn.Add(new SR_LookUp { Index = itm.Code, Value = itm.Description });
                    }

                }
                else
                {      //Disconnected
                    //Read from the backup
                    Rtn = null;
                }
            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public List<SR_LookUp> ReasonDenyList()
        {
            List<SR_LookUp> Rtn = new List<SR_LookUp>();
            try
            {
                // Declarations

                Bsn_Request oRequest = new Bsn_Request();
                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                List<SRWS_ASMX.SR_LookUpItem> oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();

                vData = sm.SR_GetReasonDeny();

                //If the device is disconnected or not 
                if (vData != null)  //Connected
                {
                    // Load from Web Services

                    foreach (SRWS_ASMX.SR_LookUpItem itm in vData)
                    {
                        Rtn.Add(new SR_LookUp { Index = itm.Code, Value = itm.Description });
                    }

                }
                else
                {      //Disconnected
                    //Read from the backup
                    Rtn = null;
                }
            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }


        public List<SR_LookUp> DepartmentList()
        {
            List<SR_LookUp> Rtn = new List<SR_LookUp>();
            try
            {
                // Declarations

                Bsn_Request oRequest = new Bsn_Request();
                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                List<SRWS_ASMX.SR_LookUpItem> oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();

                vData = sm.SR_GetDepartmentList();

                //If the device is disconnected or not 
                if (vData != null)  //Connected
                {
                    // Load from Web Services

                    foreach (SRWS_ASMX.SR_LookUpItem itm in vData)
                    {
                        Rtn.Add(new SR_LookUp { Index = itm.Code, Value = itm.Description });
                    }

                }
                else
                {      //Disconnected
                    //Read from the backup
                    Rtn = null;
                }
            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
    }
}
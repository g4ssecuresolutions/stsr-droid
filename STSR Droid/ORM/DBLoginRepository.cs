using System;
using System.Data;
using System.IO;
using System.Net;
using System.Xml;
using SQLite;
////using System.Web.Services;
using SR_Class;
using STSR_Droid.SQLiteTables;
using STSR_Droid.Models;


namespace STSR_Droid.ORM
{
    public class DBLoginRepository: SR_Class.SR_DataAccess
    {

        public string Login(string oUserID,string oPassword ) {

            

            var output = "";
            output += "Login to STSR Application.";
            //var dbLite = LiteConnection();
            if (string.IsNullOrEmpty(oUserID))
            {
                output += "Please enter your User ID.";
                return output;
            }
            if (string.IsNullOrEmpty(oPassword))
            {
                output += "Please enter your Password.";
                return output;
            }
            try
            {
                SR_Class.SR_ServiceManage oService = new SR_Class.SR_ServiceManage();
                //var zData = oService.SR_LoginAuthentication(oUserID, oPassword);
                var vData = oService.SR_LoginAuthentication(oUserID, oPassword);
                //var xData = oService.SR_SPRequestSelect("301", 1);
                //if (vData != null)
                if (true)
                    {
                        int Cnt = 0;
                    SR_UserLogin Pro = new SR_UserLogin();

                    ////temporary
                    //Cnt = 1;
                    //Pro.SR_UserID = "301"; // "453";
                    //Pro.SR_RoleID = "5";
                    //Pro.SR_FullName = "Efrain Contreras";
                    //Pro.NuPassword = "1";
                    //Pro.WFApprover = "1";
                    //Pro.WFExtender = "0";
                    //Pro.ClientApprover = "1";
                    //Pro.ClientExtender = "0";
                    //Pro.CorpCustID = "0";
                    //Pro.ResetPassword = "0";
                    //End temporary


                    foreach (var item in vData)
                    {
                        Cnt = ++Cnt;
                        Pro.SR_UserID = item.SR_UserID;
                        Pro.SR_RoleID = item.SR_RoleID;
                        Pro.SR_FullName = item.FullName;
                        Pro.NuPassword = item.NUPassword.ToString();
                        Pro.WFApprover = item.WFApprover.ToString();
                        Pro.WFExtender = item.WFExtender.ToString();
                        Pro.ClientApprover = item.ClientApprover.ToString();
                        Pro.ClientExtender = item.ClientExtender.ToString();
                        Pro.CorpCustID = item.CorpCustID;
                        Pro.ResetPassword = item.ResetPassword.ToString();
                        //IsEULA = item.IsEULA;
                    }
                    if (Cnt == 0)
                    {
                        output += "\nUser not found. Possibilities: ";
                        output += "\n1.- your User ID / Password is not correct.";
                        output += "\n2.- your account had been disabled for security reason. Please contact your administrator at 866.943.8892.";
                        return output;
                    }
                    else
                    {
                        if (Pro.ResetPassword == "0")
                        {
                            output += "Please, be aware your password had been reset. If you have problems please contact your administrator at 866.943.8892.";
                            try {
                                //var vExec = ServiceConnection().SR_ResetPassword(Pro.SR_UserID);
                            }
                            catch {
                                output += "\nError encountered in ResetPasswordFlag code.";
                            }
                        }
                        if (FirstTime(Pro.SR_UserID))
                        {
                            //Save SR_UserID and Redirect to page of terms
                            SQLiteConnection odb = CreateSystemDB();

                            string oResult = CreateSessionTable();
                            if (oResult != "SUCCESS")
                            {
                                output += "\n" + oResult;
                                return output;
                            }
                            oResult = CleanSessionTable();

                            if (oResult == "SUCCESS")
                            {
                                TblSession rec = new TblSession();
                                rec.SR_UserID = Pro.SR_UserID;
                                rec.SR_UserName = oUserID;

                                rec.SR_RoleID = Pro.SR_RoleID;
                                rec.SR_Approve = (Pro.WFApprover.ToLower() == "true") ? true : false;
                                rec.SR_Extend = (Pro.WFExtender.ToLower() == "true") ? true : false;
                                rec.SR_CustApprove = (Pro.ClientApprover.ToLower() == "true") ? true : false;
                                rec.SR_CustExtend = (Pro.ClientExtender.ToLower() == "true") ? true : false;
                                rec.SR_UserFullName = Pro.SR_FullName;
                                rec.SR_UserCustAccess = Pro.CorpCustID;
                                rec.SR_CorpCustID= Pro.CorpCustID;
                                rec.SR_LoggedIn = true;
                                rec.SR_QAS_Mode = false;
                                rec.SR_EULA = IsEULA;

                                odb.Insert(rec);

                                //// testing performance
                                //var table = odb.Table<SR_Session>();
                                //foreach (var item in table)
                                //{
                                //    var it = item.SR_UserFullName;
                                //}
                                //// end testing performance
                            }
                            else
                            {
                                output += "\n" + oResult;
                                return output;
                            }
                            //Redirect to Default Page
                            output = "FIRSTIME";
                            return output;
                        }
                        else
                        {

                            //Save SR_RoleID,SR_Approve,SR_Extend,SR_CustApprove,SR_CustExtend,SR_UserID,SR_UserName
                            //SR_UserFullName,SR_UserCustAccess,SR_LoggedIn,SR_QAS_Mode
                            SQLiteConnection odb = CreateSystemDB();

                            string oResult = CreateSessionTable();
                            if (oResult != "SUCCESS")
                            {
                                output += "\n" + oResult;
                                return output;
                            }
                            oResult = CleanSessionTable();

                            if (oResult == "SUCCESS") {
                                TblSession rec = new TblSession();
                                rec.SR_UserID = Pro.SR_UserID;
                                rec.SR_UserName = oUserID;

                                rec.SR_RoleID = Pro.SR_RoleID;
                                rec.SR_Approve = (Pro.WFApprover.ToLower()=="true")? true:false;
                                rec.SR_Extend = (Pro.WFExtender.ToLower() == "true")? true:false;
                                rec.SR_CustApprove = (Pro.ClientApprover.ToLower() == "true")? true :false;
                                rec.SR_CustExtend = (Pro.ClientExtender.ToLower() == "true")? true :false;
                                rec.SR_UserFullName = Pro.SR_FullName;
                                rec.SR_UserCustAccess = Pro.CorpCustID;
                                rec.SR_LoggedIn = true;
                                rec.SR_QAS_Mode = false;
                                rec.SR_EULA = IsEULA;

                                odb.Insert(rec);
                                //// testing performance
                                //var table = odb.Table<SR_Session>();
                                //foreach (var item in table) {
                                //    var it = item.SR_UserFullName;
                                //}
                                //// end testing performance
                            }
                            else {
                                output += "\n" + oResult;
                                return output;
                            }
                            //Redirect to Default Page
                            output = "GRANTED";
                            return output;
                        }

                    }
                    //output += "\nLogged in...";
                }
                else
                {
                }
            }
            catch (Exception e)
            {
                output += "There are some problem with the connection. Please try later. Error:" +e.Message ;
            }
            
            return output;
        }



    }
}
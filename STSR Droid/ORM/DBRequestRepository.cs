using System;
using System.Data;
using System.IO;
using SQLite;
////using System.Web.Services;
using System.Collections.Generic;
using SR_Class;
    
using STSR_Droid.Business;
using STSR_Droid.SQLiteTables;
using STSR_Droid.Models;
using Android.Database.Sqlite;

namespace STSR_Droid.ORM
{
    public class DBRequestRepository : SR_Class.SR_DataAccess
    {

        public Boolean LastPage = false;
        public List<SR_Request> Request()
        {
            return Request(-1);
        }
        public List<SR_Request> Request(int oRequestID)
        {
            List<SR_Request> Rtn = new List<SR_Request>();
            try { 
                // Declarations
            
                Bsn_Request oRequest = new Bsn_Request();
                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                List<SRWS_ASMX.SR_RequestItem> oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                if (oRequestID == -1)
                {
                    vData =  sm.SR_SPRequestSelect(rec.SR_UserID, 1);
                   
                }
                else
                {
                    vData = sm.SR_GetRequestbyID(oRequestID.ToString());
                }

                //If the device is disconnected or not 
                if (vData != null)  //Connected
                {
                    // Load from Web Services
                    Rtn= oRequest.LoadModelFromService(vData);
                    // Reload the Backup Table
                    string Result = oRequest.LoadTable(Rtn);

                }
                else {      //Disconnected
                    //Read from the backup
                    Rtn = oRequest.LoadModelFromBackup();
                }
            }
            catch (Exception e) {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public List<SR_Request> Request(int oRequestID,int oPage)
        {
            oRequestID = -1; //Always
            List<SR_Request> Rtn = new List<SR_Request>();
            try
            {
                // Declarations

                Bsn_Request oRequest = new Bsn_Request();
                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                List<SRWS_ASMX.SR_RequestItem> oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;

                SR_DataConversion dc = new SR_DataConversion();
                vData = sm.SR_SPRequestSelect(rec.SR_UserID, oPage);

                //If the device is disconnected or not 
                if (vData != null)  //Connected
                {
                    // Load from Web Services
                    Rtn = oRequest.LoadModelFromService(vData);
                    // Reload the Backup Table
                    string Result = oRequest.LoadTable(Rtn);
                    if (vData.Count < 25) { LastPage = true; }
                }
                else
                {      //Disconnected
                    //Read from the backup
                    Rtn = oRequest.LoadModelFromBackup();
                }
            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }

        public SR_SiteAddress GetSiteAddressbyLocation(string oSiteID)
        {
            
            SR_SiteAddress Rtn = new SR_SiteAddress();
            try
            {
                // Declarations

                Bsn_Request oRequest = new Bsn_Request();
                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                vData = sm.SR_GetSiteAddressbyLocation(oSiteID);

                //If the device is disconnected or not 
                if (vData != null)  //Connected
                {
                    // Load from Web Services
                    Rtn = oRequest.LoadAddressSiteFromService(vData);
                    // Reload the Backup Table
                    //string Result = oRequest.LoadTable(Rtn);

                }
                else
                {      //Disconnected
                    //Read from the backup
                    Rtn = null;
                }
            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public SR_SiteAddress GetSiteAddressbySite(string oSiteID)
        {

            SR_SiteAddress Rtn = new SR_SiteAddress();
            try
            {
                // Declarations

                Bsn_Request oRequest = new Bsn_Request();
                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                vData = sm.SR_GetSiteAddressbySite(oSiteID);

                //If the device is disconnected or not 
                if (vData != null)  //Connected
                {
                    // Load from Web Services
                    Rtn = oRequest.LoadAddressSiteFromService(vData);
                    // Reload the Backup Table
                    //string Result = oRequest.LoadTable(Rtn);

                }
                else
                {      //Disconnected
                    //Read from the backup
                    Rtn = null;
                }
            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }

        public Boolean SaveNewHoldRequest(SR_Request oRequest)
        {

            Boolean Rtn = false;
            try
            {
                // Declarations

                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                if (oRequest.RequestedBy == 0) { oRequest.RequestedBy = Convert.ToInt32(rec.SR_UserID); }
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.SaveNewHoldRequest(oRequest.ToSRWS_ASMX());

            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }

        public Boolean DeleteHoldRequest(SR_Request oRequest)
        {

            Boolean Rtn = false;
            try
            {
                // Declarations

                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.DeleteHoldRequest(oRequest.ToSRWS_ASMX());

            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public Boolean SaveApprovedRequest(SR_Request oRequest)
        {

            Boolean Rtn = false;
            try
            {
                // Declarations

                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                if (oRequest.RequestedBy == 0) { oRequest.RequestedBy = Convert.ToInt32(rec.SR_UserID); }
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.SaveApprovedRequest(oRequest.ToSRWS_ASMX());

            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public Boolean SaveCancelledRequestbyID(SR_Request oRequest)
        {

            Boolean Rtn = false;
            try
            {
                // Declarations

                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                if (oRequest.RequestedBy == 0) { oRequest.RequestedBy = Convert.ToInt32(rec.SR_UserID); }
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.SaveCancelledRequestbyID(oRequest.ToSRWS_ASMX());

            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public Boolean SaveHoldRequest(SR_Request oRequest)
        {

            Boolean Rtn = false;
            try
            {
                // Declarations

                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                if (oRequest.RequestedBy == 0) { oRequest.RequestedBy = Convert.ToInt32(rec.SR_UserID); }
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.SaveHoldRequest(oRequest.ToSRWS_ASMX());

            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public int SaveNewRequest(SR_Request oRequest)
        {

            int Rtn = 0;
            try
            {
                // Declarations

                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                if (oRequest.RequestedBy == 0) { oRequest.RequestedBy = Convert.ToInt32(rec.SR_UserID); }
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.SaveNewRequest(oRequest.ToSRWS_ASMX());

            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public Boolean SaveCancelledRequest(SR_Request oRequest)
        {

            Boolean Rtn = false;
            try
            {
                // Declarations

                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                if (oRequest.RequestedBy == 0) { oRequest.RequestedBy = Convert.ToInt32(rec.SR_UserID); }
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.SaveCancelledRequest(oRequest.ToSRWS_ASMX());

            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public Boolean SaveTerminatedRequest(SR_Request oRequest)
        {

            Boolean Rtn = false;
            try
            {
                // Declarations

                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                if (oRequest.RequestedBy == 0) { oRequest.RequestedBy = Convert.ToInt32(rec.SR_UserID); }
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.SaveTerminatedRequest(oRequest.ToSRWS_ASMX());

            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public Boolean SaveRequest(SR_Request oRequest)
        {

            Boolean Rtn = false;
            try
            {
                // Declarations

                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                if (oRequest.RequestedBy == 0) { oRequest.RequestedBy = Convert.ToInt32(rec.SR_UserID); }
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.SaveRequest(oRequest.ToSRWS_ASMX());

            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public new int GetHoldRequestID()
        {

            int Rtn = -1;
            try
            {
                // Declarations

                // Need to get the UserID from the system
                SR_Class.SR_System oSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSession.GetSession();
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.GetHoldRequestID();

            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }

        public SR_Session SetSessionValues(SR_Session Rtn)
        {
            //SR_Session Rtn = new SR_Session();
            try
            {
                // Declarations

                Bsn_Request oRequest = new Bsn_Request();
                // Need to get the UserID from the system
                SR_Class.SR_System oSysSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSysSession.GetSession();

                Rtn.SR_RoleID = rec.SR_RoleID;
                Rtn.SR_UserID = rec.SR_UserID;
                Rtn.SR_UserName = rec.SR_UserName;
                Rtn.SR_UserFullName = rec.SR_UserFullName;
                Rtn.SR_UserCustAccess = rec.SR_UserCustAccess;
                //Rtn.SR_CorpCustID = rec.SR_CorpCustID;
                Rtn.SR_Approve = (rec.SR_Approve)? "True":"False";
                Rtn.SR_Extend = (rec.SR_Extend) ? "True" : "False";
                Rtn.SR_CustApprove = (rec.SR_CustApprove)? "True":"False";
                Rtn.SR_CustExtend = (rec.SR_CustExtend)? "True":"False";
                Rtn.SR_LoggedIn = (rec.SR_LoggedIn)? "True":"False";
                Rtn.SR_QAS_Mode = (rec.SR_QAS_Mode)? "True":"False";
                //Rtn.SR_UserAttempt = rec.SR_UserAttempt;
                //Rtn.SR_Action = rec.SR_Action;
            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public string MigrateHoldRequests(SR_Request oRequest)
        {
            string Rtn="";
            //int strRequestID = 0;
            try
            {
                // Declarations

                // Need to get the UserID from the system
                DBG4SRepository orDb2 = new DBG4SRepository();
                SR_Session SRSession = new SR_Session();
                SRSession = orDb2.SetSessionValues(SRSession);
                SRSession.SR_Action = "Hold";
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                //SRWS_ASMX.SR_RequestItem oData = null;
                if (oRequest.RequestedBy == 0) { oRequest.RequestedBy = Convert.ToInt32(SRSession.SR_UserID); }
                SR_ServiceManage sm = new SR_ServiceManage();
                Rtn = sm.SR_SaveRequestService(SRSession, oRequest.ToSRWS_ASMX());

                //var vData = oData;
                //SR_DataConversion dc = new SR_DataConversion();
                //List<SRWS_ASMX.SR_RequestItem> HoldList = sm.SR_GetRequestHoldList();
                //foreach (SRWS_ASMX.SR_RequestItem item in HoldList)
                //{
                //    SR_Request itemRequest = new SR_Request();
                //    itemRequest.LoadModel(item);
                //    int oSourceRequestID = itemRequest.RequestID;


                //    itemRequest.RequestStatusID = 2;
                //    strRequestID = sm.SaveNewRequest(itemRequest.ToSRWS_ASMX());
                //    itemRequest.RequestID = strRequestID;
                //    int oTargetRequestID = strRequestID;
                //    sm.SR_RequestExtension(itemRequest.ToSRWS_ASMX());

                //    sm.SR_RequestNotes(oSourceRequestID, oTargetRequestID);


                //}
                //Rtn = true;
            }
            catch (Exception e)
            {
                string t = e.Message;
                Rtn = "Cannot reach database. System detected a problem.";
            }
            //return Rtn;
            return Rtn;
        }

        public  string SubmitRequest(SR_Request oRequest)
        {

            string Rtn = "";
            try
            {
                // Declarations

                // Need to get the UserID from the system
                DBG4SRepository orDb2 = new DBG4SRepository();
                SR_Session SRSession = new SR_Session();
                SRSession = orDb2.SetSessionValues(SRSession);
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.SR_SubmitRequest(SRSession, oRequest.ToSRWS_ASMX());

            }
            catch (Exception e)
            {
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }

        public  string ResetRequest(SR_Request oRequest)
        {

            string Rtn = "";
            try
            {
                // Declarations

                // Need to get the UserID from the system
                DBG4SRepository orDb2 = new DBG4SRepository();
                SR_Session SRSession = new SR_Session();
                SRSession = orDb2.SetSessionValues(SRSession);
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.SR_ResetRequest(SRSession, oRequest.ToSRWS_ASMX());

            }
            catch (Exception e)
            {
                Rtn = "Error";
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public string SaveRequestService(SR_Request oRequest,string oAction)
        {

            string Rtn = "";
            try
            {
                // Declarations

                // Need to get the UserID from the system
                DBG4SRepository orDb2 = new DBG4SRepository();
                SR_Session SRSession = new SR_Session();
                SRSession = orDb2.SetSessionValues(SRSession);
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                SRSession.SR_Action = oAction;
                SRSession.SR_Status = "Open";
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                if (oRequest.RequestedBy == 0) { oRequest.RequestedBy = Convert.ToInt32(SRSession.SR_UserID); }
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.SR_SaveRequestService(SRSession, oRequest.ToSRWS_ASMX());

            }
            catch (Exception e)
            {
                Rtn = "Error";
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        public string SaveHoldRequests(SR_Request oRequest)
        {

            string Rtn = "";
            try
            {
                // Declarations

                // Need to get the UserID from the system
                DBG4SRepository orDb2 = new DBG4SRepository();
                SR_Session SRSession = new SR_Session();
                SRSession = orDb2.SetSessionValues(SRSession);
                //Real source Data is exec usp_SR_Requests_Select_04 443
                //Read the Request from the Web Services
                //SRSession.SR_Action = oAction;
                SRSession.SR_Status = "Hold";
                SRWS_ASMX.SR_RequestItem oData = null;
                SR_ServiceManage sm = new SR_ServiceManage();
                var vData = oData;
                oRequest.RequestedBy = Convert.ToInt32(SRSession.SR_UserID); 
                SR_DataConversion dc = new SR_DataConversion();
                Rtn = sm.SR_SaveHoldRequests(SRSession, oRequest.ToSRWS_ASMX());

            }
            catch (Exception e)
            {
                Rtn = "Error";
                string t = e.Message;
            }
            //return Rtn;
            return Rtn;
        }
        
    }
}


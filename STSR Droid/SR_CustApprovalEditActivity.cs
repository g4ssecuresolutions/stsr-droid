using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using STSR_Droid.ORM;
using STSR_Droid.Class;
using Android.Content.PM;
using Android.Graphics;

namespace STSR_Droid
{
    [Activity(Label = "SR_CustApprovalEditActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SR_CustApprovalEditActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.SR_CustApprovalEdit);


            //TextView lblRequestID = FindViewById<TextView>(Resource.Id.lblRequestID);
            //TextView txtRequestID = FindViewById<TextView>(Resource.Id.txtRequestID);
            //TextView lblStatus = FindViewById<TextView>(Resource.Id.lblStatus);
            //TextView txtStatus = FindViewById<TextView>(Resource.Id.txtStatus);
            //TextView lblBranch = FindViewById<TextView>(Resource.Id.lblBranch);
            //TextView txtBranch = FindViewById<TextView>(Resource.Id.txtBranch);
            //txtRequestID.SetTextColor(Color.Red);
            //txtStatus.SetTextColor(Color.Red);
            //txtBranch.SetTextColor(Color.Red);
            //lblRequestID.SetTextColor(Color.Black);
            //lblStatus.SetTextColor(Color.Black);
            //lblBranch.SetTextColor(Color.Black);


            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            SlidingTabFragment fragment = new SlidingTabFragment();
            fragment.ModuleType = "CustomerApproval";
            transaction.Replace(Resource.Id.sample_content_fragment, fragment);
            transaction.Commit();
        }
    }
}
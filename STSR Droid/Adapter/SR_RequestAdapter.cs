using System;
using System.Collections.Generic;
using System.IO;
using Android.App;
using Android.Content;
using Android.Provider;
using Android.Views;
using Android.Widget;
using SR_Class;
using STSR_Droid.Models;
using STSR_Droid.ORM;
using Android.Graphics;


namespace STSR_Droid.Adapter
{
    public delegate void RaiseRequestItem(int oIndex);

    public class SR_RequestAdapter: BaseAdapter
    {
        List<Models.SR_Request> _RequestList;
        Activity _activity;
        public int RefPage;
        public Boolean LastPage = false;
        public event RaiseRequestItem OnClickItem;

        public SR_RequestAdapter(Activity activity)
        {
            _activity = activity;
            FillRequestList();

        }
        public SR_RequestAdapter(Activity activity,int oRequestID)
        {
            _activity = activity;
            FillRequestList(oRequestID);

        }
        public void Flush()
        {
            STSR_Droid.ORM.DBRequestRepository dbr = new STSR_Droid.ORM.DBRequestRepository();
            _RequestList.AddRange(dbr.Request(-1, RefPage));
            LastPage = dbr.LastPage;
        }
        void FillRequestList()
        {

            //TO DO
            STSR_Droid.ORM.DBRequestRepository dbr = new STSR_Droid.ORM.DBRequestRepository();
            _RequestList = dbr.Request();

        }
        void FillRequestList(int oRequestID)
        {
            try { 
            //TO DO
            STSR_Droid.ORM.DBRequestRepository dbr = new STSR_Droid.ORM.DBRequestRepository();
            _RequestList = dbr.Request(oRequestID);
            }
                catch (Exception e) {
                    string t = e.Message;
            }

        }

        public override int Count
        {
            get { return _RequestList.Count; }
        }

        public int Page { get; set; }

        public override Java.Lang.Object GetItem(int position)
        {
            // could wrap a Contact in a Java.Lang.Object
            // to return it here if needed
            return null;
        }

        public override long GetItemId(int position)
        {
            return _RequestList[position].RequestID;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            
            var view = convertView ?? _activity.LayoutInflater.Inflate(Resource.Layout.SR_RequestListItem, parent, false);
            var resRequestID = view.FindViewById<TextView>(Resource.Id.txtRequestID);
            var resDeptID = view.FindViewById<TextView>(Resource.Id.txtDeptID);
            var resCustomer = view.FindViewById<TextView>(Resource.Id.txtCustomer);
            var resSite = view.FindViewById<TextView>(Resource.Id.txtSite);
            var resCity = view.FindViewById<TextView>(Resource.Id.txtCity);
            var resState = view.FindViewById<TextView>(Resource.Id.txtState);
            var resStartDate = view.FindViewById<TextView>(Resource.Id.txtStartDate);
            var resEndDate = view.FindViewById<TextView>(Resource.Id.txtEndDate);
            var resStatus = view.FindViewById<TextView>(Resource.Id.txtStatus);
            var btnImage = view.FindViewById<ImageView>(Resource.Id.btnImage);
            //var resBranch = view.FindViewById<TextView>(Resource.Id.txtBranch);
            

            view.SetBackgroundColor(Color.White);

            resRequestID.Text = _RequestList[position].RequestID.ToString();
            resDeptID.Text = (_RequestList[position].DeptID!=null)?_RequestList[position].DeptID.ToString():"";
            resCustomer.Text = (_RequestList[position].CustName!=null)?_RequestList[position].CustName.ToString():"";
            resSite.Text = (_RequestList[position].SiteName!=null) ?_RequestList[position].SiteName.ToString():"";
            resCity.Text = (_RequestList[position].City != null) ?_RequestList[position].City.ToString():"";
            resState.Text = (_RequestList[position].State != null) ?_RequestList[position].State.ToString():"";
            resStartDate.Text = (_RequestList[position].StartDate != null) ?_RequestList[position].StartDate.ToString():"";
            resEndDate.Text = (_RequestList[position].EndDate != null) ?_RequestList[position].EndDate.ToString():"";
            resStatus.Text = (_RequestList[position].Status != null) ? _RequestList[position].Status.ToString() : "";
            //resBranch.Text = (_RequestList[position].DeptID != null) ? _RequestList[position].DeptID.ToString() : "";

            resRequestID.SetTextColor(Color.Red);
            resDeptID.SetTextColor(Color.Black);
            resCustomer.SetTextColor(Color.Black);
            resSite.SetTextColor(Color.Black);
            resCity.SetTextColor(Color.Black);
            resState.SetTextColor(Color.Black);
            resStartDate.SetTextColor(Color.Black);
            resEndDate.SetTextColor(Color.Black);
            resStatus.SetTextColor(Color.Black);

            btnImage = view.FindViewById<ImageView>(Resource.Id.btnImage);

            //if (_RequestList[position].Urgent) { btnImage.SetImageResource(Resource.Drawable.009_000); }


            btnImage.SetImageResource(GetResourceID(_RequestList[position]));
            //btnImage.SetImageResource(Resource.Drawable.logo);
            btnImage.Tag= _RequestList[position].RequestID.ToString();
            btnImage.Click += btnImage_Click;

            return view;
        }
         void btnImage_Click(object sender, EventArgs e)
        {
            if (sender != null)
            {
                ImageView chk = (ImageView)sender;
                string strIndex = chk.Tag.ToString();
                int intIndex = 0;
                int.TryParse(strIndex, out intIndex);
                OnClickItem(intIndex);
            }

        }


        private List<SR_Request> ConvertToModelList(SRWSServices.SR_Request[] oRec)
        {
            List<SR_Request> Rtn = new List<SR_Request>();
            foreach (var item in oRec)
            {
                SR_Request RNA = new SR_Request();
                RNA.RequestID = item.RequestID;

                RNA.DeptID = item.DeptID;
                // RNA.CustName = item.CustName;
                RNA.SiteName = item.SiteName;
                RNA.City = item.City;
                RNA.State = item.State;
                RNA.StartDate = item.StartDate;
                RNA.EndDate = item.EndDate;
                //RNA.Status = item.Status;

                Rtn.Add(RNA);

            }
            return Rtn;
        }

        public int GetResourceID(SR_Request oRec)
        {
            int Rtn = 0;
            //Resource.Drawable.099_000 c = new Resource.Drawable.099_000();
            //string Filename = "./" & oRec.CodeName & ".PNG";
            if (oRec.Urgent)
            {
                Rtn = Resource.Drawable.img099_000;
                goto Ending;
            }
            if (oRec.Status.ToLower() == "close")
            {
                Rtn = Resource.Drawable.img901_000;
                goto Ending;
            }
            switch (oRec.CodeName)
            {
                case "000_000":
                    Rtn = Resource.Drawable.img000_000;
                    break;
                case "001_000":
                    Rtn = Resource.Drawable.img001_000;
                    break;
                case "002_000":
                    Rtn = Resource.Drawable.img002_000;
                    break;
                case "003_000":
                    Rtn = Resource.Drawable.img003_000;
                    break;
                case "004_003":
                    Rtn = Resource.Drawable.img004_003;
                    break;
                case "005_000":
                    Rtn = Resource.Drawable.img005_000;
                    break;
                case "006_000":
                    Rtn = Resource.Drawable.img006_000;
                    break;
                case "007_000":
                    Rtn = Resource.Drawable.img007_000;
                    break;
                case "007_003":
                    Rtn = Resource.Drawable.img007_003;
                    break;
                case "008_003":
                    Rtn = Resource.Drawable.img008_003;
                    break;
                case "009_000":
                    Rtn = Resource.Drawable.img009_000;
                    break;
                case "010_000":
                    Rtn = Resource.Drawable.img010_000;
                    break;
                case "900_000":
                    Rtn = Resource.Drawable.img900_000;
                    break;
                case "902_000":
                    Rtn = Resource.Drawable.img902_000;
                    break;
            }
            Ending:
            return Rtn;
        }

    }
}
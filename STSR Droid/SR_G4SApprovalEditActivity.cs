using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using STSR_Droid.ORM;
using STSR_Droid.Class;
using Android.Content.PM;
using Android.Graphics;

namespace STSR_Droid
{
    [Activity(Label = "SR_G4SApprovalEditActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SR_G4SApprovalEditActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here
            SetContentView(Resource.Layout.SR_G4SApprovalEdit);

            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            SlidingTabFragment fragment = new SlidingTabFragment();
            fragment.ModuleType = "G4SApproval";
            transaction.Replace(Resource.Id.sample_content_fragment, fragment);
            transaction.Commit();

        }
    }
}
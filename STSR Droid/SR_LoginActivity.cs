﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Android.OS;
using STSR_Droid.Views;
using STSR_Droid.Models;
using STSR_Droid.ORM;
using Android.Graphics;
using Android.Content.PM;
using STSR_Droid.SQLiteTables;
//using Android.Support.V7.App;
using Android.Views.InputMethods;
using Android.Content.Res;



namespace STSR_Droid
{
    [Activity(Label = "G4SNow", MainLauncher = true, ScreenOrientation = ScreenOrientation.Portrait)]
    public class SR_LoginActivity : Activity
    {
        private int iHeightDefault;
        private LinearLayout rootView;

        private int Layoutposhight = 1200;
        private int Layoutposlow = 850;

        protected override void OnCreate(Bundle bundle)
        {

            base.OnCreate(bundle);
            if (ReadEULA())
            {

                // Set our view from the "main" layout resource
                SetContentView(Resource.Layout.SR_Login);

                // Detects the Login components
                Button btnLoging = FindViewById<Button>(Resource.Id.btnLogin);
                Button btnClear = FindViewById<Button>(Resource.Id.btnClear);
                EditText txtUserID = FindViewById<EditText>(Resource.Id.txtUserID);
                EditText txtPassword = FindViewById<EditText>(Resource.Id.txtPassword);
                LinearLayout LLMainView = FindViewById<LinearLayout>(Resource.Id.MainView);
                RelativeLayout RLDefaultID = FindViewById<RelativeLayout>(Resource.Id.DefaultView);

                int Layoutpos = RLDefaultID.Top;
                View MainView = FindViewById(Resource.Id.MainView);

                MainView.SetBackgroundColor(Color.White);
                btnLoging.SetTextColor(Color.Black);
                txtUserID.SetTextColor(Color.Black);
                txtPassword.SetTextColor(Color.Black);


                //What?
                btnLoging.Click += btnLogin_Click;
                btnClear.Click += btnClear_Click;

                //===============================================================================
                rootView = FindViewById<LinearLayout>(Resource.Id.MainView);

                Rect r = new Rect();
                View view = Window.DecorView;
                view.GetWindowVisibleDisplayFrame(r);
                iHeightDefault = r.Bottom;

                rootView.ViewTreeObserver.GlobalLayout += (sender, e) => {
                    Rect ir = new Rect();
                    View iview = Window.DecorView;
                    iview.GetWindowVisibleDisplayFrame(ir);

                    if (iHeightDefault > ir.Bottom)
                    {
                        // Keyboard is Show
                        RLDefaultID.Top = Layoutposlow;
                    }
                    else
                    {
                        // Keyboard is Hidden
                        RLDefaultID.Top = Layoutposhight;
                    }
                };



            } else
            {


                SetContentView(Resource.Layout.SR_EULA);
                // Detects the Login components
                Button btnAccept = FindViewById<Button>(Resource.Id.btnAccept);
                Button btnCancel = FindViewById<Button>(Resource.Id.btnCancel);

                btnAccept.Click += btnAccept_Click;
                btnCancel.Click += btnCancel_Click;

                //===============================================================================
                //===============================================================================

            }

            
            

        }

        void btnAccept_Click(object sender, EventArgs e)
        {
            UserReadEULA();
            SetContentView(Resource.Layout.SR_Login);

            // Detects the Login components
            Button btnLoging = FindViewById<Button>(Resource.Id.btnLogin);
            Button btnClear = FindViewById<Button>(Resource.Id.btnClear);
            EditText txtUserID = FindViewById<EditText>(Resource.Id.txtUserID);
            EditText txtPassword = FindViewById<EditText>(Resource.Id.txtPassword);
            LinearLayout LLMainView = FindViewById<LinearLayout>(Resource.Id.MainView);
            RelativeLayout RLDefaultID = FindViewById<RelativeLayout>(Resource.Id.DefaultView);

            int Layoutpos = RLDefaultID.Top;
            View MainView = FindViewById(Resource.Id.MainView);

            MainView.SetBackgroundColor(Color.White);
            btnLoging.SetTextColor(Color.Black);
            txtUserID.SetTextColor(Color.Black);
            txtPassword.SetTextColor(Color.Black);


            //What?
            btnLoging.Click += btnLogin_Click;
            btnClear.Click += btnClear_Click;

            //===============================================================================
            rootView = FindViewById<LinearLayout>(Resource.Id.MainView);

            Rect r = new Rect();
            View view = Window.DecorView;
            view.GetWindowVisibleDisplayFrame(r);
            iHeightDefault = r.Bottom;

            rootView.ViewTreeObserver.GlobalLayout += (osender, oe) => {
                Rect ir = new Rect();
                View iview = Window.DecorView;
                iview.GetWindowVisibleDisplayFrame(ir);

                if (iHeightDefault > ir.Bottom)
                {
                    // Keyboard is Show
                    RLDefaultID.Top = Layoutposlow;
                }
                else
                {
                    // Keyboard is Hidden
                    RLDefaultID.Top = Layoutposhight;
                }
            };
        }
        void btnCancel_Click(object sender, EventArgs e)
        {
            Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        }
        void btnLogin_Click(object sender, EventArgs e) {
            DBLoginRepository dbr = new DBLoginRepository();
            EditText txtUserID = FindViewById<EditText>(Resource.Id.txtUserID);
            EditText txtPassword = FindViewById<EditText>(Resource.Id.txtPassword);
            var result = dbr.Login(txtUserID.Text, txtPassword.Text);
            if (result!=null) {
                switch (result)
                {
                    case "FIRSTIME":
                        {
                            StartActivity(typeof(SR_ViewTermActivity));
                            break;
                        }
                    case "GRANTED":
                        {
                            StartActivity(typeof(SR_DefaultActivity));
                            break;
                        }
                    default:
                        {
                            Toast.MakeText(this, result, ToastLength.Short).Show();
                            break;
                        }
                }
            }
            else {
                Toast.MakeText(this, "Login_Click: General Failure", ToastLength.Short).Show();
            }


                    

        }
        void btnClear_Click(object sender, EventArgs e)
        {
            EditText txtUserID = FindViewById<EditText>(Resource.Id.txtUserID);
            EditText txtPassword = FindViewById<EditText>(Resource.Id.txtPassword);

            txtPassword.Text = "";
            txtUserID.Text = "";    //This should return the focus to User


        }
        void UserReadEULA()
        {
            try
            {
                SR_Class.SR_System oSysSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSysSession.GetSession();

                rec.SR_EULA = true;
                oSysSession.SaveSession(rec);
            }
            catch (Exception e)
            {
                string t = e.Message;
            }



        }
        bool ReadEULA()
        {
            bool Rtn = false;

            try
            {
                SR_Class.SR_System oSysSession = new SR_Class.SR_System();
                TblSession rec = new TblSession();
                rec = oSysSession.GetSession();

                if (!rec.SR_EULA)
                {
                    oSysSession.Delete(rec);
                }
                //rec.SR_EULA = true;
                Rtn = rec.SR_EULA;

            }
            catch (Exception e)
            {
                string t = e.Message;
            }

            return Rtn;
        }
    }
}


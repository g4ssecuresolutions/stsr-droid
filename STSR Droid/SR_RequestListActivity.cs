using System;
using System.Collections;
using Android.Text;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Android.OS;
using STSR_Droid.Views;
using STSR_Droid.Models;
using STSR_Droid.ORM;
using STSR_Droid.Class;
using Android.Graphics;
using System.Collections.Generic;
using Android.Content.PM;

namespace STSR_Droid
{
    [Activity(Label = "SR_RequestListActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SR_RequestListActivity : Activity
    {

        public int Page = 1;
       
        public STSR_Droid.Adapter.SR_RequestAdapter RequestAdapter;
        protected override void OnCreate(Bundle bundle)
        {
            try { 
                base.OnCreate(bundle);
                SetContentView(Resource.Layout.SR_RequestList);
                View DefaultView = FindViewById<View>(Resource.Id.RequestView);
                Button btnNew = FindViewById<Button>(Resource.Id.btnNew);
                Button btnRequest = FindViewById<Button>(Resource.Id.btnList);
                Button btnSearch = FindViewById<Button>(Resource.Id.btnSearch);

                btnNew.Click += btnNew_Click;
                btnRequest.Click += btnRequest_Click;
                btnSearch.Click += btnSearch_Click;

                //Populate the list
                DefaultView.SetBackgroundColor(Color.White);
                RequestAdapter = new Adapter.SR_RequestAdapter(this);
                var RequestListView = FindViewById<ListView>(Resource.Id.lstRequest);
                var tvheader = FindViewById<TextView>(Resource.Id.tv_header);

                tvheader.SetTextColor(Color.Black);
                RequestListView.Adapter = RequestAdapter;
                RequestAdapter.OnClickItem += RequestAdapter_OnClickItem;



            }
            catch (Exception e)
            {
                string t = e.Message;
            }
        }

        void btnNew_Click(object sender, EventArgs e)
        {

            STSR_Droid.Models.SR_Request oRequest = new STSR_Droid.Models.SR_Request();
            SetContentView(Resource.Layout.SR_RequestEdit);
            //DBRequestRepository dbr = new DBRequestRepository();

            //List<SR_Request> lstRequest = new List<SR_Request>();
            //var vData = dbr.SR_GetRequestbyID(oIndex.ToString());

            //foreach (var item in vData)
            //{

            oRequest.RequestID = 0;
            oRequest.RequestStatusID = 0;
            oRequest.Status = "New Request";
            //    //oRequest.Urgent = item.Urgent;
            //}

            TextView lblRequestID = FindViewById<TextView>(Resource.Id.lblRequestID);
            TextView txtRequestID = FindViewById<TextView>(Resource.Id.txtRequestID);
            TextView lblStatus = FindViewById<TextView>(Resource.Id.lblStatus);
            TextView txtStatus = FindViewById<TextView>(Resource.Id.txtStatus);
            TextView lblBranch = FindViewById<TextView>(Resource.Id.lblBranch);
            TextView txtBranch = FindViewById<TextView>(Resource.Id.txtBranch);
            ImageView imgPriority = FindViewById<ImageView>(Resource.Id.imgPriority);

            txtRequestID.SetTextColor(Color.Red);
            txtStatus.SetTextColor(Color.Red);
            txtBranch.SetTextColor(Color.Red);
            lblRequestID.SetTextColor(Color.Black);
            lblStatus.SetTextColor(Color.Black);
            lblBranch.SetTextColor(Color.Black);

            //int RequestID = CInt(txtRequestID.Tag);

            txtRequestID.Text = "Request ID: New";
            txtRequestID.Tag = "NEW";
            txtStatus.Text = oRequest.Status;
            //StartActivity(typeof(SR_RequestEditActivity));

            if (oRequest.Urgent)
            {
                imgPriority.Visibility = ViewStates.Visible;
            }
            else
            {
                imgPriority.Visibility = ViewStates.Invisible;
            }
            

            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            SlidingTabFragment fragment = new SlidingTabFragment();
            fragment.RequestID = "New";
            fragment.ModuleType = "RequestApproval";
            fragment.SR_Session.SR_Action = "New";
            transaction.Replace(Resource.Id.sample_content_fragment, fragment);
            transaction.Commit();

        }
        void btnRequest_Click(object sender, EventArgs e)
        {
            
            Page = Page + 1;
            RequestAdapter.RefPage=Page;
            RequestAdapter.Flush();
            if (RequestAdapter.LastPage)
            {
                Button btnRequest = FindViewById<Button>(Resource.Id.btnList);
                btnRequest.Text = "Download completed";
                btnRequest.Enabled = false;
            }
            //var RequestAdapter = new Adapter.SR_RequestAdapter(this);
            var RequestListView = FindViewById<ListView>(Resource.Id.lstRequest);
            var tvheader = FindViewById<TextView>(Resource.Id.tv_header);

            tvheader.SetTextColor(Color.Black);
            RequestListView.Adapter = RequestAdapter;
            RequestAdapter.OnClickItem += RequestAdapter_OnClickItem;

            if (!RequestAdapter.LastPage) { RequestListView.SmoothScrollToPosition((Page - 1) * 25); }
        }
        void ReloadList(int oPage)
        {
            //var RequestAdapter = new Adapter.SR_RequestAdapter(this);
            RequestAdapter.Page = oPage;
            var RequestListView = FindViewById<ListView>(Resource.Id.lstRequest);
            var tvheader = FindViewById<TextView>(Resource.Id.tv_header);

            tvheader.SetTextColor(Color.Black);
            RequestListView.Adapter = RequestAdapter;
            RequestAdapter.OnClickItem += RequestAdapter_OnClickItem;
        }

        void btnSearch_Click(object sender, EventArgs e)
        {
            try {
                ListView RequestListView = FindViewById<ListView>(Resource.Id.lstRequest);
                EditText RequestID = FindViewById<EditText>(Resource.Id.txtSearchID);
                TextView tvheader = FindViewById<TextView>(Resource.Id.tv_header);
                int intRequestID;
                int.TryParse(RequestID.Text.ToString(), out intRequestID);
                var RequestAdapter = new Adapter.SR_RequestAdapter(this, intRequestID);

                tvheader.SetTextColor(Color.Black);
                RequestListView.Adapter = RequestAdapter;
                RequestAdapter.OnClickItem += RequestAdapter_OnClickItem;
            }
            catch (Exception re)
            {
                string t = re.Message;
            }
        }
        void RequestAdapter_OnClickItem(int oIndex)
        {
            STSR_Droid.Models.SR_Request oRequest = new STSR_Droid.Models.SR_Request();
            SetContentView(Resource.Layout.SR_RequestEdit);
            DBRequestRepository dbr = new DBRequestRepository();

            List<SR_Request> lstRequest = new List<SR_Request>();

            List<SRWS_ASMX.SR_RequestItem> vData = dbr.SR_GetRequestbyID(oIndex.ToString());
            SRWS_ASMX.SR_RequestItem itemData = new SRWS_ASMX.SR_RequestItem();

            foreach (var item in vData)
            {
                oRequest.LoadModel(item);
                itemData = item;
                //oRequest.RequestID = item.RequestID;
                //oRequest.RequestStatusID = item.RequestStatusID;
                //oRequest.Status = item.Status;
                //oRequest.DeptID = item.DeptID;
                //oRequest.Urgent = item.Urgent;
            }

            TextView lblRequestID = FindViewById<TextView>(Resource.Id.lblRequestID);
            TextView txtRequestID = FindViewById<TextView>(Resource.Id.txtRequestID);
            TextView lblStatus = FindViewById<TextView>(Resource.Id.lblStatus);
            TextView txtStatus = FindViewById<TextView>(Resource.Id.txtStatus);
            TextView lblBranch = FindViewById<TextView>(Resource.Id.lblBranch);
            TextView txtBranch = FindViewById<TextView>(Resource.Id.txtBranch);
            ImageView imgPriority = FindViewById<ImageView>(Resource.Id.imgPriority);

            txtRequestID.SetTextColor(Color.Red);
            txtStatus.SetTextColor(Color.Red);
            txtBranch.SetTextColor(Color.Red);
            lblRequestID.SetTextColor(Color.Black);
            lblStatus.SetTextColor(Color.Black);
            lblBranch.SetTextColor(Color.Black);

            //int RequestID = CInt(txtRequestID.Tag);

            txtRequestID.Text = oIndex.ToString();
            txtRequestID.Tag = oIndex.ToString();
            txtStatus.Text = oRequest.Status;
            txtBranch.Text = oRequest.DeptID;

            //StartActivity(typeof(SR_RequestEditActivity));

            if (oRequest.Urgent)
            {
                imgPriority.Visibility = ViewStates.Visible;
            }
            else
            {
                imgPriority.Visibility = ViewStates.Invisible;
            }

            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            SlidingTabFragment fragment = new SlidingTabFragment();
            fragment.RequestID = oIndex.ToString();
            fragment.ModuleType = "RequestApproval";
            fragment.SR_Session.SR_Action = "Edit";
            fragment.mod_PortalRequest = itemData;
            transaction.Replace(Resource.Id.sample_content_fragment, fragment);
            transaction.Commit();


        }

    }

}


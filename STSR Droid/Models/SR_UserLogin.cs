using System;

namespace STSR_Droid.Models
{
    public class SR_UserLogin
    {
        public string SR_UserID { get; set; }
        public string SR_RoleID { get; set; }
        public string SR_FullName { get; set; }
        public string NuPassword { get; set; }
        public string WFApprover { get; set; }
        public string WFExtender { get; set; }
        public string ClientApprover { get; set; }
        public string ClientExtender { get; set; }
        public string CorpCustID { get; set; }
        public string ResetPassword { get; set; }
    }
}
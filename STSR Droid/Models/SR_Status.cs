using System;

namespace STSR_Droid.Models
{
    public class SR_Status
    {
        public int StatusID { get; set; }
        public string StatusName { get; set; }
    }
}
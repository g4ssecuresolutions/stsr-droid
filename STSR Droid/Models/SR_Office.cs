using System;

namespace STSR_Droid.Models
{
    public class SR_Office
    {
        public string DeptID { get; set; }
        public string Descr { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string TimeZone { get; set; }
        public string UserID_01 { get; set; }
        public string UserID_02 { get; set; }
        public string UserID_03 { get; set; }
        public string UserID_04 { get; set; }
    }
}
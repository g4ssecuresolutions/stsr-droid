using System;

namespace STSR_Droid.Models
{
    public class SR_LookUp
    {
        public string Index { get; set; }
        public string Value { get; set; }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace STSR_Droid.Models
{
    public class SR_SaveWorkflowApprove
    {
        public int SR_RequestID { get; set; }
        public int UserID { get; set; }
        public int RequestStatusID { get; set; }
        public bool RequestApproved { get; set; }
        public bool RequestDeny { get; set; }
        public bool RequestQuestioned { get; set; }
        public string Notes { get; set; }
        public double RequestRate { get; set; }
        public bool AddlCharges { get; set; }
        public bool AppdCharges { get; set; }
        public bool DeniedCharges { get; set; }
        public bool ChargesChanged { get; set; }
        public string DeptID { get; set; }
        public string DeniedReason { get; set; }


        //Charges section
        public double HotelAmount { get; set; }
        public double PerDiemAmount { get; set; }
        public double MileageAmount { get; set; }
        public double CarRentalAmount { get; set; }
        public double OtherAmount { get; set; }

        public string PONumber { get; set; }
        public string CostCenterCode { get; set; }
        public string WorkOrderNumber { get; set; }


        public SRWSServices.SR_SaveWorkflowApprove ToSRWSServices()
        {
            SRWSServices.SR_SaveWorkflowApprove Rtn = new SRWSServices.SR_SaveWorkflowApprove();

            Rtn.SR_RequestID = this.SR_RequestID;
            Rtn.UserID = this.UserID;
            Rtn.RequestStatusID = this.RequestStatusID;
            Rtn.RequestApproved = this.RequestApproved;
            Rtn.RequestDeny = this.RequestDeny;
            Rtn.RequestQuestioned = this.RequestQuestioned;
            Rtn.Notes = this.Notes;
            Rtn.RequestRate = this.RequestRate;
            Rtn.AddlCharges = this.AddlCharges;
            Rtn.AppdCharges = this.AppdCharges;
            Rtn.DeniedCharges = this.DeniedCharges;
            Rtn.ChargesChanged = this.ChargesChanged;
            Rtn.DeptID = this.DeptID;
            Rtn.DeniedReason = this.DeniedReason;


            //Charges section                  arges section
            Rtn.HotelAmount = this.HotelAmount;
            Rtn.PerDiemAmount = this.PerDiemAmount;
            Rtn.MileageAmount = this.MileageAmount;
            Rtn.CarRentalAmount = this.CarRentalAmount;
            Rtn.OtherAmount = this.OtherAmount;

            //New Fields
            Rtn.PONumber = this.PONumber;
            Rtn.CostCenterCode = this.CostCenterCode;
            Rtn.WorkOrderNumber = this.WorkOrderNumber;

            return Rtn;
        }
    }
}
using System;

namespace STSR_Droid.Models
{
    public class SR_SiteAddress
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Contact { get; set; }
        public string ContactPhone { get; set; }
        public string ContactDepartment { get; set; }
        public string DeptID { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
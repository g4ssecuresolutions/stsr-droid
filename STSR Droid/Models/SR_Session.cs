using System;
using SRWS_ASMX;

namespace STSR_Droid.Models
{
    public enum StatusID{
        New = 1,
        Open = 2,
	    Cancelled = 3,
	    Submitted = 4,
	    Pending = 5,
	    CustDenied = 6,
	    Denied = 7,
	    CustAppd = 8,
	    Approved = 9,
	    Terminated_Early = 10,
	    Extended = 11,
	    CustExtAppd = 12,
	    G4SExtAppd = 13,
	    Submitted_Updated = 14,
	    Approved_Updated = 15,
	    Approved_GuardAssigned = 16,
	    Approved_Updated_GuardAssigned = 17,
	    Pending_Hold = 18
    }
    public class SR_Session
    {
        public string DataLog { get; set; }
        public string Datatable { get; set; }
        public string FileName { get; set; }
        public string MapAddress { get; set; }
        public string oFileObj { get; set; }
        public string Redirect_Message { get; set; }
        public string Redirect_Page { get; set; }
        public string SelectedUserID { get; set; }
        public string ServiceRequestID { get; set; }
        public string SR_Action { get; set; }
        public string SR_Admin { get; set; }
        public string SR_Approve { get; set; }
        public string SR_CustApprove { get; set; }
        public string SR_CustExtend { get; set; }
        public string SR_DeptID { get; set; }
        public string SR_Exempt { get; set; }
        public string SR_Extend { get; set; }
        public string SR_Filter1 { get; set; }
        public string SR_GroupID { get; set; }
        public string SR_L4 { get; set; }
        public string SR_LoggedIn { get; set; }
        public string SR_LoggedMsg { get; set; }
        public string SR_Manager { get; set; }
        public string SR_NotifyBranch { get; set; }
        public string SR_QAS_Mode { get; set; }
        public string SR_Redirection { get; set; }
        public string SR_RoleID { get; set; }
        public string SR_UserCustAccess { get; set; }
        public string SR_UserFullName { get; set; }
        public string SR_UserID { get; set; }
        public string SR_UserName { get; set; }
        public string SR_Status { get; set; }

        public bool SR_EULA { get; set; }

        public SRWS_ASMX.SR_Session ToSRWS_ASMX()
        {
            SRWS_ASMX.SR_Session Rtn = new SRWS_ASMX.SR_Session();

            Rtn.DataLog = this.DataLog;
            Rtn.Datatable = this.Datatable;
            Rtn.FileName = this.FileName;
            Rtn.MapAddress = this.MapAddress;
            Rtn.oFileObj = this.oFileObj;
            Rtn.Redirect_Message = this.Redirect_Message;
            Rtn.Redirect_Page = this.Redirect_Page;
            Rtn.SelectedUserID = this.SelectedUserID;
            Rtn.ServiceRequestID = this.ServiceRequestID;
            Rtn.SR_Action = this.SR_Action;
            Rtn.SR_Admin = this.SR_Admin;
            Rtn.SR_Approve = this.SR_Approve;
            Rtn.SR_CustApprove = this.SR_CustApprove;
            Rtn.SR_CustExtend = this.SR_CustExtend;
            Rtn.SR_DeptID = this.SR_DeptID;
            Rtn.SR_Exempt = this.SR_Exempt;
            Rtn.SR_Extend = this.SR_Extend;
            Rtn.SR_Filter1 = this.SR_Filter1;
            Rtn.SR_GroupID = this.SR_GroupID;
            Rtn.SR_L4 = this.SR_L4;
            Rtn.SR_LoggedIn = this.SR_LoggedIn;
            Rtn.SR_LoggedMsg = this.SR_LoggedMsg;
            Rtn.SR_Manager = this.SR_Manager;
            Rtn.SR_NotifyBranch = this.SR_NotifyBranch;
            Rtn.SR_QAS_Mode = this.SR_QAS_Mode;
            Rtn.SR_Redirection = this.SR_Redirection;
            Rtn.SR_RoleID = this.SR_RoleID;
            Rtn.SR_UserCustAccess = this.SR_UserCustAccess;
            Rtn.SR_UserFullName = this.SR_UserFullName;
            Rtn.SR_UserID = this.SR_UserID;
            Rtn.SR_UserName = this.SR_UserName;
            Rtn.SR_Status = this.SR_Status;
            Rtn.SR_EULA = this.SR_EULA;
            return Rtn;
        }
        public SRWSServices.SR_Session ToSRWSServices()
        {
            SRWSServices.SR_Session Rtn = new SRWSServices.SR_Session();

            Rtn.DataLog = this.DataLog;
            Rtn.Datatable = this.Datatable;
            Rtn.FileName = this.FileName;
            Rtn.MapAddress = this.MapAddress;
            Rtn.oFileObj = this.oFileObj;
            Rtn.Redirect_Message = this.Redirect_Message;
            Rtn.Redirect_Page = this.Redirect_Page;
            Rtn.SelectedUserID = this.SelectedUserID;
            Rtn.ServiceRequestID = this.ServiceRequestID;
            Rtn.SR_Action = this.SR_Action;
            Rtn.SR_Admin = this.SR_Admin;
            Rtn.SR_Approve = this.SR_Approve;
            Rtn.SR_CustApprove = this.SR_CustApprove;
            Rtn.SR_CustExtend = this.SR_CustExtend;
            Rtn.SR_DeptID = this.SR_DeptID;
            Rtn.SR_Exempt = this.SR_Exempt;
            Rtn.SR_Extend = this.SR_Extend;
            Rtn.SR_Filter1 = this.SR_Filter1;
            Rtn.SR_GroupID = this.SR_GroupID;
            Rtn.SR_L4 = this.SR_L4;
            Rtn.SR_LoggedIn = this.SR_LoggedIn;
            Rtn.SR_LoggedMsg = this.SR_LoggedMsg;
            Rtn.SR_Manager = this.SR_Manager;
            Rtn.SR_NotifyBranch = this.SR_NotifyBranch;
            Rtn.SR_QAS_Mode = this.SR_QAS_Mode;
            Rtn.SR_Redirection = this.SR_Redirection;
            Rtn.SR_RoleID = this.SR_RoleID;
            Rtn.SR_UserCustAccess = this.SR_UserCustAccess;
            Rtn.SR_UserFullName = this.SR_UserFullName;
            Rtn.SR_UserID = this.SR_UserID;
            Rtn.SR_UserName = this.SR_UserName;
            Rtn.SR_Status = this.SR_Status;
            return Rtn;
        }
    }


}
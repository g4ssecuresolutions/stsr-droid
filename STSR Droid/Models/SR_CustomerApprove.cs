using System;
using System.Collections.Generic;

namespace STSR_Droid.Models
{
    public class SR_CustomerApprove
    {
        public int SR_RequestID { get; set; }
        public string DeptID { get; set; }
        public string CorpCustID { get; set; }
        public string CustNotes { get; set; }
        public string CustName { get; set; }
        public string DisplayName { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Contact { get; set; }
        public string OfficerType { get; set; }
        public int OfficerNum { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        //public string SiteName { get; set; }
        public string Status { get; set; }
        public int RequestStatusID { get; set; }
        public string Notes { get; set; }
        public double RequestRate { get; set; }
        public double RequestBill { get; set; }
        public bool AddlCharges { get; set; }
        public bool AppdCharges { get; set; }
        public bool DeniedCharges { get; set; }
        public bool ShowApprove { get; set; }
        public bool CtrlEnabled { get; set; }
        public bool EnableCtrlDenied { get; set; }
        public bool EnableCtrlAppd { get; set; }
        public bool EnableCtrlAddl { get; set; }
        public bool EnableCtrlApprove { get; set; }
        public bool QuestCharges { get; set; }

        // Charges section
        public double HotelAmount { get; set; }
        public double PerDiemAmount { get; set; }
        public double MileageAmount { get; set; }
        public double CarRentalAmount { get; set; }
        public double OtherAmount { get; set; }

        public void LoadModel(List<SRWS_ASMX.SR_CustomerApprove> oG4SApprove)
        {
            if (oG4SApprove == null)
            {
                this.SR_RequestID = 0;
                this.DeptID = "";
                this.CorpCustID = "";
                this.CustNotes = "";
                this.CustName = "";
                this.DisplayName = "";
                this.Address1 = "";
                this.City = "";
                this.State = "";
                this.Contact = "";
                this.OfficerType = "";
                this.OfficerNum = 0;
                this.StartDate = Convert.ToDateTime("");
                this.EndDate = Convert.ToDateTime("");
                //this.SiteName = "";
                this.Status = "";
                this.RequestStatusID = 0;
                this.Notes = "";
                this.RequestRate = 0;
                this.RequestBill = 0;
                this.AddlCharges = false;
                this.AppdCharges = false;
                this.DeniedCharges = false;
                this.ShowApprove = false;
                this.CtrlEnabled = false;
                this.EnableCtrlDenied = false;
                this.EnableCtrlAppd = false;
                this.EnableCtrlAddl = false;
                this.EnableCtrlApprove = false;
                this.QuestCharges = false;
                // Charges section
                this.HotelAmount = 0;
                this.PerDiemAmount = 0;
                this.MileageAmount = 0;
                this.CarRentalAmount = 0;
                this.OtherAmount = 0;
            }
            else
            {
                Boolean n = true;
                foreach (SRWS_ASMX.SR_CustomerApprove item in oG4SApprove)
                {
                    if (n)
                    {
                        this.SR_RequestID = item.SR_RequestID;
                        this.DeptID = item.DeptID;
                        this.CorpCustID = item.CorpCustID;
                        this.CustNotes = item.CustNotes;
                        this.CustName = item.CustName;
                        this.DisplayName = item.DisplayName;
                        this.Address1 = item.Address1;
                        this.City = item.City;
                        this.State = item.State;
                        this.Contact = item.Contact;
                        this.OfficerType = item.OfficerType;
                        this.OfficerNum = Convert.ToInt32(item.OfficerNum);
                        this.StartDate = Convert.ToDateTime(item.StartDate);
                        this.EndDate = Convert.ToDateTime(item.EndDate);
                        //this.CustName = item.CustName;
                        this.Status = item.Status;
                        this.RequestStatusID = item.RequestStatusID;
                        this.Notes = item.Notes;
                        this.RequestRate = item.RequestRate;
                        //this.RequestBill = item.re
                        this.AddlCharges = item.AddlCharges;
                        this.AppdCharges = item.AppdCharges;
                        this.DeniedCharges = item.DeniedCharges;
                        this.ShowApprove = (item.ShowApprove == "true") ? true : false;
                        this.CtrlEnabled = (item.CtrlEnabled == "true") ? true : false;
                        this.EnableCtrlDenied = item.EnableCtrlDenied;
                        this.EnableCtrlAppd = item.EnableCtrlAppd;
                        this.EnableCtrlAddl = item.EnableCtrlAddl;
                        this.EnableCtrlApprove = item.EnableCtrlApprove;

                        n = false;

                    }

                    // Charges section
                    switch (item.Code)
                    {
                        case "1":  // Hotel
                            this.HotelAmount = item.Amount;
                            break;
                        case "2":  // Per Diem
                            this.PerDiemAmount = item.Amount;
                            break;
                        case "3":  // Mileage
                            this.MileageAmount = item.Amount;
                            break;
                        case "4":  // Car Rental
                            this.CarRentalAmount = item.Amount;
                            break;
                        case "99":  // Other
                            this.OtherAmount = item.Amount;
                            break;
                    }                    
                }
            }

        }

        // Used to save, delete Records
        public SRWSServices.SR_G4SApprove ToSRWS_ASMX()
        {
            SRWSServices.SR_G4SApprove Rtn = new SRWSServices.SR_G4SApprove();
            Rtn.SR_RequestID = this.SR_RequestID;
            Rtn.DeptID = this.DeptID;
            Rtn.CorpCustID = this.CorpCustID;
            Rtn.CustNotes = this.CustNotes;
            Rtn.CustName = this.CustName;
            Rtn.DisplayName = this.DisplayName;
            Rtn.Address1 = this.Address1;
            Rtn.City = this.City;
            Rtn.State = this.State;
            Rtn.Contact = this.Contact;
            Rtn.OfficerType = this.OfficerType;
            Rtn.OfficerNum = this.OfficerNum.ToString();
            Rtn.StartDate = this.StartDate.ToString();
            Rtn.EndDate = this.EndDate.ToString();
            Rtn.Status = this.Status;
            Rtn.RequestStatusID = this.RequestStatusID;
            Rtn.Notes = this.Notes;
            Rtn.RequestRate = this.RequestRate;
            //Rtn.RequestBill = this.RequestBill;
            Rtn.AddlCharges = this.AddlCharges;
            Rtn.AppdCharges = this.AppdCharges;
            Rtn.DeniedCharges = this.DeniedCharges;
            Rtn.ShowApprove = (this.ShowApprove)? "true" : "false";
            Rtn.CtrlEnabled = (this.CtrlEnabled)? "true" : "false";
            Rtn.EnableCtrlDenied = this.EnableCtrlDenied;
            Rtn.EnableCtrlAppd = this.EnableCtrlAppd;
            Rtn.EnableCtrlAddl = this.EnableCtrlAddl;
            Rtn.EnableCtrlApprove = this.EnableCtrlApprove;
            //// Charges section
            //Rtn.Code = this.Code;
            //Rtn.Approved = this.Approved;
            //Rtn.Denied = this.Denied;
            //Rtn.Questioned = this.Questioned;
            //Rtn.Approvedby = this.Approvedby;
            //Rtn.Deniedby = this.Deniedby;
            //Rtn.Questionedby = this.Questionedby;
            //Rtn.Description = this.Description;
            //Rtn.Amount = this.Amount;

            return Rtn;
        }
    }
}
using System;
using System.Data;
using System.IO;
using SQLite;
////using System.Web.Services;
using SR_Class;
using STSR_Droid;
using STSR_Droid.SQLiteTables;
//using STSR_Droid.SRWSServices;

namespace STSR_Droid.Models
{
    public class SR_G4SCharges
    {
        public string mCode { get; set; }
        public bool mApproved { get; set; }
        public bool mDenied { get; set; }
        public bool mQuestioned { get; set; }
        public int mApprovedby { get; set; }
        public int mDeniedby { get; set; }
        public int mQuestionedby { get; set; }
        public int mDescription { get; set; }
        public double mAmount { get; set; }

    }
}
using System;
using System.Collections.Generic;

namespace STSR_Droid.Models
{
    public class SR_G4SApprove
    {
        public int SR_RequestID { get; set; }
        public string DeptID { get; set; }
        public string CorpCustID { get; set; }
        public string CustNotes { get; set; }
        public string CustName { get; set; }
        public string DisplayName { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Contact { get; set; }
        public string OfficerType { get; set; }
        public string OfficerNum { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartDate2 { get; set; }
        public string EndDate2 { get; set; }
        public string Status { get; set; }
        public int RequestStatusID { get; set; }
        public string Notes { get; set; }
        public double RequestRate { get; set; }
        public bool AddlCharges { get; set; }
        public bool AppdCharges { get; set; }
        public bool DeniedCharges { get; set; }
        public string ShowApprove { get; set; }
        public string CtrlEnabled { get; set; }
        public bool EnableCtrlDenied { get; set; }
        public bool EnableCtrlAppd { get; set; }
        public bool EnableCtrlAddl { get; set; }
        public bool EnableCtrlApprove { get; set; }
        public double TotalAdditionalAmount { get; set; }
        public int ReasonDenyID { get; set; }
        public bool IsGuard { get; set; }
        public bool Approved { get; set; }
        public bool Denied { get; set; }

        // Charges section
        public double HotelAmount { get; set; }
        public double PerDiemAmount { get; set; }
        public double MileageAmount { get; set; }
        public double CarRentalAmount { get; set; }
        public double OtherAmount { get; set; }

        //New Fields
        public string PONumber { get; set; }
        public string CostCenterCode { get; set; }
        public string WorkOrderNumber { get; set; }

        public SR_WorkFlowApprove ToWorkFlowApprove() {
            SR_WorkFlowApprove Rtn = new SR_WorkFlowApprove();


            Rtn.SR_RequestID = this.SR_RequestID;
            Rtn.DeptID = this.DeptID;
            Rtn.CorpCustID = this.CorpCustID;
            Rtn.CustNotes = this.CustNotes;
            Rtn.CustName = this.CustName;
            Rtn.DisplayName = this.DisplayName;
            Rtn.Address1 = this.Address1;
            Rtn.City = this.City;
            Rtn.State = this.State;
            Rtn.Contact = this.Contact;
            Rtn.OfficerType = this.OfficerType;
            Rtn.OfficerNum = (string.IsNullOrEmpty(this.OfficerNum))?0:Convert.ToInt32(this.OfficerNum);
            Rtn.StartDate = Convert.ToDateTime(this.StartDate);
            Rtn.EndDate = Convert.ToDateTime(this.EndDate);
            //Rtn.SiteName = this.SiteName;
            Rtn.Status = this.Status;
            Rtn.RequestStatusID = this.RequestStatusID;
            Rtn.Notes = this.Notes;
            Rtn.RequestRate = this.RequestRate;
            //Rtn.RequestBill = this.RequestBill;
            Rtn.AddlCharges = this.AddlCharges;
            Rtn.AppdCharges = this.AppdCharges;
            Rtn.DeniedCharges = this.DeniedCharges;
            //Rtn.ShowApprove = this.ShowApprove;
            //Rtn.CtrlEnabled = this.CtrlEnabled;
            Rtn.EnableCtrlDenied = this.EnableCtrlDenied;
            Rtn.EnableCtrlAppd = this.EnableCtrlAppd;
            Rtn.EnableCtrlAddl = this.EnableCtrlAddl;
            Rtn.EnableCtrlApprove = this.EnableCtrlApprove;
            //Rtn.QuestCharges = this.QuestCharges;
            Rtn.Approved = this.Approved;
            Rtn.Denied = this.Denied;
            

            Rtn.HotelAmount = this.HotelAmount;
            Rtn.PerDiemAmount = this.PerDiemAmount;
            Rtn.MileageAmount = this.MileageAmount;
            Rtn.CarRentalAmount = this.CarRentalAmount;
            Rtn.OtherAmount = this.OtherAmount;

            Rtn.PONumber = this.PONumber;
            Rtn.CostCenterCode = this.CostCenterCode;
            Rtn.WorkOrderNumber = this.WorkOrderNumber;

            return Rtn;
        }
        public SR_SaveWorkflowApprove ToSaveWorkFlowApprove(Boolean IsChargeChanged)
        {
            SR_SaveWorkflowApprove Rtn = new SR_SaveWorkflowApprove();


            Rtn.SR_RequestID = this.SR_RequestID;
            //Rtn.UserID = this.
            Rtn.RequestStatusID = this.RequestStatusID;
            Rtn.RequestApproved = this.Approved;
            Rtn.RequestDeny = this.Denied;
            //Rtn.RequestQuestioned = IsQuestioned;
            Rtn.Notes = this.Notes;
            Rtn.RequestRate = this.RequestRate;
            Rtn.AddlCharges = this.AddlCharges;
            Rtn.AppdCharges = this.AppdCharges;
            Rtn.DeniedCharges = this.DeniedCharges;
            Rtn.ChargesChanged = IsChargeChanged;
            Rtn.DeptID = this.DeptID;
            Rtn.DeniedReason = GetDeniedReasonbyID(this.ReasonDenyID);

            Rtn.HotelAmount = this.HotelAmount;
            Rtn.PerDiemAmount = this.PerDiemAmount;
            Rtn.MileageAmount = this.MileageAmount;
            Rtn.CarRentalAmount = this.CarRentalAmount;
            Rtn.OtherAmount = this.OtherAmount;

            Rtn.PONumber = this.PONumber;
            Rtn.CostCenterCode = this.CostCenterCode;
            Rtn.WorkOrderNumber = this.WorkOrderNumber;

            return Rtn;
        }
        public void LoadModel(List<SRWS_ASMX.SR_G4SApprove> oG4SApprove)
        {
            if (oG4SApprove == null)
            {
                this.SR_RequestID = 0;
                this.DeptID = "";
                this.CorpCustID = "";
                this.CustNotes = "";
                this.CustName = "";
                this.DisplayName = "";
                this.Address1 = "";
                this.City = "";
                this.State = "";
                this.Contact = "";
                this.OfficerType = "";
                this.OfficerNum = "";
                this.StartDate = "";
                this.EndDate = "";
                this.StartDate2 = "";
                this.EndDate2 = "";
                this.Status = "";
                this.RequestStatusID = 0;
                this.Notes = "";
                this.RequestRate = 0;
                this.AddlCharges = false;
                this.AppdCharges = false;
                this.DeniedCharges = false;
                this.ShowApprove = "";
                this.CtrlEnabled = "";
                this.EnableCtrlDenied = false;
                this.EnableCtrlAppd = false;
                this.EnableCtrlAddl = false;
                this.EnableCtrlApprove = false;
                this.TotalAdditionalAmount = 0;
                this.ReasonDenyID = 0;
                this.IsGuard = false;

                // Charges section
                this.HotelAmount = 0;
                this.PerDiemAmount = 0;
                this.MileageAmount = 0;
                this.CarRentalAmount = 0;
                this.OtherAmount = 0;
                this.PONumber = "";
                this.CostCenterCode = "";
                this.WorkOrderNumber = "";

            }
            else
            {
                Boolean n = true;
                foreach (SRWS_ASMX.SR_G4SApprove item in oG4SApprove)
                {
                    if (n)
                    {
                        this.SR_RequestID = item.SR_RequestID;
                        this.DeptID = item.DeptID;
                        this.CorpCustID = item.CorpCustID;
                        this.CustNotes = item.CustNotes;
                        this.CustName = item.CustName;
                        this.DisplayName = item.DisplayName;
                        this.Address1 = item.Address1;
                        this.City = item.City;
                        this.State = item.State;
                        this.Contact = item.Contact;
                        this.OfficerType = item.OfficerType;
                        this.OfficerNum = item.OfficerNum;
                        this.StartDate = item.StartDate;
                        this.EndDate = item.EndDate;
                        this.StartDate2 = item.StartDate2;
                        this.EndDate2 = item.EndDate2;
                        this.Status = item.Status;
                        this.RequestStatusID = item.RequestStatusID;
                        this.Notes = item.Notes;
                        this.RequestRate = item.RequestRate;
                        this.AddlCharges = item.AddlCharges;
                        this.AppdCharges = item.AppdCharges;
                        this.DeniedCharges = item.DeniedCharges;
                        this.ShowApprove = item.ShowApprove;
                        this.CtrlEnabled = item.CtrlEnabled;
                        this.EnableCtrlDenied = item.EnableCtrlDenied;
                        this.EnableCtrlAppd = item.EnableCtrlAppd;
                        this.EnableCtrlAddl = item.EnableCtrlAddl;
                        this.EnableCtrlApprove = item.EnableCtrlApprove;
                        this.TotalAdditionalAmount = item.TotalAdditionalAmount;
                        this.ReasonDenyID = 0;
                        this.IsGuard = item.IsGuard;

                        this.PONumber = item.PONumber;
                        this.CostCenterCode = item.CostCenterCode;
                        this.WorkOrderNumber = item.WorkOrderNumber;

                        n = false;
                    }
                    // Charges section
                    switch (item.Code)
                    {
                        case "1":  // Hotel
                            this.HotelAmount = item.Amount;
                            break;
                        case "2":  // Per Diem
                            this.PerDiemAmount = item.Amount;
                            break;
                        case "3":  // Mileage
                            this.MileageAmount = item.Amount;
                            break;
                        case "4":  // Car Rental
                            this.CarRentalAmount = item.Amount;
                            break;
                        case "99":  // Other
                            this.OtherAmount = item.Amount;
                            break;
                    }

                }


            }

        }

        // Used to save, delete Records
        public SRWSServices.SR_G4SApprove ToSRWS_ASMX()
        {
            SRWSServices.SR_G4SApprove Rtn = new SRWSServices.SR_G4SApprove();
            Rtn.SR_RequestID = this.SR_RequestID;
            Rtn.DeptID = this.DeptID;
            Rtn.CorpCustID = this.CorpCustID;
            Rtn.CustNotes = this.CustNotes;
            Rtn.CustName = this.CustName;
            Rtn.DisplayName = this.DisplayName;
            Rtn.Address1 = this.Address1;
            Rtn.City = this.City;
            Rtn.State = this.State;
            Rtn.Contact = this.Contact;
            Rtn.OfficerType = this.OfficerType;
            Rtn.OfficerNum = this.OfficerNum;
            Rtn.StartDate = this.StartDate;
            Rtn.EndDate = this.EndDate;
            Rtn.StartDate2 = this.StartDate2;
            Rtn.EndDate2 = this.EndDate2;
            Rtn.Status = this.Status;
            Rtn.RequestStatusID = this.RequestStatusID;
            Rtn.Notes = this.Notes;
            Rtn.RequestRate = this.RequestRate;
            Rtn.AddlCharges = this.AddlCharges;
            Rtn.AppdCharges = this.AppdCharges;
            Rtn.DeniedCharges = this.DeniedCharges;
            Rtn.ShowApprove = this.ShowApprove;
            Rtn.CtrlEnabled = this.CtrlEnabled;
            Rtn.EnableCtrlDenied = this.EnableCtrlDenied;
            Rtn.EnableCtrlAppd = this.EnableCtrlAppd;
            Rtn.EnableCtrlAddl = this.EnableCtrlAddl;
            Rtn.EnableCtrlApprove = this.EnableCtrlApprove;
            Rtn.TotalAdditionalAmount = this.TotalAdditionalAmount;
            // Additional Charges Section
            //Rtn.Code = this.Code;
            //Rtn.Approved = this.Approved;
            //Rtn.Denied = this.Denied;
            //Rtn.Questioned = this.Questioned;
            //Rtn.Approvedby = this.Approvedby;
            //Rtn.Deniedby = this.Deniedby;
            //Rtn.Questionedby = this.Questionedby;
            //Rtn.Description = this.Description;
            //Rtn.Amount = this.Amount;

            Rtn.PONumber = this.PONumber;
            Rtn.CostCenterCode = this.CostCenterCode;
            Rtn.WorkOrderNumber = this.WorkOrderNumber;

            return Rtn;
        }

        public void Render(Double oValue, int oCategory)
        {

        }
        String GetDeniedReasonbyID(int oIndex)
        {
            String Rtn = "";
            switch (oIndex)
            {
                case 10:
                    Rtn = "Lack of Man Power";
                    break;
                case 20:
                    Rtn = "Weather Conditions";
                    break;
                case 30:
                    Rtn = "Distance or Location";
                    break;
            }
            return Rtn;
        }
    }
}
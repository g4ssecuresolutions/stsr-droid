using System;
using System.Data;
using System.IO;
using SQLite;
////using System.Web.Services;
using SR_Class;
using STSR_Droid;
using STSR_Droid.SQLiteTables;
//using STSR_Droid.SRWSServices;

namespace STSR_Droid.Models
{
    public class SR_Request
    {
        public int RequestID { get; set; }
        public string SiteID { get; set; }
        public string CorpCustID { get; set; }
        public bool ManualEntry { get; set; }
        public string SiteName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public Double Latitude { get; set; }
        public Double Longitude { get; set; }
        public string Contact { get; set; }
        public string ContactPhone { get; set; }

        public string RequesterName { get; set; }
        public string RequesterPhone { get; set; }
        public string TechnicianName { get; set; }
        public string TechnicianPhone { get; set; }
        public string GuardName { get; set; }
        public string GuardPhone { get; set; }

        public string OfficerType { get; set; }
        public int OfficerNum { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Notes { get; set; }
        public Double RequestRate { get; set; }
        public bool RequestCancel { get; set; }
        public bool RequestTerminate { get; set; }
        public DateTime RequestTerminateDate { get; set; }
        public string RequestTerminateTime { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDTTM { get; set; }
        public int RequestedBy { get; set; }
        public DateTime RequestedDTTM { get; set; }
        public bool AddlCharges { get; set; }
        public bool AppdCharges { get; set; }
        public string DeptID { get; set; }
        public bool AddlChargesDenied { get; set; }
        public int AddlChargesDeniedBy { get; set; }
        public int RequestStatusID { get; set; }
        public bool ArmGuard { get; set; }
        public string ReasonService { get; set; }
        public bool Uniform { get; set; }
        public string Keys { get; set; }
        public string AuthorizedArea { get; set; }
        public bool EmergencyConstruction { get; set; }
        public bool DMOPreApproved { get; set; }
        public bool DMOGranted { get; set; }
        public DateTime RequestDate { get; set; }
        public string ContactDepartment { get; set; }
        public string LocationType { get; set; }
        public int ReasonServiceID { get; set; }
        public int LocationTypeID { get; set; }
        public bool Urgent { get; set; }
        public bool SaveNote { get; set; }
        public DateTime ExtensionDate { get; set; }
        public string ExtensionNote { get; set; }
        public Double TotalAdditionalAmount { get; set; }
        public DateTime CustomerSiteEntry { get; set; }
        public string CustName { get; set; }
        public string Status { get; set; }
        public string CodeName { get; set; }


        public string PONumber { get; set; }
        public string CostCenterCode { get; set; }
        public string WorkOrderNumber { get; set; }

        public void LoadModel(SRWS_ASMX.SR_RequestItem oRequest)
        {
            if (oRequest == null)
            {
                this.RequestID = 0;
                this.SiteID = "";
                this.CorpCustID = "";
                this.ManualEntry = false;
                this.SiteName = "";
                this.Address1 = "";
                this.Address2 = "";
                this.City = "";
                this.State = "";
                this.ZipCode = "";
                this.Latitude = 0;
                this.Longitude = 0;
                this.Contact = "";
                this.ContactPhone = "";

                this.RequesterName = "";
                this.RequesterPhone = "";
                this.TechnicianName = "";
                this.TechnicianPhone = "";
                this.GuardName = "";
                this.GuardPhone = "";

                this.OfficerType = "";
                this.OfficerNum = 0;
                this.StartDate = DateTime.MinValue;
                this.EndDate = DateTime.MinValue;
                this.Notes = "";
                this.RequestRate =0;
                this.RequestCancel =false;
                this.RequestTerminate = false;
                this.RequestTerminateDate = DateTime.MinValue;
                this.RequestTerminateTime = "";
                this.UpdatedBy =0;
                this.UpdatedDTTM = DateTime.MinValue;
                this.RequestedBy = 0;
                this.RequestedDTTM = DateTime.MinValue;
                this.AddlCharges = false;
                this.AppdCharges = false;
                this.DeptID = "";
                this.AddlChargesDenied = false;
                this.AddlChargesDeniedBy = 0;
                this.RequestStatusID = 0;
                this.ArmGuard = false;
                this.ReasonService = "";
                this.Uniform = false;
                this.Keys = "";
                this.AuthorizedArea = "";
                this.EmergencyConstruction = false;
                this.DMOPreApproved = false;
                this.DMOGranted = false;
                this.RequestDate = DateTime.MinValue;
                this.ContactDepartment = "";
                this.LocationType = "";
                this.ReasonServiceID = 0;
                this.LocationTypeID = 0;
                this.Urgent = false;
                this.SaveNote = false;
                this.ExtensionDate = DateTime.MinValue;
                this.ExtensionNote = "";
                this.TotalAdditionalAmount = 0;
                this.CustomerSiteEntry = DateTime.MinValue;
                this.CustName = "";
                this.Status = "";
                this.CodeName = "";
                this.PONumber = "";
                this.CostCenterCode = "";
                this.WorkOrderNumber = "";
            }
            else
            {
                this.RequestID = oRequest.RequestID;
                this.SiteID = oRequest.SiteID;
                this.CorpCustID = oRequest.CorpCustID;
                this.ManualEntry = oRequest.ManualEntry;
                this.SiteName = oRequest.SiteName;
                this.Address1 = oRequest.Address1 ;
                this.Address2 = oRequest.Address2;
                this.City = oRequest.City;
                this.State = oRequest.State;
                this.ZipCode = oRequest.ZipCode;
                this.Latitude = oRequest.Latitude;
                this.Longitude = oRequest.Longitude;
                this.Contact = oRequest.Contact;
                this.ContactPhone = oRequest.ContactPhone;

                this.RequesterName = oRequest.RequesterName;
                this.RequesterPhone = oRequest.RequesterPhone;
                this.TechnicianName = oRequest.TechnicianName;
                this.TechnicianPhone = oRequest.TechnicianPhone;
                this.GuardName = oRequest.GuardName;
                this.GuardPhone = oRequest.GuardPhone;

                this.OfficerType = oRequest.OfficerType;
                this.OfficerNum = oRequest.OfficerNum;
                this.StartDate = oRequest.StartDate;
                this.EndDate = oRequest.EndDate;
                this.Notes = oRequest.Notes;
                this.RequestRate = oRequest.RequestRate;
                this.RequestCancel = oRequest.RequestCancel;
                this.RequestTerminate = oRequest.RequestTerminate;
                this.RequestTerminateDate = oRequest.RequestTerminateDate;
                this.RequestTerminateTime = oRequest.RequestTerminateTime;
                this.UpdatedBy = oRequest.UpdatedBy;
                this.UpdatedDTTM = oRequest.UpdatedDTTM;
                this.RequestedBy = oRequest.RequestedBy;
                this.RequestedDTTM = oRequest.RequestedDTTM;
                this.AddlCharges = oRequest.AddlCharges;
                this.AppdCharges = oRequest.AppdCharges;
                this.DeptID = oRequest.DeptID;
                this.AddlChargesDenied = oRequest.AddlChargesDenied;
                this.AddlChargesDeniedBy = oRequest.AddlChargesDeniedBy;
                this.RequestStatusID = oRequest.RequestStatusID;
                this.ArmGuard = oRequest.ArmGuard;
                this.ReasonService = oRequest.ReasonService;
                this.Uniform = oRequest.Uniform;
                this.Keys = oRequest.Keys;
                this.AuthorizedArea = oRequest.AuthorizedArea;
                this.EmergencyConstruction = oRequest.EmergencyConstruction;
                this.DMOPreApproved = oRequest.DMOPreApproved;
                this.DMOGranted = oRequest.DMOGranted;
                this.RequestDate = oRequest.RequestDate;
                this.ContactDepartment = oRequest.ContactDepartment;
                this.LocationType = oRequest.LocationType;
                this.ReasonServiceID = oRequest.ReasonServiceID;
                this.LocationTypeID = oRequest.LocationTypeID;
                this.Urgent = oRequest.Urgent;
                this.SaveNote = oRequest.SaveNote;
                this.ExtensionDate = oRequest.ExtensionDate;
                this.ExtensionNote = oRequest.ExtensionNote;
                this.TotalAdditionalAmount = oRequest.TotalAdditionalAmount;
                this.CustomerSiteEntry = oRequest.CustomerSiteEntry;
                this.CustName = oRequest.CustName;
                this.Status = oRequest.Status;
                this.CodeName = oRequest.CodeName;
                this.PONumber = oRequest.PONumber;
                this.CostCenterCode = oRequest.CostCenterCode;
                this.WorkOrderNumber = oRequest.WorkOrderNumber;

            }

        }
        public SRWSServices.SR_Request ToSRWS_ASMX()
        {
            SRWSServices.SR_Request Rtn = new SRWSServices.SR_Request();
            Rtn.RequestID = this.RequestID;
            Rtn.SiteID = this.SiteID;
            Rtn.CorpCustID = this.CorpCustID;
            Rtn.ManualEntry = this.ManualEntry;
            Rtn.SiteName = this.SiteName;
            Rtn.Address1 = this.Address1;
            Rtn.Address2 = this.Address2;
            Rtn.City = this.City;
            Rtn.State = this.State;
            Rtn.ZipCode = this.ZipCode;
            Rtn.Latitude = Convert.ToDecimal(this.Latitude);
            Rtn.Longitude = Convert.ToDecimal(this.Longitude);
            Rtn.Contact = this.Contact;
            Rtn.ContactPhone = this.ContactPhone;

            Rtn.RequesterName = this.RequesterName;
            Rtn.RequesterPhone = this.RequesterPhone;
            Rtn.TechnicianName = this.TechnicianName;
            Rtn.TechnicianPhone = this.TechnicianPhone;
            Rtn.GuardName = this.GuardName;
            Rtn.GuardPhone = this.GuardPhone;

            Rtn.OfficerType = this.OfficerType;
            Rtn.OfficerNum = this.OfficerNum;
            Rtn.StartDate = this.StartDate;
            Rtn.EndDate = this.EndDate;
            Rtn.Notes = this.Notes;
            Rtn.RequestRate = Convert.ToDecimal(this.RequestRate);
            Rtn.RequestCancel = this.RequestCancel;
            Rtn.RequestTerminate = this.RequestTerminate;
            Rtn.RequestTerminateDate = this.RequestTerminateDate;
            Rtn.RequestTerminateTime = this.RequestTerminateTime;
            Rtn.UpdatedBy = this.UpdatedBy;
            Rtn.UpdatedDTTM = this.UpdatedDTTM;
            Rtn.RequestedBy = this.RequestedBy;
            Rtn.RequestedDTTM= this.RequestedDTTM;
            Rtn.AddlCharges= this.AddlCharges;
            Rtn.AppdCharges= this.AppdCharges;
            Rtn.DeptID= this.DeptID;
            Rtn.AddlChargesDenied= this.AddlChargesDenied;
            Rtn.AddlChargesDeniedBy= this.AddlChargesDeniedBy;
            Rtn.RequestStatusID= this.RequestStatusID;
            Rtn.ArmGuard= this.ArmGuard;
            Rtn.ReasonService= this.ReasonService;
            Rtn.Uniform= this.Uniform;
            Rtn.Keys= this.Keys;
            Rtn.AuthorizedArea= this.AuthorizedArea;
            Rtn.EmergencyConstruction= this.EmergencyConstruction;
            Rtn.DMOPreApproved= this.DMOPreApproved;
            Rtn.DMOGranted= this.DMOGranted;
            Rtn.RequestDate= this.RequestDate;
            Rtn.ContactDepartment= this.ContactDepartment;
            Rtn.LocationType= this.LocationType;
            Rtn.ReasonServiceID= this.ReasonServiceID;
            Rtn.LocationTypeID= this.LocationTypeID;
            Rtn.Urgent= this.Urgent;
            Rtn.SaveNote= (this.SaveNote )? "True" : "False";
            Rtn.ExtensionDate= this.ExtensionDate;
            Rtn.ExtensionNote= this.ExtensionNote;
            Rtn.TotalAdditionalAmount= Convert.ToDecimal(this.TotalAdditionalAmount);
            Rtn.CustomerSiteEntry= this.CustomerSiteEntry;
            //Rtn.CustName= this.CustName;
            Rtn.Status = this.Status;
            Rtn.CodeName = this.CodeName;

            Rtn.PONumber = this.PONumber;
            Rtn.CostCenterCode = this.CostCenterCode;
            Rtn.WorkOrderNumber = this.WorkOrderNumber;

            return Rtn;
        }

    }


}
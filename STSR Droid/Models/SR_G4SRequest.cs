using System;
using System.Data;
using System.IO;
using SQLite;
////using System.Web.Services;
using SR_Class;
using STSR_Droid;
using STSR_Droid.SQLiteTables;
//using STSR_Droid.SRWSServices;

namespace STSR_Droid.Models
{
    public class SR_G4SRequest
    {
        public int SR_RequestID { get; set; }
        public string DeptID { get; set; }
        public string CustName { get; set; }
        public string SiteName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Contact { get; set; }
        public string OfficerType { get; set; }
        public int OfficerNum { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public string Notes { get; set; }

    }
}
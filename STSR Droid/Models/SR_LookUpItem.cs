using System;


namespace STSR_Droid.Models
{
    public class SR_LookUpItem
    {
        public string SR_RecordID { get; set; }
        public string Enabled { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string DefaultValue { get; set; }
    }
}
using System;
using System.Data;
using System.IO;
using SQLite;

namespace STSR_Droid.SQLiteTables
{
    [Table("TblG4SRequest")]
    public class TblG4SRequest
    {
        [PrimaryKey, AutoIncrement, Column("_SQLiteID")]
        public int SQLiteID { get; set; }
        public int SR_RequestID { get; set; }
        [MaxLength(10)]
        public string DeptID { get; set; }
        [MaxLength(50)]
        public string CustName { get; set; }
        [MaxLength(50)]
        public string SiteName { get; set; }
        [MaxLength(50)]
        public string City { get; set; }
        [MaxLength(2)]
        public string State { get; set; }
        [MaxLength(50)]
        public string Contact { get; set; }
        [MaxLength(50)]
        public string OfficerType { get; set; }
        public int OfficerNum { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [MaxLength(50)]
        public string Status { get; set; }
        [MaxLength(1000000)]
        public string Notes { get; set; }

    }
}
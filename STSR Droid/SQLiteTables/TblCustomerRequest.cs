using System;
using System.Data;
using System.IO;
using SQLite;

namespace STSR_Droid.SQLiteTables
{
    [Table("TblCustomerRequest")]
    public class TblCustomerRequest
    {
        [PrimaryKey, AutoIncrement, Column("_SQLiteID")]
        public int SQLiteID { get; set; }
        public int SR_RequestID { get; set; }
        [MaxLength(10)]
        public string DeptID { get; set; }
        [MaxLength(10)]
        public string CorpCustID { get; set; }
        [MaxLength(10000)]
        public string CustNotes { get; set; }
        [MaxLength(50)]
        public string CustName { get; set; }
        [MaxLength(50)]
        public string DisplayName { get; set; }
        [MaxLength(50)]
        public string Address1 { get; set; }
        [MaxLength(50)]
        public string City { get; set; }
        [MaxLength(2)]
        public string State { get; set; }
        [MaxLength(50)]
        public string Contact { get; set; }
        [MaxLength(50)]
        public string OfficerType { get; set; }
        public int OfficerNum { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [MaxLength(50)]
        public string Status { get; set; }
        public int RequestStatusID { get; set; }
        [MaxLength(1000000)]
        public string Notes { get; set; }
        public double RequestRate { get; set; }
        public bool AddlCharges { get; set; }
        public bool AppdCharges { get; set; }
        public bool DeniedCharges { get; set; }
        public bool ShowApprove { get; set; }
        public bool CtrlEnabled { get; set; }
        public bool EnableCtrlDenied { get; set; }
        public bool EnableCtrlAppd { get; set; }
        public bool EnableCtrlAddl { get; set; }
        public bool EnableCtrlApprove { get; set; }


    }
}
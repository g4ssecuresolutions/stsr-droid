using System;
using System.Data;
using System.IO;
using SQLite;

namespace STSR_Droid.SQLiteTables
{
    [Table("TblSession")]
    public class TblSession
    {
        [PrimaryKey,AutoIncrement,Column("_SessionID")]
        public int SessionID { get; set; }
        [MaxLength(7)]
        public string SR_RoleID { get; set; }
        [MaxLength(7)]
        public string SR_UserID { get; set; }
        [MaxLength(20)]
        public string SR_UserName { get; set; }
        [MaxLength(30)]
        public string SR_UserFullName { get; set; }
        [MaxLength(7)]
        public string SR_UserCustAccess { get; set; }
        [MaxLength(7)]
        public string SR_CorpCustID { get; set; }
        public bool SR_Approve { get; set; }
        public bool SR_Extend { get; set; }
        public bool SR_CustApprove { get; set; }
        public bool SR_CustExtend { get; set; }
        public bool SR_LoggedIn { get; set; }
        public bool SR_QAS_Mode { get; set; }
        public int SR_UserAttempt { get; set; }
        [MaxLength(20)]
        public string SR_Action { get; set; }
        public bool SR_EULA { get; set; }

    }
}
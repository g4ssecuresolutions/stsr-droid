using System;
using System.Data;
using System.IO;
using SQLite;

namespace STSR_Droid.SQLiteTables
{
    [Table("TblRequest")]
    public class TblRequest
    {
        [PrimaryKey, AutoIncrement, Column("_SQLiteID")]
        public int SQLiteID { get; set; }
        public int RequestID { get; set; }
        [MaxLength(10)]
        public string SiteID { get; set; }
        [MaxLength(10)]
        public string CorpCustID { get; set; }
        public bool ManualEntry { get; set; }
        [MaxLength(50)]
        public string SiteName { get; set; }
        [MaxLength(50)]
        public string Address1 { get; set; }
        [MaxLength(50)]
        public string Address2 { get; set; }
        [MaxLength(50)]
        public string City { get; set; }
        [MaxLength(50)]
        public string State { get; set; }
        [MaxLength(10)]
        public string ZipCode { get; set; }
        public Double Latitude { get; set; }
        public Double Longitude { get; set; }
        [MaxLength(50)]
        public string Contact { get; set; }
        [MaxLength(50)]
        public string ContactPhone { get; set; }

        [MaxLength(50)]
        public string RequesterName { get; set; }
        [MaxLength(50)]
        public string RequesterPhone { get; set; }
        [MaxLength(50)]
        public string TechnicianName { get; set; }
        [MaxLength(50)]
        public string TechnicianPhone { get; set; }
        [MaxLength(50)]
        public string GuardName { get; set; }
        [MaxLength(50)]
        public string GuardPhone { get; set; }

        [MaxLength(50)]
        public string OfficerType { get; set; }
        public int OfficerNum { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [MaxLength(1000000)]
        public string Notes { get; set; }
        public Double RequestRate { get; set; }
        public bool RequestCancel { get; set; }
        public bool RequestTerminate { get; set; }
        public DateTime RequestTerminateDate { get; set; }
        [MaxLength(4)]
        public string RequestTerminateTime { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDTTM { get; set; }
        public int RequestedBy { get; set; }
        public DateTime RequestedDTTM { get; set; }
        public bool AddlCharges { get; set; }
        public bool AppdCharges { get; set; }
        [MaxLength(10)]
        public string DeptID { get; set; }
        public bool AddlChargesDenied { get; set; }
        public int AddlChargesDeniedBy { get; set; }
        public int RequestStatusID { get; set; }
        public bool ArmGuard { get; set; }
        [MaxLength(50)]
        public string ReasonService { get; set; }
        public bool Uniform { get; set; }
        [MaxLength(50)]
        public string Keys { get; set; }
        [MaxLength(50)]
        public string AuthorizedArea { get; set; }
        public bool EmergencyConstruction { get; set; }
        public bool DMOPreApproved { get; set; }
        public bool DMOGranted { get; set; }
        public DateTime RequestDate { get; set; }
        [MaxLength(50)]
        public string ContactDepartment { get; set; }
        [MaxLength(50)]
        public string LocationType { get; set; }
        public int ReasonServiceID { get; set; }
        public int LocationTypeID { get; set; }
        public bool Urgent { get; set; }
        public bool SaveNote { get; set; }
        public DateTime ExtensionDate { get; set; }
        [MaxLength(1000)]
        public string ExtensionNote { get; set; }
        public Double TotalAdditionalAmount { get; set; }
        public DateTime CustomerSiteEntry { get; set; }
        [MaxLength(50)]
        public string CustName { get; set; }
        [MaxLength(150)]
        public string Status { get; set; }

    }
}